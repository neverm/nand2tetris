# nand2tetris

### Description
Implementation of various projects from the book "The Elements of Computing Systems"
by N. Nisan and S.Schocken. The projects are: assembler, virtual machine and compiler.

### Building
Running `gradle build` will produce all projects as jar files and place them in jar/ directory


### Assembler
Assembler takes a Hack assembly language file and produces pseudo-binary executable.
Pseudo-binary file is a plain-text representation of a binary format:
such file consists of lines of text of length 16, each line representing a single command

Target platform has 16-bit words and stores running programs separately from data.

Assembly language features two types of instructions: addressing (A-instruction) and compute (C-instruction).

##### A-instruction

A-instruction loads a value into the A-register, which then can be used for addressing. Syntactically, A-instruction
looks as follows `@5`.
    
Assembler also supports using symbolic names instead of numbers, each will get assigned its unique number
at assembly time. This effectively adds variable support, using `@var_name` as memory address will result
in the same memory address throughout the program.

##### C-instruction

C-instruction consists of three parts: destination, computation and jump.
Syntactically they are separated by equal sign and a semicolon, as follows

    destination=computation;jump
    
Destination or jump can be omitted, together with corresponding separator: `computation;jump`
or `destination=computation` 

Destination is a combination of A, M and D that specifies where the result of the computation will be put. Some
valid destinations are: M, A, AMD, DM, etc. A and D stand for A and D register respectively, and
setting M as destination would result in writing computation result to the memory, using A register as address.

The following computations are allowed: 0, 1, -1, D, A, M, !D, !A, !M, -D, -A, -M,
D+1, A+1, M+1, D-1, A-1, M-1, D+A, D+M, D-A, D-M, A-D, M-D, D&A, D&M, D|A, D|M

Operations are defined as follows:

| Operation        | Meaning    
| ------------- | ------------- |
| 0, 1, -1      | Constant |
| R      | Take value of the source as is|
| !R | bitwise negation: flip all bits R|
| -R | arithmetic negation: flip all bits in R and add 1|
| R+1 | add 1 to the value in R|
| R-1 | subtract 1 from the value in R|
| R+S | add value in R to the value in S|
| R-S | subtract value S from the value in R|
| R&S | bitwise AND of values in R and S|
| R&#124;S | bitwise OR of values in R and S|

All jump instructions will compare value currently stored in D register, and perform
a jump to the address currently loaded in the A register. Typical usage would be loading
a label address into A register and then performing some conditional or unconditional jump.

The following jumps types are supported:

| Jump Instruction | Condition for the value in D register
| ---------------- | ---------- |
| JGT | Greater than 0
| JEQ | Equal to 0
| JGE | Greater than or equal to 0
| JLT | Less than 0 
| JLE | Less than or equal to 0
| JNE | Not equal to zero
| JMP | Unconditional jump

##### Labels

Address of some instruction can be stored in a symbolic label and later be referred
to:

    (LOOP)
    // some computation here
    @LOOP
    0;JMP
    
Addresses can be referred forward, so if there is a `(LOOP)` label further in the code
loading `@LOOP` will not generate a new variable address, but will load the address of the 
instruction that follows `(LOOP)`.


### Virtual Machine

VM translator takes VM bytecode file(s) and translates them into Hack assembly language file.
VM language models a stack based virtual machine with support of functions and memory segments.
VM program is organized into functions, each function having stand-alone code.
VM has a single 16 bit data type which can be used as integer, boolean or a pointer. 

#### VM commands

##### Arithmetic and logical commands

All of the commands take values from the stack
and push the result back onto the stack.

| Command | Return value
| ------- | ----------- |
| add | x + y
| sub | x - y
| neg | -y
| eq | true if x = y else false
| gt | true if x > y else false
| lt | true if x < y else false
| and | x AND y (bitwise)
| or | x OR y (bitwise)
| not | NOT y (bitwise)


##### Memory access commands

VM features eight separate virtual memory segments with which memory commands can interact.

There are two memory commands: `push segment index` and `pop segment index`.
Push takes value from the segment and pushes this value to the top of the stack.
Pop removes value from the top of the stack and places it into the given memory segment
and index in that segment.

Memory segments

| Segment | Purpose | Comment
| ------- | ----------- | ----- |
| argument | stores function's arguments| Allocated dynamically by the VM implementation when the function is entered
| local | stores function's local variables | Allocated dynamically by the VM implementation and initiazized to 0s when the function is entered
| static | stores static variables that are shared by all functions in the same .vm file | Allocated by the VM implementation for each .vm file
| constant | pseudo-segment that holds all constants in range 0..32767 | Emulated by the VM implementation, seen by all functions in the program.
| this/that | general purpose segments, that can be made to correspond to different areas in the heap | Any vm function can use these segments
| pointer | a two entry segment that holds base addresses for this/that segments | Setting pointer 0 (or 1) to some address has effect of aligning this (or that) segment to the heap area beginning at that address
| temp | fixed eight entry segment that holds temporary variables | Shared by all functions in the program

##### Program flow and function calling commands

| Command name | Description
| ---- | ----
| label symbol | Label declaration
| goto symbol | Unconditional branching
| if-goto symbol | Conditional branching
| function functionName nLocals | Function declaration specifying the number of function's local variables
| call functionName nArgs | Function invocation, specifying the number of function's arguments
| return | transfer control back to the calling function

##### Standard mapping over Hack platform

**Ram usage**

| Addresses | Usage |
| ---- | ---- |
| 0-15 | Virtual registers
| 16-255 | Static variables
| 256-2047 | Stack
| 2048-16483 | Heap
| 16384-24575 | Memory mapped I/O

**Virtual registers**

| Register | Name | Usage |
| ---- | ---- | ----
| RAM[0] | SP | Stack pointer: points to the topmost location on the stack
| RAM[1] | LCL | Points to the base of the current VM function's `local` segment
| RAM[2] | ARG | Points to the base of the current VM function's `argument` segment
| RAM[3] | THIS | Points to the base of current `this` segment (within the heap)
| RAM[4] | THAT | Points to the base of current `that` segment (within the heap)
| RAM[5-12] | - | Holds `temp` segment
| RAM[13-15] | - | Used by VM implementation as general purpose registers

**Memory segments mapping**

`local`, `argument`, `this`, `that` are mapped directly onto the RAM,
so accessing ith entry of these segments will result in accessing
`base+i` entry of RAM, where `base` is stored in the corresponding register.

`pointer`, `temp` mapped directly into fixed area of RAM (so the `base` is always fixed)
`pointer` is mapped to locations 3-4, thus allowing to adjust base address of `this` and `that`.
`temp` is mapped to locations 5-12.
`constant` segment if virtual, for `constant i` calls VM implementation supplies
`i`
`static` is implemented by creating assembly language variables.
Creating new variable allocates space for it, which can be exploited
by VM implementation to mimic static variables functionality.
Every `static i` call in `Xxx.vm` file gets translated into
`Xxx.i` variable, thus guaranteeing uniqueness among other vm files.

### Compiler

#### Jack language specification

Jack is a simple weakly type language, that supports classes (no inheritance), subroutines, arrays and control structures.

The basic Jack programming unit is class. Class should reside in a separate `.jack`
file and can be compiled separately. Class declaration format is the following
(the order matters):

    class className {
        // field and static variable declarations
        // subroutine declarations
    }

There are three kinds of subroutines: methods, functions and constructors.

Functions correspond to java static methods and do not operate on any object instance besides their arguments.
They have only access to static variables.

Methods correspond to java methods, and operate on an instance of the class they belong to, thus they have access
to field variables, as well as to static variables.

Constructors are used for object creation, and return instance of the created object.
By convention, all constructor functions should be named `new`, and called as
a regular function would be called: `ClassName.new(arg1, arg2)`

**Subroutine declaration format**

    subroutine type name (parameter-list) {
        //local variable declarations
        //statements
    }

##### Syntax

|  |   |
| ---- | --- |
| White space and comments | Space characters, newlines and comments are ignored.|
| | Comment formats supported: `//comment`, `/* multiline comment */`
| Symbols | `(`, `)`: grouping arithmetic expressions
| | `[`, `]`: array indexing
| | `{`, `}`: grouping program units and statements
| | `,`: variable list operator
| | `;`: statement terminator
| | `=`: Assignment and comparison operator
| | `.`: Class membership
| | &#124;, `+`, `-`, `*`, `/`, `&`, `~`, `<`, `>`, : operators
| Reserved words | `class`, `constructor`, `method`, `function`: program components
| | `int`, `boolean`, `char`, `void`: primitive types
| | `var`, `static`, `field`: variable declarations
| | `let`, `do`, `if`, `else`, `while`, `return`: Statements
| | `true`, `false`, `null`: constant values
| | `this`: object reference
| Constants | Integer constants are positive in standard decimal notation
| | negative integers like -25 are expressions consisting of applying unary `-` to a positive constant
| | String constants are enclosed within douple quotes and may contain any character except newline or douple quoteThese characters are supplied by the functions String.newLine() and String.doubleQuote() from the standard library
| | Boolean constants can be `true` or `false`
| | `null` constant signifies null reference
| Identifiers | Identifiers are composed from sequences of letters a-z, A-Z, digits (0-9) and underscore `_`
| | The first character can't be a number, and identifiers are case sensitive

By convention, to run Jack program it should have Main class with main method somewhere,
and this method serves as an entry point to the program by runtime system.

##### Types

Jack feature three primitive types:

* int: 16 bit 2s complement
* boolean: false or true
* char: unicode character

Variables of primitive types are allocated into memory when they are declared.

##### Object types

Every class defines an object type. Declaration of a variable of object type creates a
reference variable. The object itself is created at runtime by calling corresponding constructor.

##### Arrays

Arrays are declared using builtin class Array. Arrays are one-dimensional with 0-based indexing.
Array entries don't have declared type and different entries of a single array may have different types.
Array declaration, like an object declaration, only creates a reference.
The actual construction is done by `Array.new(length)` call.

##### Strings

Strings are declared using builtin class `String`. Jack compiler recognizes
`"xxx"` syntax and treats it as contents of some `String` object. Contents of `String` object
can be accessed and modified by `String` class methods

##### Variable scope

Jack features four kinds of variables, that have different scope

| Kind | Description | Declared in | Scope |
| ---- | ---- | ---- | ---- |
| Static variables | One copy of each static variable exists, and is shared among all objects of the class | Class declaration | The class in which declared |
| Field variables | Every object instance of the class has a private copy of field variables | Class declaration | The class in which they are declared, except for functions|
| Local variables | Local variables are allocated on the stack when a subroutine is called, and freed when it returns | Subroutine declaration | Subroutine in which they are declared |
| Parameter variables | Used to specify input to subroutines | Parameter list in subroutine declarations | Subroutine in which they are declared |

#### Statements

* Let

  Syntax: `let variable = expression;` or `let variable[expression] = expression`

  Assignment operation to either a variable or array element

* if

  Syntax:

      if (expression) {
          consequence_statements
      }
      else {
          alternative_statements
      }

  Standard if branching, if expression evaluates to anything but 0, false or null,
  the consequence statements will be executed, otherwise (if else branch is present) the alternative
  statements will be executed. Braces are mandatory even if only a single statement present.

* while

  Syntax:

       while (expression) {
           statements
       }

  Execute statements while expression evaluates to anything but 0, false or null

* do

  Syntax:

      do functionName;

  Used to call a function or a method for it's side-effect, ignoring return value

* return

  Syntax:

      return expression;

  or

      return;

  Used to return a value from a subroutine. The second form must be used for methods and functions
  that return a void value.
  Constructors must return the expression `this`


#### Expressions

An expression is one of the following:

* a constant
* a variable name
* `this` keyword
* array element, using syntax `array_name[expression]`
* a subroutine call that returns a non-void type
* an expression prefixed by one of the unary operators: `-` (arithmetic negation)
or `~` (boolean negation)
* an expression of the form `exp1 op exp2`, where `op` is
one of the following binary operators:
    1. `+`, `-`, `*`, `/`: integer arithmetic operators
    2. `&`, `|`: boolean and and or
* `(exp)` an expression in parenthesis.

No order of precedence is assigned to the operators, so the order of evaluation is left to right

`1 + 2 * 3` would result in 9

Parenthesis must be used to enforce order of precedence.

##### Subroutine calls

Subroutines are called using dot notation on either a class (for functions and constructors),
like this `ClassName.functionName(params)`, or on an object (for methods), like this `x.methodName(params)`.

Parameters are comma-separated list of expressions that correspond to the arguments of the subroutine being called.

Within a class methods are called using `methodName(params)` syntax,
while constructors and static methods (functions) must use explicit
class name notation `ClassName.subroutineName(params)`.
Outside of the class, methods are called on instances explicitly,
like this `o.methodName(params)` where `o` is an instance of some other class.
