package com.nvm.nand.compiler.parsing;

import java.util.ArrayList;
import java.util.List;

import static com.nvm.nand.compiler.parsing.ParseTree.*;
import static com.nvm.nand.compiler.parsing.ParseTree.NodeType.EMPTY;
import static com.nvm.nand.compiler.parsing.Token.Type.IDENTIFIER;
import static com.nvm.nand.compiler.parsing.Token.Type.KEYWORD;
import static com.nvm.nand.compiler.parsing.Token.Type.SYMBOL;

/**
 * Recursive descend implementation of a parser for the Jack language grammar. The simplicity
 * of the grammar leads to a simple implementation - the only time a lookahead needs to be performed
 * is for parsing an expression: to distinguish among some_name, some_name[5], some_name.method() and some_name(arg)
 * we need to look one token ahead
 *
 */
public class Parser {

    private static final String allowedUnaryOperators = "-~";

    private static final String allowedBinaryOperators = "+-*/&|<>=";

    private List<Token> input;

    private int position;

    public Parser(List<Token> input) {
        this.input = input;
    }

    /**
     * Parse class declaration from current input
     * @return parse tree for class
     * @throws ParseException if current stream of token doesn't represent valid Jack class or there are no tokens left
     */
    public ParseTree parseClass() throws ParseException {
        List<ParseTree> children = new ArrayList<>();
        children.add(parseKeyword("class"));
        children.add(parseIdentifier());
        children.add(parseSymbol("{"));

        Token next = lookupNextToken();
        String value = next.getValue();
        while (next.getType() == KEYWORD && (value.equals("field") || value.equals("static"))) {
            children.add(parseClassVariableDeclaration());
            next = lookupNextToken();
            value = next.getValue();
        }
        while (next.getType() == KEYWORD && (value.equals("constructor") || value.equals("method") ||
                value.equals("function"))) {
            children.add(parseSubroutineDeclaration());
            next = lookupNextToken();
            value = next.getValue();
        }
        children.add(parseSymbol("}"));
        return new ParseTree(Name.CLASS, children);
    }

    /**
     * Parse class variable declaration from current input
     * Assume that first token is a valid class variable keyword: 'field' or 'static'
     * @return parse tree for class variable declaration
     * @throws ParseException if current stream of token doesn't represent valid Jack class variable declaration
     * or there are no tokens left
     */
    public ParseTree parseClassVariableDeclaration() throws ParseException {
        List<ParseTree> children = new ArrayList<>();
        children.add(parseKeyword(lookupNextToken().getValue()));
        children.add(parseType());
        children.add(parseIdentifier());
        while (isNextToken(SYMBOL, ",")) {
            children.add(parseSymbol(","));
            children.add(parseIdentifier());
        }
        children.add(parseSymbol(";"));
        return new ParseTree(Name.CLASS_VAR, children);
    }

    /**
     * Parse subroutine declaration from current input
     * Assume that first token is a valid subroutine keyword: 'method', 'constructor' or 'function'
     * @return parse tree for subroutine declaration
     * @throws ParseException if current stream of token doesn't represent valid Jack subroutine declaration
     * or there are no tokens left
     */
    public ParseTree parseSubroutineDeclaration() throws ParseException {
        List<ParseTree> children = new ArrayList<>();
        children.add(parseKeyword(lookupNextToken().getValue()));
        if (isNextToken(KEYWORD, "void")) {
            children.add(parseKeyword("void"));
        }
        else {
            children.add(parseType());
        }
        children.add(parseIdentifier());
        children.add(parseSymbol("("));
        if (!isNextToken(SYMBOL, ")")) {
            children.add(parseParametersList());
        }
        else {
            children.add(new ParseTree(Name.PARAMETERS_LIST, EMPTY));
        }
        children.add(parseSymbol(")"));
        while (isNextToken(KEYWORD, "var")) {
            children.add(parseVariableDeclaration());
        }
        children.add(parseSubroutineBody());
        return new ParseTree(Name.SUBROUTINE_DECLARATION, children);
    }

    /**
     * Parse subroutine declaration parameters list separated by comma from current input
     * Expects at least one parameter to be present
     * @return parse tree for subroutine declaration parameters list
     * @throws ParseException if current stream of token doesn't represent valid Jack subroutine declaration
     * parameters list or there are no tokens left
     */
    public ParseTree parseParametersList() throws ParseException {
        List<ParseTree> children = new ArrayList<>();
        do {
            children.add(parseType());
            children.add(parseIdentifier());
        } while (canAdvance() && isNextToken(SYMBOL, ",") && children.add(parseSymbol(",")));
        return new ParseTree(Name.PARAMETERS_LIST, children);
    }

    /**
     * Parse subroutine body from current input
     * @return parse tree for subroutine body
     * @throws ParseException if current stream of token doesn't represent valid Jack subroutine body
     * or there are no tokens left
     */
    public ParseTree parseSubroutineBody() throws ParseException {
        List<ParseTree> children = new ArrayList<>();
        children.add(parseSymbol("{"));
        while (canAdvance() && isNextToken(KEYWORD, "var")) {
            children.add(parseVariableDeclaration());
        }
        children.add(parseStatements());
        children.add(parseSymbol("}"));
        return new ParseTree(Name.SUBROUTINE_BODY, children);
    }

    /**
     * Parse variable declaration from current input
     * @return parse tree for variable declaration
     * @throws ParseException if current stream of token doesn't represent valid Jack variable declaration
     * or there are no tokens left
     */
    public ParseTree parseVariableDeclaration() throws ParseException {
        List<ParseTree> children = new ArrayList<>();
        children.add(parseKeyword("var"));
        children.add(parseType());
        children.add(parseIdentifier());
        while (canAdvance() && isNextToken(SYMBOL, ",")) {
            children.add(parseSymbol(","));
            children.add(parseIdentifier());
        }
        children.add(parseSymbol(";"));
        return new ParseTree(Name.VARIABLE_DECLARATION, children);
    }

    /**
     * Parse zero or more statements
     * @return parse tree for statements
     * @throws ParseException if current stream of token doesn't represent valid statement sequence
     * or there are no tokens left
     */
    public ParseTree parseStatements() throws ParseException {
        List<ParseTree> children = new ArrayList<>();
        while (canAdvance() && isStatementNext()) {
            children.add(parseStatement());
        }
        if (children.size() > 0) {
            return new ParseTree(Name.STATEMENTS, children);
        }
        else {
            return new ParseTree(Name.STATEMENTS, EMPTY);
        }
    }

    /**
     * Parse do statement
     * @return parse tree for do statement
     * @throws ParseException if current stream of token doesn't represent valid do statement
     * or there are no tokens left
     */
    public ParseTree parseDo() throws ParseException {
        List<ParseTree> children = new ArrayList<>();
        children.add(parseKeyword("do"));
        children.addAll(parseSubroutineCall(consumeToken()));
        children.add(parseSymbol(";"));
        return new ParseTree(Name.DO_STATEMENT, children);
    }

    /**
     * Parse if statement
     * @return parse tree for if statement
     * @throws ParseException if current stream of token doesn't represent valid if statement
     * or there are no tokens left
     */
    public ParseTree parseIf() throws ParseException {
        List<ParseTree> children = new ArrayList<>();
        children.add(parseKeyword("if"));
        children.add(parseSymbol("("));
        children.add(parseExpression());
        children.add(parseSymbol(")"));
        children.add(parseSymbol("{"));
        children.add(parseStatements());
        children.add(parseSymbol("}"));
        if (canAdvance() && isNextToken(KEYWORD, "else")) {
            children.add(parseKeyword("else"));
            children.add(parseSymbol("{"));
            children.add(parseStatements());
            children.add(parseSymbol("}"));
        }
        return new ParseTree(Name.IF_STATEMENT, children);
    }

    /**
     * Parse let statement
     * @return parse tree for let statement
     * @throws ParseException if current stream of token doesn't represent valid let statement
     * or there are no tokens left
     */
    public ParseTree parseLet() throws ParseException {
        List<ParseTree> children = new ArrayList<>();
        children.add(parseKeyword("let"));
        children.add(parseIdentifier());
        if (isNextToken(SYMBOL, "[")) {
            children.add(parseSymbol("["));
            children.add(parseExpression());
            children.add(parseSymbol("]"));
        }
        children.add(parseSymbol("="));
        children.add(parseExpression());
        children.add(parseSymbol(";"));
        return new ParseTree(Name.LET_STATEMENT, children);
    }

    /**
     * Parse while statement
     * @return parse tree for while statement
     * @throws ParseException if current stream of token doesn't represent valid while statement
     * or there are no tokens left
     */
    public ParseTree parseWhile() throws ParseException {
        List<ParseTree> children = new ArrayList<>();
        children.add(parseKeyword("while"));
        children.add(parseSymbol("("));
        children.add(parseExpression());
        children.add(parseSymbol(")"));
        children.add(parseSymbol("{"));
        children.add(parseStatements());
        children.add(parseSymbol("}"));
        return new ParseTree(Name.WHILE_STATEMENT, children);
    }

    /**
     * Parse return statement
     * @return parse tree for return statement
     * @throws ParseException if current stream of token doesn't represent valid return statement
     * or there are no tokens left
     */
    public ParseTree parseReturn() throws ParseException {
        List<ParseTree> children = new ArrayList<>();
        children.add(parseKeyword("return"));
        if (!isNextToken(SYMBOL, ";")) {
            children.add(parseExpression());
        }
        children.add(parseSymbol(";"));
        return new ParseTree(Name.RETURN_STATEMENT, children);
    }

    /**
     * Parse expression
     * @return parse tree for an expression
     * @throws ParseException if current stream of token doesn't represent a valid expression
     * or there are no tokens left
     */
    public ParseTree parseExpression() throws ParseException {
        List<ParseTree> children = new ArrayList<>();
        children.add(parseTerm());
        while (canAdvance() && allowedBinaryOperators.contains(lookupNextToken().getValue())) {
            Token next = consumeToken();
            children.add(new ParseTree(Name.SYMBOL, next.getValue()));
            children.add(parseTerm());
        }
        return new ParseTree(Name.EXPRESSION, children);
    }

    /**
     * Parse one or more expressions separated by comma
     * @return parse tree for expression list
     * @throws ParseException if current stream of token doesn't represent valid expression list
     * or there are no tokens left
     */
    public ParseTree parseExpressionList() throws ParseException {
        List<ParseTree> children = new ArrayList<>();
        children.add(parseExpression());
        while (canAdvance() && isNextToken(SYMBOL, ",")) {
            children.add(parseSymbol(","));
            children.add(parseExpression());
        }
        return new ParseTree(Name.EXPRESSION_LIST, children);
    }

    /**
     * Parse a term. A term could be a constant, an identifier or an application of unary operator to a term.
     * In case of an identifier an additional lookahead is required to distinguish a variable name from array access
     * or a function application which also start with identifier
     * @return parse tree for return statement
     * @throws ParseException if current stream of token doesn't represent valid return statement
     * or there are no tokens left
     */
    public ParseTree parseTerm() throws ParseException {
        Token currentToken = consumeToken();
        List<ParseTree> children = new ArrayList<>();
        switch (currentToken.getType()) {
            case INT_CONSTANT:
                int numberValue = Integer.parseInt(currentToken.getValue());
                if (numberValue > 32767) {
                    throw new ParseException("Integer value is bigger than 32767");
                }
                children.add(new ParseTree(Name.INT_CONST, currentToken.getValue()));
                break;
            case STR_CONSTANT:
                children.add(new ParseTree(Name.STR_CONST, currentToken.getValue()));
                break;
            case KEYWORD:
                children.add(new ParseTree(Name.KEYWORD, currentToken.getValue()));
                break;
            case SYMBOL:
                String value = currentToken.getValue();
                if (value.equals("(")) {
                    children.add(new ParseTree(Name.SYMBOL, "("));
                    children.add(parseExpression());
                    children.add(parseSymbol(")"));
                    return new ParseTree(Name.TERM, children);
                }
                else if (allowedUnaryOperators.contains(value)) {
                    children.add(new ParseTree(Name.SYMBOL, value));
                    children.add(parseTerm());
                    return new ParseTree(Name.TERM, children);
                }
                else {
                    throw new ParseException(String.format("Symbol '%s' can't" +
                            " be used as unary operator", value));
                }
            case IDENTIFIER:

                if (!canAdvance() || !"([.".contains(lookupNextToken().getValue())) {
                    // parse simple identifier
                    children.add(new ParseTree(Name.IDENTIFIER, currentToken.getValue()));
                }
                else {
                    Token tokenAhead = lookupNextToken();
                    String valueAhead = tokenAhead.getValue();
                    if (valueAhead.equals("[")) {
                        // parse array access
                        consumeToken();
                        children.add(new ParseTree(Name.IDENTIFIER, currentToken.getValue()));
                        children.add(new ParseTree(Name.SYMBOL, valueAhead));
                        children.add(parseExpression());
                        children.add(parseSymbol("]"));
                        return new ParseTree(Name.TERM, children);
                    }
                    else {
                        // parse function or method call
                        children = parseSubroutineCall(currentToken);
                    }
                }
                break;
            default:
                unexpectedToken(currentToken);
        }
        return new ParseTree(Name.TERM, children);
    }


    /**
     * Consume one token, expecting it to be of a particular type and have a particular value
     * @param type type of the token expected to be next
     * @param tokenValue value of the token expected to be next
     * @param treeName name of the resulting parse tree
     * @return tree representing this token
     * @throws ParseException when there are no tokens left or the next token
     * doesn't correspond to given token value
     */
    private ParseTree parseToken(Token.Type type, String tokenValue, Name treeName) throws ParseException {
        Token next = consumeToken();
        if (next.getType() == type && next.getValue().equals(tokenValue)) {
            return new ParseTree(treeName, tokenValue);
        }
        else {
            throw new ParseException(String.format("Unexpected token: %s. Expected: %s", next.getValue(), tokenValue));
        }
    }

    /**
     * Consume one token, expecting it to be of symbol type and have a particular value
     * @param symbolValue value of the token expected to be next
     * @return tree representing this token
     * @throws ParseException when there are no tokens left or the next token
     * doesn't correspond to the given symbol
     */
    private ParseTree parseSymbol(String symbolValue) throws ParseException {
        return parseToken(SYMBOL, symbolValue, Name.SYMBOL);
    }

    /**
     * Consume one token, expecting it to be of keyword type and have a particular value
     * @param keywordValue value of the token expected to be next
     * @return tree representing this token
     * @throws ParseException when there are no tokens left or the next token
     * doesn't correspond to the given keyword
     */
    private ParseTree parseKeyword(String keywordValue) throws ParseException {
        return parseToken(KEYWORD, keywordValue, Name.KEYWORD);
    }

    /**
     * Parse identifier from the current input
     * @return Parse tree corresponding to the identifier next in the input
     * @throws ParseException when next token is not an identifier or there are no tokens left
     */
    private ParseTree parseIdentifier() throws ParseException {
        Token next = consumeToken();
        if (next.getType() != IDENTIFIER) {
            throw new ParseException(String.format("Expected an identifier got %s %s",
                   next.getType(), next.getValue()));
        }
        else {
            return new ParseTree(Name.IDENTIFIER, next.getValue());
        }
    }

    private ParseTree parseStatement() throws ParseException {
        if (isNextToken(KEYWORD, "do")) {
            return parseDo();
        }
        else if (isNextToken(KEYWORD, "while")) {
            return parseWhile();
        }
        else if (isNextToken(KEYWORD, "let")) {
            return parseLet();
        }
        else if (isNextToken(KEYWORD, "if")) {
            return parseIf();
        }
        else if (isNextToken(KEYWORD, "return")) {
            return parseReturn();
        }
        else {
            throw new ParseException(String.format("Unable to parse statement, invalid token: %s",
                    lookupNextToken().getValue()));
        }
    }

    /**
     * Parse subroutine call that is either a subroutine call f(), or a method call: Class.f() or object.f()
     * Output is not encosed into a parent parsetree to be used in different parsing functions
     * @param startToken first token in the input
     * @return list of parse trees corresponding to the
     * @throws ParseException
     */
    private List<ParseTree> parseSubroutineCall(Token startToken) throws ParseException {
        if (startToken.getType() != IDENTIFIER) {
            throw new ParseException(String.format("Subroutine call should start with identifier, got %s instead",
                    startToken.getValue()));
        }
        List<ParseTree> children = new ArrayList<>();
        children.add(new ParseTree(Name.IDENTIFIER, startToken.getValue()));
        if (isNextToken(SYMBOL, ".")) {
            children.add(parseSymbol("."));
            Token currentToken = consumeToken();
            if (currentToken.getType() != IDENTIFIER) {
                unexpectedToken(currentToken);
            }
            children.add(new ParseTree(Name.IDENTIFIER, currentToken.getValue()));
        }
        children.add(parseSymbol("("));
        if (isNextToken(SYMBOL, ")")) {
            children.add(new ParseTree(Name.EXPRESSION_LIST, EMPTY));
        }
        else {
            children.add(parseExpressionList());
        }
        children.add(parseSymbol(")"));
        return children;
    }

    private ParseTree parseType() throws ParseException {
        Token next = lookupNextToken();
        String value = next.getValue();
        if (next.getType() == KEYWORD && (value.equals("int")) ||
                value.equals("char") || value.equals("boolean")) {
            consumeToken();
            return new ParseTree(Name.KEYWORD, value);
        }
        else {
            return parseIdentifier();
        }
    }

    /**
     * Test next token to having given type and value
     * @param type expected from the next token
     * @param value expected from the next token
     * @return true if the next token in the input conforms to given values
     * @throws ParseException if there are no tokens left in the input
     */
    private boolean isNextToken(Token.Type type, String value) throws ParseException {
        Token next = lookupNextToken();
        return (next.getType() == type && next.getValue().equals(value));
    }

    private boolean isStatementNext() throws ParseException {
        return isNextToken(KEYWORD, "do") || isNextToken(KEYWORD, "if") || isNextToken(KEYWORD, "let") ||
            isNextToken(KEYWORD, "while") || isNextToken(KEYWORD, "return");
    }

    private boolean isSubroutineDecNext() throws ParseException {
        return isNextToken(KEYWORD, "method") || isNextToken(KEYWORD, "constructor") ||
                isNextToken(KEYWORD, "function");
    }

    private Token consumeToken() throws ParseException {
        Token nextToken = lookupNextToken();
        position++;
        return nextToken;
    }

    private Token lookupNextToken() throws ParseException {
        if (!canAdvance()) {
            throw new ParseException("Can't advance parser: no more tokens left");
        }
        return input.get(position);
    }

    private boolean canAdvance() {
        return position < input.size();
    }

    private void unexpectedToken(Token token) throws ParseException {
        throw new ParseException(String.format("Unexpected token: %s", token.getValue()));
    }
}
