package com.nvm.nand.compiler.codegen;

import com.nvm.nand.compiler.parsing.ParseTree;

import java.util.*;
import java.util.stream.Collectors;

import static com.nvm.nand.compiler.codegen.SymbolTable.Entry.*;
import static com.nvm.nand.compiler.codegen.SymbolTable.Entry.Kind.*;
import static com.nvm.nand.compiler.parsing.ParseTree.Name.*;

public class Generator {

    public static final String LABEL_END = "IF_END";
    public static final String LABEL_ALTERNATIVE = "IF_ALTERNATIVE";
    public static final String LABEL_LOOP = "LOOP";
    public static final String LABEL_LOOP_END = "LOOP_END";

    private static Map<String, String> binaryOperations, unaryOperations, keywords;

    static {
        binaryOperations = new HashMap<>();
        binaryOperations.put("+", "add");
        binaryOperations.put("-", "sub");
        binaryOperations.put("&", "and");
        binaryOperations.put("|", "or");
        binaryOperations.put("=", "eq");
        binaryOperations.put(">", "gt");
        binaryOperations.put("<", "lt");
        binaryOperations.put("*", "call Math.multiply 2");
        binaryOperations.put("/", "call Math.divide 2");

        unaryOperations = new HashMap<>();
        unaryOperations.put("~", "not");
        unaryOperations.put("-", "neg");

        keywords = new HashMap<>();
        keywords.put("false", "constant 0");
        keywords.put("null", "constant 0");
        keywords.put("true", "constant 1\n" + "neg");
        keywords.put("this", "pointer 0");

    }

    private SymbolTable classTable;

    private String className;

    private int labelCount;

    public void setClassName(String className) {
        this.className = className;
    }

    public void setClassTable(SymbolTable classTable) {
        this.classTable = classTable;
    }

    /**
     * Generate symbol table for a given class. The table will include all variables defined
     * in class variable declarations.
     * @param tree representing a class. Assume the tree is a valid class parse tree
     * @return symbol table for the given class
     */
    public SymbolTable generateClassTable(ParseTree tree) {
        return generateTableForDeclarations(tree.getChildren(CLASS_VAR), true);
    }

    /**
     * Generate symbol table for a given subroutine. The table will include all variables defined
     * in subroutine variable declarations.
     * Note, that if subroutine is a method, address of the object is passed implicitly as the first argument
     * @param tree representing a subroutine. Assume the tree is a valid subroutine parse tree
     * @return symbol table for the given subroutine
     */
    public SymbolTable generateSubroutineTable(ParseTree tree) {
        SymbolTable paramsTable = new SymbolTable();
        String modifier = tree.getChildren().get(0).getTerminalValue();
        if (modifier.equals("method")) {
            paramsTable.put("this", "object", Kind.ARGUMENT);
        }
        if (modifier.equals("constructor")) {
            paramsTable.put("this", "object", Kind.LOCAL);
        }
        for (ParseTree parameter: tree.getChildren(PARAMETERS_LIST)) {
            List<ParseTree> children = parameter.getChildren();
            if (children.size() > 0) {
                int i = 0;
                do {
                    String type = children.get(i).getTerminalValue();
                    String name = children.get(i + 1).getTerminalValue();
                    paramsTable.put(name, type, Kind.ARGUMENT);
                    i += 3; // advance over type, name and possible comma
                } while (i < children.size());
            }
        }
        List<ParseTree> varDeclarations =
                tree.getChild(SUBROUTINE_BODY).getChildren(VARIABLE_DECLARATION);
        return paramsTable.addAll(generateTableForDeclarations(varDeclarations, false));
    }

    /**
     * Generate symbol table for given variable declarations. If the declarations are a part of class,
     * the kind of declaration will be taken from the first keyword of which declaration, otherwise (for subroutines)
     * the kind of all declarations will be local
     * @param variableDeclarations to generate table for
     * @param isClassDec whether these variable declarations are a part of class or a subroutine
     * @return table with the variable declarations filled in
     */
    private SymbolTable generateTableForDeclarations(List<ParseTree> variableDeclarations, boolean isClassDec) {
        SymbolTable table = new SymbolTable();
        for (ParseTree variableDeclaration: variableDeclarations) {
            if (variableDeclaration.getChildren().size() == 0) {
                continue;
            }
            List<ParseTree> children = variableDeclaration.getChildren();

            Kind kind;
            String modifier = children.get(0).getTerminalValue();
            switch (modifier) {
                case "static":
                    kind = STATIC;
                    break;
                case "field":
                    kind = FIELD;
                    break;
                case "var":
                    kind = LOCAL;
                    break;
                default:
                    throw new IllegalArgumentException("Unsupported variable declaration modifier: " + modifier);
            }
            int variablePosition = 2;
            String type = children.get(variablePosition-1).getTerminalValue();
            do {
                table.put(children.get(variablePosition).getTerminalValue(), type, kind);
                variablePosition += 2;
            } while (variablePosition < children.size());
        }
        return table;
    }

    /**
     * Generate VM code for the class declaration, specified by the given tree
     * @param classTree representing a class
     * @return VM code for the class
     */
    public String generateClass(ParseTree classTree) {
        StringBuilder result = new StringBuilder();
        this.className = classTree.getChild(1).getTerminalValue();
        this.classTable = generateClassTable(classTree);
        List<ParseTree> subroutines = classTree.getChildren(SUBROUTINE_DECLARATION);
        for (ParseTree subroutineDeclaration: subroutines) {
            result.append(generateSubroutine(subroutineDeclaration));
        }
        return result.toString();
    }

    /**
     * Generate VM code for the subroutine declaration, specified by the given tree
     * @param declaration representing a subroutine
     * @return VM code for the class
     */
    public String generateSubroutine(ParseTree declaration) {
        String modifier = declaration.getChild(0).getTerminalValue();
        String name = declaration.getChild(2).getTerminalValue();
        SymbolTable subroutineTable = generateSubroutineTable(declaration);
        ParseTree statements = declaration.getChild(SUBROUTINE_BODY).getChild(STATEMENTS);
        StringBuilder result = new StringBuilder();
        long localsCount = subroutineTable.getNumberOfKind(LOCAL);
        result.append(String.format("function %s.%s %d\n", className, name, localsCount));
        if (modifier.equals("constructor")) {
            long fieldsCount = classTable.getNumberOfKind(FIELD);
            fieldsCount = (fieldsCount == 0) ? 1 : fieldsCount;
            result.append(String.format("push constant %d\n", fieldsCount));
            result.append("call Memory.alloc 1\n");
            // save newly created object address to the first
            // local variable allocated in the symbol table for this
            // purpose in constructors
            result.append("pop local 0\n");
            result.append("push local 0\n");
            // adjust THIS to point to the newly created object
            result.append("pop pointer 0\n");
        }
        // todo: remove this code and test whether we really need to adjust THIS pointer
        else if (modifier.equals("method")) {
            result.append("push argument 0\n");
            // adjust THIS to the implicit `this` argument, passed at 0th position
            result.append("pop pointer 0\n");
        }
        result.append(generateStatements(statements, subroutineTable));
        return result.toString();
    }

    /**
     * Generate VM code for given expression with respect to given symbol table.
     * The result of evaluating expression will be available
     * on the top of the stack after running generated code.
     * Assume given parse tree is valid as defined in Jack grammar
     * @param expression to be translated
     * @param subroutineTable
     * @return VM code for the expression
     */
    public String generateExpression(ParseTree expression, SymbolTable subroutineTable) {
        if (expression.getChildren().size() == 1) {
            // single term expression
            return generateTerm(expression.getChild(0), subroutineTable);
        }
        // expression in form term1 op1 term2 op2 term3 op3 ... termN
        Iterator<ParseTree> iterator = expression.getChildren().iterator();
        StringBuilder result = new StringBuilder();
        String leftmostTerm = generateTerm(iterator.next(), subroutineTable);
        result.append(leftmostTerm);
        while (iterator.hasNext()) {
            String op = binaryOperations.get(iterator.next().getTerminalValue());
            String term = generateTerm(iterator.next(), subroutineTable);
            result.append(term + op + "\n");
        }
        return result.toString();
    }

    private String generateTerm(ParseTree term, SymbolTable table) {
        ParseTree firstChild = term.getChild(0);
        StringBuilder result = new StringBuilder();
        // todo: include additional information in the parse tree
        // that would specify what kind of term we have: array access, parenthesized expression, unary op, etc
        switch (firstChild.getName()) {
            case SYMBOL:
                // parenthesized exp
                if (firstChild.getTerminalValue().equals("(")) {
                    return generateExpression(term.getChild(1), table);
                }
                // unary op
                return generateTerm(term.getChild(1), table)
                        + unaryOperations.get(firstChild.getTerminalValue())
                        + "\n";
            case INT_CONST:
                return "push constant " + firstChild.getTerminalValue() + "\n";
            case STR_CONST:
                String value = firstChild.getTerminalValue();
                int newArg = value.length() == 0 ? 1 : value.length();
                result.append(String.format("push constant %d\ncall String.new 1\n", newArg));
                for (int i = 0; i < value.length(); i++) {
                    int charCode = value.charAt(i);
                    result.append(String.format("push constant %d\ncall String.appendChar 2\n", charCode));
                }
                return result.toString();
            case IDENTIFIER:
                // variable access
                if (term.getChildren().size() == 1) {
                    return generateVariableSegmentAccess(term.getChild(0).getTerminalValue(), table, "push");
                }
                else if (term.hasTerminalChild("[")) {
                    // array access
                    result.append(generateExpression(term.getChild(EXPRESSION), table));
                    result.append(generateVariableSegmentAccess(firstChild.getTerminalValue(), table, "push"));
                    result.append("add\n");
                    result.append("pop pointer 1\n");
                    result.append("push that 0\n");
                    return result.toString();
                }
                else {
                    // function application
                    return generateSubroutineCall(term, table);
                }
            case KEYWORD:
                return String.format("push %s\n", keywords.get(firstChild.getTerminalValue()));

            default:
                throw new IllegalArgumentException("Illegal term child: " + firstChild.getName());
        }
    }

    /**
     * Generate VM code for an if statement
     * @param table
     * @return VM code for the if statement
     */
    public String generateIf(ParseTree ifStatement, SymbolTable table) {
        ParseTree condition = ifStatement.getChild(EXPRESSION);
        List<ParseTree> statementTrees = ifStatement.getChildren(STATEMENTS);
        StringBuilder result = new StringBuilder();
        result.append(generateExpression(condition, table));
        result.append("not\n");
        int labelNum = generateLabelNumber();
        if (statementTrees.size() > 1) {
            // two hand if
            ParseTree consequence = statementTrees.get(0);
            ParseTree alternative = statementTrees.get(1);
            result.append(generateIfGoto(LABEL_ALTERNATIVE, labelNum));
            result.append(generateStatements(consequence, table));
            result.append(generateGoto(LABEL_END, labelNum));
            result.append(generateLabel(LABEL_ALTERNATIVE, labelNum));
            result.append(generateStatements(alternative, table));
            result.append(generateLabel(LABEL_END, labelNum));
        }
        else {
            // one hand if
            ParseTree consequence = statementTrees.get(0);
            result.append(generateIfGoto(LABEL_END, labelNum));
            result.append(generateStatements(consequence, table));
            result.append(generateLabel(LABEL_END, labelNum));
        }
        return result.toString();
    }

    /**
     * Generate VM code for a while statement
     * @param whileStatement
     * @param table
     * @return VM code for the while statement
     */
    public String generateWhile(ParseTree whileStatement, SymbolTable table) {
        ParseTree condition = whileStatement.getChild(EXPRESSION);
        ParseTree statements = whileStatement.getChild(STATEMENTS);
        StringBuilder result = new StringBuilder();
        int labelNum = generateLabelNumber();
        result.append(generateLabel(LABEL_LOOP, labelNum));
        result.append(generateExpression(condition, table));
        result.append("not\n");
        result.append(generateIfGoto(LABEL_LOOP_END, labelNum));
        result.append(generateStatements(statements, table));
        result.append(generateGoto(LABEL_LOOP, labelNum));
        result.append(generateLabel(LABEL_LOOP_END, labelNum));
        return result.toString();
    }


    /**
     * Generate return statement. For a return that contains expression,
     * evaluate the expression and call return. For an empty return,
     * push 0 on the stack and then return
     * @param statement
     * @param table
     * @return VM code for return statement
     */
    public String generateReturn(ParseTree statement, SymbolTable table) {
        StringBuilder result = new StringBuilder();
        if (statement.getChildren(EXPRESSION).size() > 0) {
            ParseTree exp = statement.getChild(EXPRESSION);
            result.append(generateExpression(exp, table));
        }
        else {
            result.append("push constant 0\n");
        }
        result.append("return\n");
        return result.toString();
    }

    /**
     * Generate VM code for the function call, specified by the given parse tree
     * @param call
     * @param table
     * @return VM code for the function call
     */
    public String generateSubroutineCall(ParseTree call, SymbolTable table) {
        String subroutineClassPrefix = className;
        String subroutineName;
        StringBuilder result = new StringBuilder();
        int numberOfArgs = 0;
        if (call.hasTerminalChild(".")) {
            String methodReceiver = call.getChild(0).getTerminalValue();
            subroutineName = call.getChild(2).getTerminalValue();

            // there is a variable defined for the receiver, push receiver on the stack
            Optional<SymbolTable.Entry> maybeEntry = lookupVariable(methodReceiver, table);
            if (maybeEntry.isPresent()) {
                result.append(generateVariableSegmentAccess(methodReceiver, table, "push"));
                subroutineClassPrefix = maybeEntry.get().getType();
                numberOfArgs = 1;
            }
            else {
                subroutineClassPrefix = methodReceiver;
            }
        }
        else {
            // as per language specification, calling f() without specifying class name before it
            // is equivalent to calling this.f()
            subroutineName = call.getChild(0).getTerminalValue();
            Optional<SymbolTable.Entry> maybeThis = lookupVariable("this", table);
            if (maybeThis.isPresent()) {
                result.append(generateVariableSegmentAccess("this", table, "push"));
                numberOfArgs = 1;
            }
            else {
                throw new IllegalArgumentException(String.format("Trying to call method %s from static context",
                        subroutineName));
            }
        }
        ParseTree parameters = call.getChild(EXPRESSION_LIST);
        for (ParseTree exp: parameters.getChildren(EXPRESSION)) {
            result.append(generateExpression(exp, table));
            numberOfArgs++;
        }
        result.append(String.format(
                "call %s %d\n",
                subroutineClassPrefix + "." + subroutineName,
                numberOfArgs)
        );
        return result.toString();
    }

    /**
     * Generate VM code for the let statement, specified by the given parse tree
     * @param letStatement
     * @param table
     * @return VM code for the let statement
     */
    public String generateLet(ParseTree letStatement, SymbolTable table) {
        String identifier = letStatement.getChild(IDENTIFIER).getTerminalValue();

        if (letStatement.hasTerminalChild("[")) {
            String result = "";
            result += generateExpression(letStatement.getChildren(EXPRESSION).get(1), table);
            result += "pop temp 0\n";
            result += generateExpression(letStatement.getChildren(EXPRESSION).get(0), table);
            result += generateVariableSegmentAccess(identifier, table, "push");
            result += "add\n";
            result += "pop pointer 1\n";
            result += "push temp 0\n";
            result += "pop that 0\n";
            return result;
        }
        else {
            // simple variable assignment
            String exp = generateExpression(letStatement.getChild(EXPRESSION), table);
            return exp + generateVariableSegmentAccess(identifier, table, "pop");

        }
    }

    /**
     * Generate VM code for the do statement, specified by the given parse tree
     * @param doStatement
     * @param table symbol table for the subroutine in which do is invoked
     * @return VM code for the do statement
     */
    public String generateDo(ParseTree doStatement, SymbolTable table) {
        // unfortunately, grammar specification doens't really like
        // expressions, so the function call is spilled directly at the same level
        // with do
        // To make use of expression codegen function we have to wrap function call
        // nodes in expression
        List<ParseTree> functionCallChildren = doStatement
                .getChildren()
                .stream()
                .filter(tree -> !(tree.getNodeType() == ParseTree.NodeType.TERMINAL
                        && (tree.getTerminalValue().equals("do") || tree.getTerminalValue().equals(";"))))
                .collect(Collectors.toList());
        ParseTree functionCall = new ParseTree(TERM, functionCallChildren);
        String result = generateSubroutineCall(functionCall, table);
        return result + "pop temp 0\n";
    }

    /**
     * Lookup variable by its name in given subroutine symbol table,
     * and if not found there in current class symbol table.
     * Generate looked up variable VM code that pushes or pops the topmost value to/from the stack
     * from/to the variable
     * @param variableName to find
     * @param subroutineTable to lookup first
     * @param action action to perform on
     * @return VM code that pushes variable on the stack
     */
    private String generateVariableSegmentAccess(String variableName, SymbolTable subroutineTable, String action) {
        Optional<SymbolTable.Entry> maybeEntry = lookupVariable(variableName, subroutineTable);
        if (!maybeEntry.isPresent()) {
            throw new IllegalArgumentException("Variable not found: " + variableName);
        }
        SymbolTable.Entry entry = maybeEntry.get();
        String kind;
        switch (entry.getKind()) {
            case LOCAL:
                kind = "local";
                break;
            case ARGUMENT:
                kind = "argument";
                break;
            case STATIC:
                kind = "static";
                break;
            case FIELD:
                kind = "this";
                break;
            default:
                throw new IllegalStateException("Variable of unsupported kind: " + entry.getKind());
        }
        return String.format("%s %s %d\n", action, kind, entry.getNumber());
    }

    private Optional<SymbolTable.Entry> lookupVariable(String variableName, SymbolTable subroutineTable) {
        SymbolTable.Entry entry = subroutineTable.contains(variableName)
                ? subroutineTable.get(variableName)
                : classTable.get(variableName);
        return (entry != null) ? Optional.of(entry) : Optional.empty();
    }


    private int generateLabelNumber() {
        labelCount++;
        return labelCount;
    }

    private String generateStatements(ParseTree statements, SymbolTable table) {
        StringBuilder result = new StringBuilder();
        for (ParseTree statement: statements.getChildren()) {
            if (statement.getChild(0).getTerminalValue().equals("if")) {
                result.append(generateIf(statement, table));
            }
            else if (statement.getChild(0).getTerminalValue().equals("while")) {
                result.append(generateWhile(statement, table));
            }
            else if (statement.getChild(0).getTerminalValue().equals("return")) {
                result.append(generateReturn(statement, table));
            }
            else if (statement.getChild(0).getTerminalValue().equals("do")) {
                result.append(generateDo(statement, table));
            }
            else if (statement.getChild(0).getTerminalValue().equals("let")) {
                result.append(generateLet(statement, table));
            }
            else {
                throw new IllegalArgumentException("Unsupported statement: "
                        + statement.getChild(0).getTerminalValue());
            }
        }
        return result.toString();
    }

    private String generateLabel(String labelName, int labelNum) {
        return String.format("label %s%d\n", labelName, labelNum);
    }

    private String generateIfGoto(String labelName, int labelNum) {
        return String.format("if-goto %s%d\n", labelName, labelNum);
    }

    private String generateGoto(String labelName, int labelNum) {
        return String.format("goto %s%d\n", labelName, labelNum);
    }
}
