package com.nvm.nand.compiler.parsing;

public class ParseException extends Exception {

    public ParseException(String message) {
        super(message);
    }

    public ParseException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public ParseException(Throwable throwable) {
        super(throwable);
    }
}
