package com.nvm.nand.compiler.parsing;

import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Parse tree representing structure of some valid (as defined by the grammar below) Jack program code.
 *
 * Jack Grammar (the quoted strings are terminals)
 *
 * - Lexical elements -
 * keyword: 'class' | 'constructor' | 'function' | 'method' | 'field' | 'static' | 'var' |
 *          'int' | 'char' | 'boolean' | 'void' | 'true' | 'false' | 'null' | 'this' | 'let' | 'do' |
 *          'if' | 'else' | 'while' | 'return'
 * symbol: '{' | '}' | '(' | ')' | '[' | ']' | '.' | ',' | ';'
 * integerConstant: A decimal number in the range 0 .. 32767
 * stringConstant: '"' A sequence of Unicode characters not including double quote or newline '"'
 * identifier: A sequence of letters, digits, and underscore ( '_' ) not starting with a digit.
 *
 * - Program structure -
 * class: 'class' className '{' classVariableDeclaration* subroutineDeclaration* '}'
 * className: identifier
 * classVariableDeclaration: ('static' | 'field') nodeType varName (',' varName)* ';'
 * nodeType: 'int' | 'char' | 'boolean' | className
 * subroutineDeclaration: ('constructor' | 'function' | 'method') ('void' | nodeType)
 *                          subroutineName '(' parameterList ')' subroutineBody
 * subroutineName: identifier
 * parameterList: ((nodeType varName) (',' nodeType varName)*)?
 * subroutineBody: '{' variableDeclaration* statements '}'
 * variableDeclaration: 'var' nodeType varName (',' varName)* ';'
 * varName: identifier
 *
 * - Statements -
 * statements: statement*
 * statement: letStatement | ifStatement | whileStatement | doStatement | returnStatement
 * letStatement: 'let' varName ('[' expression ']')? '=' expression ';'
 * ifStatement: 'if' '(' expression ')' '{' statements '}' ('else' '{' statements '}')?
 * whileStatement: 'while' '(' expression ')' '{' statements '}'
 * doStatement: 'do' subroutineCall ';'
 * returnStatement: 'return' expression? ';'
 *
 * - Expressions -
 * expression: term (op term)*
 * term: integerConstant | stringConstant | keywordConstant | varName | varName '[' expression ']' | subroutineCall |
 *       '(' expression ')' | unaryOp term
 * subroutineCall: subroutineName '(' expressionList ')' | (className | varName) '.' subroutineName '(' expressionList ')'
 * expressionList: (expression (',' expression)*)?
 * op: '+' | '-' | '*' | '/' | '&' | '|' | '<' | '>' | '='
 * unaryOp: '-' | '~'
 * keywordConstant: 'true' | 'false' | 'null' | 'this'
 */
public class ParseTree {
    
    public enum Name {KEYWORD, IDENTIFIER, SYMBOL, INT_CONST, STR_CONST, CLASS, CLASS_VAR, SUBROUTINE_DECLARATION,
        PARAMETERS_LIST, SUBROUTINE_BODY, VARIABLE_DECLARATION, STATEMENTS, WHILE_STATEMENT,
        IF_STATEMENT, RETURN_STATEMENT, LET_STATEMENT, DO_STATEMENT, EXPRESSION, EXPRESSION_LIST, TERM}

    /**
     * Terminals are leaves of the tree, they have terminal value but don't have children
     * Non-terminals are nodes of the tree, they always have children but never a value
     * Empty nodeType is non-terminal that has zero children, eg empty list of expressions
     * I wish there were algebraic types in java tbh
     */
    public enum NodeType {TERMINAL, NON_TERMINAL, EMPTY}

    private Name name;
    private NodeType nodeType;
    private String terminalValue;
    private List<ParseTree> children;

    public ParseTree(Name name, NodeType nodeType, String terminalValue, List<ParseTree> children) {
        this.name = name;
        this.nodeType = nodeType;
        this.terminalValue = terminalValue;
        this.children = children;
    }

    public ParseTree(Name name, NodeType nodeType) {
        this(name, nodeType, null, null);
    }

    public ParseTree(Name name, List<ParseTree> children) {
        this(name, NodeType.NON_TERMINAL, null, children);
    }

    // todo: use this constructor for short lists
    public ParseTree(Name name, ParseTree... children) {
        this(name, NodeType.NON_TERMINAL, null, Arrays.asList(children));
    }

    public ParseTree(Name name, String terminalValue) {
        this(name, NodeType.TERMINAL, terminalValue, null);
    }

    /**
     * @return name of the production rule used to parse the content of this tree
     */
    public Name getName() {
        return name;
    }

    /**
     * @return nodeType of the tree: terminal or non-terminal
     */
    public NodeType getNodeType() {
        return nodeType;
    }

    /**
     * Get terminal value that this tree represents. Only defined for terminal trees (leaves)
     * @return terminal value that this tree represents
     */
    public String getTerminalValue() {
        return terminalValue;
    }

    /**
     * Get subtrees of this tree in order they were given in the input. Only defined for non-terminal trees
     * @return subtrees of this tree
     */
    public List<ParseTree> getChildren() {
        switch (nodeType) {
            case NON_TERMINAL:
                return children;
            case EMPTY:
                return Arrays.asList();
            default:
            case TERMINAL:
                throw new IllegalArgumentException("Can't retrieve child of a terminal tree");
        }
    }

    /**
     * Get first found child with given name among the direct children of this tree
     * @param name of the child to find
     * @return found child
     */
    public ParseTree getChild(Name name) {
        if (nodeType != NodeType.NON_TERMINAL) {
            throw new IllegalArgumentException("Can't retrieve child of a terminal tree");
        }
        Optional<ParseTree> result =
                getChildren()
                        .stream()
                        // get subroutine body
                        .filter(child -> child.getName() == name)
                        .findFirst();
        if (result.isPresent()) {
            return result.get();
        }
        else {
            throw new NoSuchElementException("Child is not found: " + name);
        }
    }

    /**
     * Check if this parse tree has a terminal child with given value
     * @param value to look for
     * @return true if there is a child with given terminal value
     */
    public boolean hasTerminalChild(String value) {
        if (nodeType != NodeType.NON_TERMINAL) {
            throw new IllegalArgumentException("Can't retrieve child of a terminal tree");
        }
        Optional<ParseTree> result =
                getChildren()
                        .stream()
                        // get subroutine body
                        .filter(child -> child.getNodeType() == NodeType.TERMINAL)
                        .filter(child -> child.getTerminalValue().equals(value))
                        .findFirst();
        return result.isPresent();
    }

    /**
     * Get n-th child of this tree
     * @param n number of the child among all children
     * @return found child
     */
    public ParseTree getChild(int n) {
        return getChildren().get(n);
    }

    /**
     * Get all direct children of this tree that have specified name
     * @param name of the children to find
     * @return found children
     */
    public List<ParseTree> getChildren(Name name) {
        return getChildren()
                .stream()
                .filter(child -> child.getName() == name)
                .collect(Collectors.toList());
    }
}
