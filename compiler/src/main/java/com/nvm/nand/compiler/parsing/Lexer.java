package com.nvm.nand.compiler.parsing;

import com.nvm.nand.compiler.parsing.Token.Type;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Lexer turns sequence of characters into a sequence of tokens
 */
public class Lexer {

    private static Pattern KEYWORDS = Pattern.compile("^(" + String.join("|", "class", "constructor", "function", "method", "field",
            "static", "var", "int", "char", "boolean", "void", "true", "false", "null", "this", "let", "do",
            "if", "else", "while", "return") + ")\\b");

    private static Pattern SYMBOLS = Pattern.compile("^(" + String.join("|", "\\{", "\\}", "\\(", "\\)", "\\[",
            "\\]", "\\.", ",", ";", "\\+", "-", "\\*", "/", "&", "\\|", "<", ">", "=", "~") + ")");

    private static Pattern INTEGER_CONSTANT = Pattern.compile("^\\d+");

    private static Pattern IDENTIFIER = Pattern.compile("^(?<!\")[_a-zA-Z][_0-9a-zA-Z]*(?!\")");

    private static Pattern STRING_CONSTANT = Pattern.compile("^\"([^\\n]*)\"");

    private static Pattern COMMENT_ONELINE = Pattern.compile("^//[^\\n]*");

    private static Pattern COMMENT_MULTILINE = Pattern.compile("^/\\*(?>.*?\\*/)", Pattern.DOTALL);

    private static Pattern WHITESPACE = Pattern.compile("^\\s+");

    private static List<Pattern> patterns = (Arrays.asList(WHITESPACE, COMMENT_ONELINE, COMMENT_MULTILINE,
            KEYWORDS, SYMBOLS, STRING_CONSTANT, INTEGER_CONSTANT, IDENTIFIER));


    private String inputProgram;
    private int currentPosition, totalLength;

    public Lexer(String inputProgram) {
        this.inputProgram = inputProgram;
        this.totalLength = inputProgram.length();
    }

    /**
     * Turn given program text into sequence of tokens, ignoring whitespace (spaces, newlines, comments, tabs)
     * @param programText text to be tokenized
     * @return list of tokens representing given program text
     */
    public static List<Token> tokenize(String programText) {
        return new Lexer(programText).tokenizeInput();
    }

    private List<Token> tokenizeInput() {
        List<Token> result = new ArrayList<>();
        while (canAdvance()) {
            Token next = advance();
            if (next.getType() != Type.WHITESPACE) {
                result.add(next);
            }
        }
        return result;
    }

    /**
     * Advance tokenizer one token further, consuming the part of input text that represent the token
     * @return next matched token in the program text
     */
    private Token advance() {
        for (Pattern pattern: patterns) {
            Matcher matcher = pattern.matcher(inputProgram);
            if (matcher.find()) {
                String matched = matcher.group(0);
                currentPosition += matched.length();
                inputProgram = inputProgram.substring(matcher.end());
                // todo: refactor, map patterns to token types somewhere and just call it here
                if (pattern == WHITESPACE || pattern == COMMENT_MULTILINE || pattern == COMMENT_ONELINE) {
                    return new Token(Type.WHITESPACE, matched);
                }
                if (pattern == STRING_CONSTANT) {
                    // omit quotes
                    return new Token(Type.STR_CONSTANT, matcher.group(1));
                }
                if (pattern == SYMBOLS) {
                    return new Token(Type.SYMBOL, matched);
                }
                if (pattern == INTEGER_CONSTANT) {
                    return new Token(Type.INT_CONSTANT, matched);
                }
                if (pattern == KEYWORDS) {
                    return new Token(Type.KEYWORD, matched);
                }
                if (pattern == IDENTIFIER) {
                    return new Token(Type.IDENTIFIER, matched);
                }
            }
        }
        throw new RuntimeException("Can't match input");
    }

    /**
     * @return true if there's still remains text to be consumed
     */
    private boolean canAdvance() {
        return currentPosition < totalLength;
    }

    public static void main(String[] args) {

        Lexer.tokenize("12345 123");
    }
}
