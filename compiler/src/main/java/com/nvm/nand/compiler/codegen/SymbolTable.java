package com.nvm.nand.compiler.codegen;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Symbol table maintains list of variables defined within a class
 * or within a function.
 * Variables are grouped by kind: functions can have local and argument variables
 * and classes can have static and field variables.
 * Each variable within a kind is assigned a number, that will be used to map the variable
 * on a memory segment in VM code. Numbers are assigned in order the variables are inserted
 * starting from 0
 */
public class SymbolTable {

    private Map<String, Entry> entries;

    private Map<Entry.Kind, Integer> kindCount;

    public SymbolTable() {
        entries = new HashMap<>();
        kindCount = new HashMap<>();
    }

    /**
     * @param variableName name of the variable
     * @param kind of the variable
     * @return true if there's an entry in the symbol table for the variable with given name and kind
     */
    public boolean contains(String variableName, Entry.Kind kind) {
        return entries.containsKey(variableName) && entries.get(variableName).getKind() == kind;
    }

    /**
     * @param variableName name of the variable
     * @return true if there's an entry in the symbol table for the variable with given name
     */
    public boolean contains(String variableName) {
        return entries.containsKey(variableName);
    }

    /**
     * Get number of the variable, which was assigned to it when the variable was added to the table.
     * Assume the variable is present in the table
     * @param variableName name of the variable
     * @return number of the variable with given name
     */
    public int getNumber(String variableName) {
        return get(variableName).getNumber();
    }

    /**
     * Get number of the variable, which was assigned to it when the variable was added to the table.
     * Assume the variable is present in the table
     * @param variableName name of the variable
     * @return kind of the variable with given name
     */
    public Entry.Kind getKind(String variableName) {
        return get(variableName).getKind();
    }

    /**
     * Given variable name get entry the table
     * Local variables shadow arguments, so if there are local variables with the same name as arguments,
     * the local variable will be returned
     * @param variableName name of the variable
     * @return entry for the variable with the given name
     */
    public Entry get(String variableName) {
        return entries.get(variableName);
    }

    /**
     * Add variable to the table and assign a number to it.
     * If the variable is i-th among it's kind, then the assigned number is i.
     * There can't be more than one variable of the same name and kind
     * @param name of the variable
     * @param type of the variable
     * @param kind for the variable, like field or static
     * @throws IllegalArgumentException if the table already has variable of this kind and type
     */
    public void put(String name, String type, Entry.Kind kind) {
        int count = kindCount.getOrDefault(kind, 0);
        addEntry(name, type, kind, count);
        kindCount.put(kind, count+1);
    }

    private void addEntry(String name, String type, Entry.Kind kind, int number) {
        if (contains(name, kind)) {
            throw new IllegalArgumentException(String.format("Variable %s of kind %s already exists in the table",
                    name, kind));
        }
        entries.put(name, new Entry(name, type, kind, number));
    }

    /**
     * @return number of entries in the table
     */
    public int size() {
        return entries.size();
    }

    /**
     * get number of entries of the specified kind in the table
     * @param kind
     * @return
     */
    public long getNumberOfKind(Entry.Kind kind) {
        return entries.values().stream()
                .filter(entry -> entry.getKind() == kind)
                .count();
    }

    /**
     * Add all entries from given symbol table into this table
     * Duplicate entries are not allowed
     * @param other table to take values from
     * @return new symbol table that is the combination of this table and other table
     * @throws IllegalArgumentException if other table contains an entry that is present in current table
     */
    public SymbolTable addAll(SymbolTable other) {
        SymbolTable result = new SymbolTable();
        result.addAllHere(this);
        result.addAllHere(other);
        return result;
    }

    /**
     * Add all entries from given symbol table into this table
     * Duplicate entries are not allowed
     * @param other table to take values from
     * @throws IllegalArgumentException if other table contains an entry that is present in current table
     */
    private void addAllHere(SymbolTable other) {
        for (Map.Entry<String, Entry> mapEntry: other.entries.entrySet()) {
            String name = mapEntry.getValue().getName();
            String type = mapEntry.getValue().getType();
            Entry.Kind kind = mapEntry.getValue().getKind();
            int number = mapEntry.getValue().getNumber();
            addEntry(name, type, kind, number);
        }
    }

    /**
     * Represent a single entry in a symbol table
     */
    public static class Entry {

        public enum Kind {STATIC, FIELD, ARGUMENT, LOCAL}

        private String name;

        private String type;

        private Kind kind;

        private int number;

        public Entry(String name, String type, Kind kind, int number) {
            this.name = name;
            this.type = type;
            this.kind = kind;
            this.number = number;
        }

        public String getName() {
            return name;
        }

        public String getType() {
            return type;
        }

        public Kind getKind() {
            return kind;
        }

        public int getNumber() {
            return number;
        }
    }
}
