package com.nvm.nand.compiler;


import com.nvm.nand.compiler.codegen.Generator;
import com.nvm.nand.compiler.parsing.*;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Main {

    private static String translateFile(File file) {
        try {
            String program = new String(Files.readAllBytes(Paths.get(file.getAbsolutePath())));
            ParseTree classTree = new Parser(Lexer.tokenize(program)).parseClass();
            String generated = new Generator().generateClass(classTree);
            String xml = XmlWriter.generate(classTree);
            return generated;
        }
        catch (IOException|ParseException ex) {
            throw new RuntimeException(ex);
        }
    }

    /**
     * Write assembly program text to the file specified by filename
     * Prepend the code by vm bootstrap code
     */
    private static void writeFile(String text, File file) {
        String baseFilename = stripExtension(file.getName());
        String outName = baseFilename + ".vm";
        try {
            PrintWriter writer = new PrintWriter(outName, "UTF-8");
            writer.println(text);
            writer.close();
        }
        catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    private static boolean isJackFile(String filename) {
        return filename.toLowerCase().endsWith(".jack");
    }

    private static String stripExtension(String filename) {
        return filename.substring(0, filename.indexOf(".jack"));
    }

    public static void main(String[] args) {
        if (args.length < 1) {
            System.err.println("Provide jack file or a directory name as an argument");
        }

        String filename = args[0];
        File input = new File(filename);
        if (input.isDirectory()) {
            for (File file: input.listFiles()) {
                if (isJackFile(file.getName())) {
                    writeFile(translateFile(file), file);
                }
            }

        }
        else if (isJackFile(filename)) {
            writeFile(translateFile(input), input);
        }
        else {
            System.err.println("Invalid argument: not a directory and not a .jack file");
        }

    }
}
