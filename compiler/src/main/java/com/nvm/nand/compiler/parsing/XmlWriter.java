package com.nvm.nand.compiler.parsing;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import java.io.*;


public class XmlWriter {

    public static String generate(ParseTree tree) {
        StringWriter writer = new StringWriter();
        generateXml(tree, writer);
        return writer.toString();
    }

    public static void writeToFile(ParseTree tree, File file) throws IOException {
        FileWriter writer = new FileWriter(file);
        generateXml(tree, writer);
    }

    /**
     * Generate xml for given parse tree and output into given writer
     * @param tree from which xml will be built
     * @param writer into which the output shouldbe written
     */
    private static void generateXml(ParseTree tree, Writer writer) {
        try {
            DocumentBuilderFactory dbFactory =
                    DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.newDocument();
            Element rootElement = parseTreeToXml(tree, doc);
            doc.appendChild(rootElement);
            TransformerFactory factory = TransformerFactory.newInstance();
            factory.setAttribute("indent-number", 2);
            Transformer transformer = factory.newTransformer();
            transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
            transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.transform(new DOMSource(doc), new StreamResult(writer));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Turn given parse tree into dom element
     * @param tree tree to process
     * @param document within which the element will be created
     * @return DOM element corresponding to given tree
     */
    private static Element parseTreeToXml(ParseTree tree, Document document) {
        Element result = document.createElement(getElementName(tree.getName()));
        if (tree.getNodeType() == ParseTree.NodeType.TERMINAL) {
            result.appendChild(document.createTextNode(String.format(" %s ", tree.getTerminalValue())));
        }
        else if (tree.getNodeType() == ParseTree.NodeType.NON_TERMINAL) {
            tree.getChildren().stream()
                    .map(child -> parseTreeToXml(child, document))
                    .forEach(childElement -> result.appendChild(childElement));
        }
        return result;
    }

    private static String getElementName(ParseTree.Name elementName) {
        switch (elementName) {
            case KEYWORD:
                return "keyword";
            case IDENTIFIER:
                return "identifier";
            case SYMBOL:
                return "symbol";
            case INT_CONST:
                return "integerConstant";
            case STR_CONST:
                return "stringConstant";
            case CLASS:
                return "class";
            case CLASS_VAR:
                return "classVarDec";
            case SUBROUTINE_DECLARATION:
                return "subroutineDec";
            case PARAMETERS_LIST:
                return "parameterList";
            case SUBROUTINE_BODY:
                return "subroutineBody";
            case VARIABLE_DECLARATION:
                return "varDec";
            case STATEMENTS:
                return "statements";
            case WHILE_STATEMENT:
                return "whileStatement";
            case IF_STATEMENT:
                return "ifStatement";
            case RETURN_STATEMENT:
                return "returnStatement";
            case LET_STATEMENT:
                return "letStatement";
            case DO_STATEMENT:
                return "doStatement";
            case EXPRESSION:
                return "expression";
            case EXPRESSION_LIST:
                return "expressionList";
            case TERM:
                return "term";
            default:
                throw new IllegalArgumentException();
        }
    }
}
