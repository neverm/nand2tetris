package com.nvm.nand.compiler.codegen;

import java.util.Arrays;

public class Utils {

    public static String commandSeq(String... commands) {
        return String.join("\n", Arrays.asList(commands)) + "\n";
    }
}
