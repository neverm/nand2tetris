package com.nvm.nand.compiler.parsing;

public class Token {

    public enum Type {KEYWORD, SYMBOL, INT_CONSTANT, STR_CONSTANT, IDENTIFIER, WHITESPACE}


    private Type type;
    private String value;

    public Token(Type type, String value) {
        this.type = type;
        this.value = value;
    }

    public Type getType() {
        return type;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return String.format("Token %s, value: %s", type, value);
    }
}
