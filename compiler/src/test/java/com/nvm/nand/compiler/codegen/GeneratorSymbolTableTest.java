package com.nvm.nand.compiler.codegen;

import com.nvm.nand.compiler.codegen.SymbolTable.Entry;
import com.nvm.nand.compiler.codegen.SymbolTable.Entry.Kind;
import com.nvm.nand.compiler.parsing.ParseTree;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;

import static com.nvm.nand.compiler.codegen.ParseTreeMaker.*;
import static org.junit.Assert.*;

public class GeneratorSymbolTableTest {

    private Generator generator;

    @Before
    public void init() {
        generator = new Generator();
    }

    @Test
    public void testEmptyClass() {
        ParseTree emptyClass = makeClassDec("A", new ArrayList<>(), new ArrayList<>());
        SymbolTable t = generator.generateClassTable(emptyClass);
        assertEquals(0, t.size());
    }

    @Test
    public void testOneFieldClass() {
        ParseTree varDec = makeClassVariablesDec("field", "int", "x");
        ParseTree emptyClass = makeClassDec("A", Arrays.asList(varDec), new ArrayList<>());
        SymbolTable t = generator.generateClassTable(emptyClass);
        Entry e = t.get("x");
        assertEquals(Kind.FIELD, e.getKind());
        assertEquals("x", e.getName());
        assertEquals("int", e.getType());
        assertEquals(0, e.getNumber());
    }

    @Test
    public void testTwoFieldClass() {
        ParseTree varDec1 = makeClassVariablesDec("field", "int", "x");
        ParseTree varDec2 = makeClassVariablesDec("field", "int", "y");
        ParseTree emptyClass = makeClassDec("A", Arrays.asList(varDec1, varDec2), new ArrayList<>());
        SymbolTable t = generator.generateClassTable(emptyClass);
        assertEquals(2, t.size());
        Entry e = t.get("y");
        assertEquals(Kind.FIELD, e.getKind());
        assertEquals("y", e.getName());
        assertEquals("int", e.getType());
        assertEquals(1, e.getNumber());
    }

    @Test
    public void testStaticAndFieldClass() {
        ParseTree varDec1 = makeClassVariablesDec("field", "int", "x");
        ParseTree varDec2 = makeClassVariablesDec("static", "int", "y");
        ParseTree emptyClass = makeClassDec("A", Arrays.asList(varDec1, varDec2), new ArrayList<>());
        SymbolTable t = generator.generateClassTable(emptyClass);
        Entry e = t.get("y");
        assertEquals(Kind.STATIC, e.getKind());
        assertEquals("y", e.getName());
        assertEquals("int", e.getType());
        assertEquals(0, e.getNumber());
    }

    @Test
    public void testEmptySubroutine() {
        ParseTree subroutine = makeSubroutineDec("f", "function", makeType("int"),
                new ArrayList<>(), new ArrayList<>(), new ArrayList<>());
        SymbolTable t = generator.generateSubroutineTable(subroutine);
        assertEquals(0, t.size());
    }

    @Test
    public void testSubroutineOneLocalNoArgs() {
        ParseTree varDec = makeSubroutineVariablesDec("int", "x");
        ParseTree subroutine = makeSubroutineDec("f", "function", makeType("int"),
                new ArrayList<>(), Arrays.asList(varDec), new ArrayList<>());
        SymbolTable t = generator.generateSubroutineTable(subroutine);
        assertEquals(1, t.size());
    }

    @Test
    public void testSubroutineWithArgs() {
        ParseTree subroutine = makeSubroutineDec("f", "function", makeType("int"),
                Arrays.asList(new Variable("x", "int"), new Variable("y", "int")),
                new ArrayList<>(), new ArrayList<>());
        SymbolTable t = generator.generateSubroutineTable(subroutine);
        assertEquals(2, t.size());
        assertEquals(0, t.getNumber("x"));
        assertEquals(Kind.ARGUMENT, t.getKind("x"));
        assertEquals(1, t.getNumber("y"));
        assertEquals(Kind.ARGUMENT, t.getKind("y"));
    }

    @Test
    public void testSubroutineWithArgsAndLocals() {
        ParseTree varDec = makeSubroutineVariablesDec("int", "z");
        ParseTree subroutine = makeSubroutineDec("f", "function", makeType("int"),
                Arrays.asList(new Variable("x", "int"), new Variable("y", "int")),
                Arrays.asList(varDec), new ArrayList<>());
        SymbolTable t = generator.generateSubroutineTable(subroutine);
        assertEquals(3, t.size());
        assertEquals(0, t.getNumber("x"));
        assertEquals(Kind.ARGUMENT, t.getKind("x"));
        assertEquals(1, t.getNumber("y"));
        assertEquals(Kind.ARGUMENT, t.getKind("y"));
        assertEquals(0, t.getNumber("z"));
        assertEquals(Kind.LOCAL, t.getKind("z"));
    }

    @Test
    public void testSubroutineWithArgShadowedByLocal() {
        ParseTree varDec = makeSubroutineVariablesDec("int", "x");
        ParseTree subroutine = makeSubroutineDec("f", "function", makeType("int"),
                Arrays.asList(new Variable("x", "int")),
                Arrays.asList(varDec), new ArrayList<>());
        SymbolTable t = generator.generateSubroutineTable(subroutine);
        assertEquals(1, t.size());
        assertEquals(0, t.getNumber("x"));
        assertEquals(Kind.LOCAL, t.getKind("x"));
    }

    @Test
    public void testMethodNoArgs() {
        ParseTree subroutine = makeSubroutineDec("f", "method", makeType("int"),
                new ArrayList<>(), new ArrayList<>(), new ArrayList<>());
        SymbolTable t = generator.generateSubroutineTable(subroutine);
        assertEquals(1, t.size());
        assertEquals(0, t.getNumber("this"));
        assertEquals(Kind.ARGUMENT, t.getKind("this"));
    }

    @Test
    public void testMethodOneArg() {
        ParseTree subroutine = makeSubroutineDec("f", "method", makeType("int"),
                Arrays.asList(new Variable("x", "int")),
                new ArrayList<>(), new ArrayList<>());
        SymbolTable t = generator.generateSubroutineTable(subroutine);
        assertEquals(2, t.size());
        assertEquals(0, t.getNumber("this"));
        assertEquals(Kind.ARGUMENT, t.getKind("this"));
        assertEquals(1, t.getNumber("x"));
        assertEquals(Kind.ARGUMENT, t.getKind("x"));
    }
}
