package com.nvm.nand.compiler.parsing.parser;

import com.nvm.nand.compiler.parsing.ParseTree;
import com.nvm.nand.compiler.parsing.Token;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;

public class TestUtils {

    public static List<Token.Type> types(Token.Type... types) {
        return Arrays.asList(types);
    }

    public static List<String> values(String... values) {
        return Arrays.asList(values);
    }

    /**
     * Create list of tokens from given lists of types and values. Requires the two arguments to be of the same
     * length.
     * @param types
     * @param values
     * @return list of tokens produced from the given lists
     */
    public static List<Token> tokens(List<Token.Type> types, List<String> values) {
        if (types.size() != values.size()) {
            throw new IllegalArgumentException(
                    String.format("Lengths of types and values are not equal: types: %d, values: %d",
                            types.size(), values.size()));
        }
        // ugly but what can you do about it there are neither map over multiple lists nor zip function
        Iterator<String> it = values.iterator();
        return types.stream()
                .map(type -> new Token(type, it.next()))
                .collect(Collectors.toList());
    }

    /**
     * Assert that given tree has given properties
     * @param tree tree to run assertions against
     * @param expectedNodeType expected type of the tree
     * @param expectedName expected name of the tree
     */
    public static void assertTree(ParseTree tree, ParseTree.NodeType expectedNodeType, ParseTree.Name expectedName) {
        assertEquals(expectedNodeType, tree.getNodeType());
        assertEquals(expectedName, tree.getName());
    }

    /**
     * Assert that given tree has given properties
     * @param tree tree to run assertions against
     * @param expectedNodeType expected type of the tree
     * @param expectedName expected name of the tree
     * @param expectedTerminalValue expected value of the terminal in this tree node
     */
    public static void assertTree(ParseTree tree, ParseTree.NodeType expectedNodeType, ParseTree.Name expectedName,
                                  String expectedTerminalValue) {
        assertTree(tree, expectedNodeType, expectedName);
        assertEquals(expectedTerminalValue, tree.getTerminalValue());
    }
}
