package com.nvm.nand.compiler.codegen;

import com.nvm.nand.compiler.parsing.ParseTree;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.Optional;

import static com.nvm.nand.compiler.codegen.ParseTreeMaker.*;
import static com.nvm.nand.compiler.codegen.Utils.commandSeq;
import static junit.framework.TestCase.assertEquals;

/**
 * Test functions, methods and constructors.
 * Void functions are tested in do statement tests
 */
public class FunctionDeclarationTest {

    private Generator generator;

    @Before
    public void init() {
        generator = new Generator();
        generator.setClassName("ClassName");
        generator.setClassTable(new SymbolTable());
    }

    @Test
    public void testEmptyDeclaration() {
        ParseTree dec = makeSubroutineDec(
                "f",
                "function",
                makeKeyword("void"),
                Arrays.asList(),
                Arrays.asList(),
                Arrays.asList(makeReturn())
        );
        String expected = commandSeq(
                "function ClassName.f 0",
                "push constant 0",
                "return"
        );
        assertEquals(expected, generator.generateSubroutine(dec));
    }

    @Test
    public void testLocalVariableDeclaration() {
        ParseTree dec = makeSubroutineDec(
                "f",
                "function",
                makeKeyword("void"),
                Arrays.asList(),
                Arrays.asList(makeSubroutineVariablesDec("int", "a", "b")),
                Arrays.asList(makeReturn())
        );
        String expected = commandSeq(
                "function ClassName.f 2",
                "push constant 0",
                "return"
        );
        assertEquals(expected, generator.generateSubroutine(dec));
    }

    @Test
    public void testNonZeroArguments() {
        ParseTree dec = makeSubroutineDec(
                "f",
                "function",
                makeKeyword("void"),
                Arrays.asList(new Variable("x", "int")),
                Arrays.asList(),
                Arrays.asList(makeReturn())
        );
        String expected = commandSeq(
                "function ClassName.f 0",
                "push constant 0",
                "return"
        );
        assertEquals(expected, generator.generateSubroutine(dec));
    }

    @Test
    public void testNonVoidReturn() {
        ParseTree dec = makeSubroutineDec(
                "f",
                "function",
                makeKeyword("int"),
                Arrays.asList(new Variable("x", "int")),
                Arrays.asList(),
                Arrays.asList(makeReturn(makeExp(makeInt("1"))))
        );
        String expected = commandSeq(
                "function ClassName.f 0",
                "push constant 1",
                "return"
        );
        assertEquals(expected, generator.generateSubroutine(dec));
    }

    @Test
    public void testBodySingleStatement() {
        ParseTree dec = makeSubroutineDec(
                "f",
                "function",
                makeKeyword("void"),
                Arrays.asList(),
                Arrays.asList(makeSubroutineVariablesDec("int", "a")),
                Arrays.asList(
                        makeLetVariable("a", makeExp(makeInt("1"))),
                        makeReturn()
                )
        );
        String expected = commandSeq(
                "function ClassName.f 1",
                "push constant 1",
                "pop local 0",
                "push constant 0",
                "return"
        );
        assertEquals(expected, generator.generateSubroutine(dec));
    }

    @Test
    public void testBodyMultipleStatements() {
        ParseTree dec = makeSubroutineDec(
                "f",
                "function",
                makeKeyword("void"),
                Arrays.asList(),
                Arrays.asList(makeSubroutineVariablesDec("int", "a")),
                Arrays.asList(
                        makeLetVariable("a", makeExp(makeInt("1"))),
                        makeLetVariable("a", makeExp(makeInt("1"))),
                        makeReturn()
                )
        );
        String expected = commandSeq(
                "function ClassName.f 1",
                "push constant 1",
                "pop local 0",
                "push constant 1",
                "pop local 0",
                "push constant 0",
                "return"
        );
        assertEquals(expected, generator.generateSubroutine(dec));
    }

    @Test
    public void testMethod() {
        ParseTree dec = makeSubroutineDec(
                "f",
                "method",
                makeKeyword("void"),
                Arrays.asList(),
                Arrays.asList(),
                Arrays.asList(makeReturn())
        );
        String expected = commandSeq(
                "function ClassName.f 0",
                "push argument 0",
                "pop pointer 0",
                "push constant 0",
                "return"
        );
        assertEquals(expected, generator.generateSubroutine(dec));
    }

    @Test
    public void testReturnComplexExpression() {
        ParseTree dec = makeSubroutineDec(
                "f",
                "function",
                makeKeyword("int"),
                Arrays.asList(),
                Arrays.asList(),
                Arrays.asList(
                        makeReturn(
                                makeExp(makeInt("1"),
                                        "+",
                                        makeInt("1")))
                )
        );
        String expected = commandSeq(
                "function ClassName.f 0",
                "push constant 1",
                "push constant 1",
                "add",
                "return"
        );
        assertEquals(expected, generator.generateSubroutine(dec));
    }
}
