package com.nvm.nand.compiler.codegen;


import com.nvm.nand.compiler.parsing.ParseTree;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.nvm.nand.compiler.parsing.ParseTree.Name.*;

/**
 * Helper class to create parse trees that emulate trees that come from a real parser, to facilitate testing
 */
public class ParseTreeMaker {

    /**
     * Represent a variable which can be of a primitive or a class type, in which case it also holds
     * information about the class name
     */
    public static class Variable {

        private String type;

        private String name;

        public Variable(String name, String type) {
            this.name = name;
            this.type = type;
        }

        public String getName() {
            return name;
        }

        public String getType() {
            return type;
        }
    }

    public static ParseTree makeClassDec(String className, List<ParseTree> variableDeclarations,
                                         List<ParseTree> subroutineDeclarations) {
        List<ParseTree> children = new ArrayList<>();
        children.add(makeKeyword("class"));
        children.add(makeIdentifier(className));
        children.add(makeSymbol("{"));
        children.addAll(variableDeclarations);
        children.addAll(subroutineDeclarations);
        children.add(makeSymbol("}"));
        return new ParseTree(CLASS, children);
    }

    /**
     * Make a subroutine declaration parse tree
     * @param name of the subroutine
     * @param subroutineModifier assume is one of: constructor, function, method
     * @param returnType could be either a keyword type or a class identifier
     * @param args list of arguments, assume all variables are of ARGUMENT kind
     * @param variableDeclarations this subroutine variable declarations
     * @param statements that constitute subroutine body
     * @return subroutine parse tree
     */
    public static ParseTree makeSubroutineDec(String name, String subroutineModifier, ParseTree returnType,
                                              List<Variable> args, List<ParseTree> variableDeclarations,
                                              List<ParseTree> statements) {
        List<ParseTree> children = new ArrayList<>();
        children.add(makeKeyword(subroutineModifier));
        children.add(returnType);
        children.add(makeIdentifier(name));
        children.add(makeSymbol("("));
        List<ParseTree> argumentsTreeChildren = new ArrayList<>();
        // todo: refactor to a join method
        if (args.size() > 0) {
            for (Variable v : args) {
                argumentsTreeChildren.add(makeType(v.getType()));
                argumentsTreeChildren.add(makeIdentifier(v.getName()));
                argumentsTreeChildren.add(makeSymbol(","));
            }
            argumentsTreeChildren.remove(argumentsTreeChildren.size() - 1); //remove last comma
        }
        children.add(new ParseTree(PARAMETERS_LIST, argumentsTreeChildren));
        children.add(makeSymbol(")"));
        List<ParseTree> bodyTreeChildren = new ArrayList<>();
        bodyTreeChildren.add(makeSymbol("{"));
        bodyTreeChildren.addAll(variableDeclarations);
        bodyTreeChildren.add(new ParseTree(STATEMENTS, statements));
        bodyTreeChildren.add(makeSymbol("}"));
        children.add(new ParseTree(SUBROUTINE_BODY, bodyTreeChildren));
        return new ParseTree(SUBROUTINE_DECLARATION, children);
    }

    public static ParseTree makeIf(ParseTree condExp, List<ParseTree> consequence, List<ParseTree> alternative) {
        return makeIf(condExp, consequence, Optional.of(alternative));
    }

    public static ParseTree makeIf(ParseTree condExp, List<ParseTree> consequence) {
        return makeIf(condExp, consequence, Optional.empty());
    }

    public static ParseTree makeLetArray(String identifier, ParseTree indexExp,
                                         ParseTree exp) {
        return new ParseTree(
                LET_STATEMENT,
                makeKeyword("let"),
                makeIdentifier(identifier),
                makeSymbol("["),
                indexExp,
                makeSymbol("]"),
                makeSymbol("="),
                exp,
                makeSymbol(";")
        );
    }

    public static ParseTree makeLetVariable(String identifier, ParseTree exp) {
        return new ParseTree(LET_STATEMENT, makeKeyword("let"),
                makeIdentifier(identifier), makeSymbol("="), exp, makeSymbol(";"));
    }

    public static ParseTree makeDo(ParseTree functionCall) {
        List<ParseTree> children = new ArrayList<>();
        children.add(makeKeyword("do"));
        // since function call is a term we need to strip off the root
        children.addAll(functionCall.getChildren());
        children.add(makeSymbol(";"));
        return new ParseTree(DO_STATEMENT, children);
    }

    public static ParseTree makeWhile(ParseTree conditionExp, List<ParseTree> statements) {
        return new ParseTree(WHILE_STATEMENT, makeKeyword("while"),
                makeSymbol("("), conditionExp, makeSymbol(")"), makeSymbol("{"),
                new ParseTree(STATEMENTS, statements), makeSymbol("}"));
    }

    public static ParseTree makeReturn() {
        return makeReturn(Optional.empty());
    }

    public static ParseTree makeReturn(ParseTree returnExp) {
        return makeReturn(Optional.of(returnExp));
    }

    public static ParseTree makeExp(ParseTree left, String operation, ParseTree right) {
        return new ParseTree(EXPRESSION, left, makeSymbol(operation), right);
    }

    public static ParseTree makeExp(ParseTree term) {
        return new ParseTree(EXPRESSION, term);
    }

    public static ParseTree makeInt(String value) {
        return makeTerm(new ParseTree(ParseTree.Name.INT_CONST, value));
    }

    public static ParseTree makeString(String value) {
        return makeTerm(new ParseTree(STR_CONST, value));
    }

    public static ParseTree makeArrayAccess(String arrayName, ParseTree indexExp) {
        return makeTerm(Arrays.asList(makeIdentifier(arrayName),
                makeSymbol("["), indexExp, makeSymbol("]")));
    }

    public static ParseTree makeKeyword(String keyword) {
        return new ParseTree(KEYWORD, keyword);
    }

    public static ParseTree makeTermKeyword(String keyword) {
        return makeTerm(makeKeyword(keyword));
    }

    public static ParseTree makeIdentifier(String value) {
        return new ParseTree(IDENTIFIER, value);
    }

    public static ParseTree makeTermIdentifier(String value) {
        return makeTerm(makeIdentifier(value));
    }

    public static ParseTree makeTerm(List<ParseTree> children) {
        return new ParseTree(TERM, children);
    }

    public static ParseTree makeSymbol(String symbol) {
        return new ParseTree(SYMBOL, symbol);
    }

    public static ParseTree makeUnary(String operation, ParseTree operandTerm) {
        return new ParseTree(TERM, makeSymbol(operation), operandTerm);
    }

    public static ParseTree makeSubroutineCall(String functionName, List<ParseTree> arguments) {
        return makeFunctionCall(functionName, arguments, Optional.empty());
    }

    public static ParseTree makeSubroutineCall(String receiver, String functionName, List<ParseTree> arguments) {
        return makeFunctionCall(functionName, arguments, Optional.of(receiver));
    }

    public static ParseTree makeType(String type) {
        return (type.equals("int") || type.equals("char") || type.equals("boolean")) ?
            makeKeyword(type) : makeIdentifier(type);
    }

    /**
     * Make a class variable declaration of a single or more variables
     * No validation of types and modifiers is performed, both are passed as is and result in a keyword
     * node in the tree
     * @param modifier keyword modifier to the variable, like static or field
     * @param type of the variables
     * @param names of variables to create
     * @return tree corresponding to the variable(s) declaration
     */
    public static ParseTree makeClassVariablesDec(String modifier, String type, String... names) {
        return new ParseTree(CLASS_VAR, makeVariablesDec(modifier, type, names));
    }

    /**
     * Make a function variable declaration of a single or more variables
     * @param type of the variables
     * @param names of variables to create
     * @return tree corresponding to the variable(s) declaration
     */
    public static ParseTree makeSubroutineVariablesDec(String type, String... names) {
        return new ParseTree(VARIABLE_DECLARATION, makeVariablesDec("var", type, names));
    }

    /**
     * Make a class variable declaration of a single or more variables
     * No validation of types and modifiers is performed, both are passed as is and result in a keyword
     * node in the tree
     * @param modifier keyword modifier to the variable
     * @param type of the variables
     * @param names of variables to create
     * @return tree corresponding to the variable(s) declaration
     */
    private static List<ParseTree> makeVariablesDec(String modifier, String type, String... names) {
        if (names.length == 0) {
            throw new IllegalArgumentException("Variable definition should include at least one variable");
        }
        List<Variable> variables = Arrays.asList(names).stream()
                .map(name -> new Variable(name, type))
                .collect(Collectors.toList());
        List<ParseTree> variableTrees = new ArrayList<>();
        variableTrees.add(makeKeyword(modifier));
        variableTrees.add(makeType(variables.get(0).getType()));
        // todo: refactor to a join method
        for (Variable variable: variables) {
            variableTrees.add(makeIdentifier(variable.getName()));
            variableTrees.add(makeSymbol(","));
        }
        variableTrees.remove(variableTrees.size()-1); //remove last comma
        variableTrees.add(makeSymbol(";"));
        return variableTrees;
    }

    /**
     * Make function call, that could be either function call in form f() or a class method call in form
     * Foo.f().
     * @param functionName of the subroutine
     * @param params list of arguments passed to the function. The grammar expects all parameters to be expressions
     *                   but to facilitate function calls creation it's allowed to pass terms here, which will be wrapped
     *                   in expression
     * @param receiver optional reveiver of the method
     * @return parse tree for the function call, as a term
     */
    private static ParseTree makeFunctionCall(String functionName, List<ParseTree> params,
                                              Optional<String> receiver) {
        List<ParseTree> children = new ArrayList<>();
        if (receiver.isPresent()) {
            children.add(makeIdentifier(receiver.get()));
            children.add(makeSymbol("."));
        }
        children.add(makeIdentifier(functionName));
        children.add(makeSymbol("("));
        List<ParseTree> expListChildren = new ArrayList<>();
        // todo: refactor to a join method
        if (params.size() > 0) {
            for (ParseTree parameter : params) {
                expListChildren.add(parameter);
                expListChildren.add(makeSymbol(","));
            }
            expListChildren.remove(expListChildren.size() - 1); //remove last comma
        }
        children.add(new ParseTree(EXPRESSION_LIST, expListChildren));
        children.add(makeSymbol(")"));
        return makeTerm(children);
    }

    private static ParseTree makeIf(ParseTree condExp, List<ParseTree> consequence,
                                    Optional<List<ParseTree>> alternative) {
        List<ParseTree> children = new ArrayList<>();
        children.add(makeKeyword("if"));
        children.add(makeSymbol("("));
        children.add(condExp);
        children.add(makeSymbol(")"));
        children.add(makeSymbol("{"));
        children.add(new ParseTree(STATEMENTS, consequence));
        children.add(makeSymbol("}"));
        if (alternative.isPresent()) {
            children.add(makeKeyword("else"));
            children.add(makeSymbol("{"));
            children.add(new ParseTree(STATEMENTS, alternative.get()));
            children.add(makeSymbol("}"));
        }

        return new ParseTree(IF_STATEMENT, children);
    }

    private static ParseTree makeReturn(Optional<ParseTree> returnExp) {
        List<ParseTree> children = new ArrayList<>();
        children.add(makeKeyword("return"));
        if (returnExp.isPresent()) {
            if (!returnExp.get().getName().equals(EXPRESSION)) {
                throw new IllegalArgumentException("Return statement is expected to return an exp");
            }
            children.add(returnExp.get());
        }
        children.add(makeSymbol(";"));
        return new ParseTree(RETURN_STATEMENT, children);
    }

    private static ParseTree makeTerm(ParseTree child) {
        return new ParseTree(TERM, Arrays.asList(child));
    }
}
