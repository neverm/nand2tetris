package com.nvm.nand.compiler.codegen;

import com.nvm.nand.compiler.codegen.SymbolTable.Entry.Kind;
import com.nvm.nand.compiler.parsing.ParseTree;
import org.junit.Before;
import org.junit.Test;

import static com.nvm.nand.compiler.codegen.ParseTreeMaker.*;
import static com.nvm.nand.compiler.codegen.Utils.commandSeq;
import static org.junit.Assert.assertEquals;

public class ArrayAccessTest {

    private SymbolTable table;

    @Before
    public void init() {
        table = new SymbolTable();
        table.put("a", "object", Kind.LOCAL);
        table.put("b", "object", Kind.LOCAL);
        table.put("i", "int", Kind.LOCAL);
    }

    @Test
    public void testZeroConstant() {
        ParseTree array = makeExp(makeArrayAccess("a", makeExp(makeInt("0"))));
        String generated = new Generator().generateExpression(array, table);
        String expected = commandSeq(
                "push constant 0",
                "push local 0",
                "add",
                "pop pointer 1",
                "push that 0"
        );
        assertEquals(expected, generated);
    }

    @Test
    public void testNonZeroConstant() {
        ParseTree array = makeExp(makeArrayAccess("a", makeExp(makeInt("1"))));
        String generated = new Generator().generateExpression(array, table);
        String expected = commandSeq(
                "push constant 1",
                "push local 0",
                "add",
                "pop pointer 1",
                "push that 0"
        );
        assertEquals(expected, generated);
    }

    @Test
    public void testIndexVariable() {
        ParseTree array = makeExp(makeArrayAccess("a", makeExp(makeTermIdentifier("i"))));
        String generated = new Generator().generateExpression(array, table);
        String expected = commandSeq(
                "push local 2",
                "push local 0",
                "add",
                "pop pointer 1",
                "push that 0");
        assertEquals(expected, generated);
    }

    @Test
    public void testNestedArrayAcc() {
        ParseTree array = makeExp(makeArrayAccess("a", makeExp(makeArrayAccess("b",
                makeExp(makeTermIdentifier("i"))))));
        String generated = new Generator().generateExpression(array, table);
        String expected = commandSeq(
                "push local 2",
                "push local 1", // push b base address
                "add", // address of b[0] is currently on the stack
                "pop pointer 1",
                "push that 0", // content of b[0] is currently on the stack
                "push local 0", // address of a on the stack
                "add", // address of a[b[0]] on the stack
                "pop pointer 1",
                "push that 0" // content of a[b[0]] on the stack
        );
        assertEquals(expected, generated);
    }
}