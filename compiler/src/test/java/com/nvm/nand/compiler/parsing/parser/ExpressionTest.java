package com.nvm.nand.compiler.parsing.parser;

import com.nvm.nand.compiler.parsing.ParseException;
import com.nvm.nand.compiler.parsing.ParseTree;
import com.nvm.nand.compiler.parsing.Parser;
import com.nvm.nand.compiler.parsing.Token;
import org.junit.Test;

import java.util.List;

import static com.nvm.nand.compiler.parsing.ParseTree.*;
import static com.nvm.nand.compiler.parsing.ParseTree.NodeType.NON_TERMINAL;
import static com.nvm.nand.compiler.parsing.ParseTree.NodeType.TERMINAL;
import static com.nvm.nand.compiler.parsing.Token.Type.*;
import static com.nvm.nand.compiler.parsing.parser.TestUtils.*;

public class ExpressionTest {

    @Test
    public void testIntConstantExpression() throws ParseException {
        List<Token> ts = tokens(types(INT_CONSTANT), values("15"));
        ParseTree e = new Parser(ts).parseExpression();
        assertTree(e, NON_TERMINAL, Name.EXPRESSION);
        ParseTree t = e.getChildren().get(0);
        assertTree(t, NON_TERMINAL, Name.TERM);
        assertTree(t.getChildren().get(0), TERMINAL, Name.INT_CONST, "15");
    }

    @Test
    public void testStringConstantExpression() throws ParseException {
        List<Token> ts = tokens(types(STR_CONSTANT), values("hmmm"));
        ParseTree e = new Parser(ts).parseExpression();
        assertTree(e, NON_TERMINAL, Name.EXPRESSION);
        ParseTree t = e.getChildren().get(0);
        assertTree(t, NON_TERMINAL, Name.TERM);
        assertTree(t.getChildren().get(0), TERMINAL, Name.STR_CONST, "hmmm");
    }

    @Test
    public void testKeywordConstantExpression() throws ParseException {
        List<Token> ts = tokens(types(KEYWORD), values("null"));
        ParseTree e = new Parser(ts).parseExpression();
        assertTree(e, NON_TERMINAL, Name.EXPRESSION);
        ParseTree t = e.getChildren().get(0);
        assertTree(t, NON_TERMINAL, Name.TERM);
        assertTree(t.getChildren().get(0), TERMINAL, Name.KEYWORD, "null");
    }

    @Test
    public void testAddTwoNumbersExpression() throws ParseException {
        List<Token> ts = tokens(types(INT_CONSTANT, SYMBOL, INT_CONSTANT), values("2", "+", "3"));
        ParseTree e = new Parser(ts).parseExpression();
        assertTree(e, NON_TERMINAL, Name.EXPRESSION);
        List<ParseTree> children = e.getChildren();
        ParseTree t1 = children.get(0);
        ParseTree t2 = children.get(2);
        assertTree(t1.getChildren().get(0), TERMINAL, Name.INT_CONST, "2");
        assertTree(children.get(1), TERMINAL, Name.SYMBOL, "+");
        assertTree(t2.getChildren().get(0), TERMINAL, Name.INT_CONST, "3");
    }

    @Test
    public void testAddThreeNumbersExpression() throws ParseException {
        List<Token> ts = tokens(types(INT_CONSTANT, SYMBOL, INT_CONSTANT, SYMBOL, INT_CONSTANT),
                values("2", "+", "3", "+", "4"));
        ParseTree e = new Parser(ts).parseExpression();
        assertTree(e, NON_TERMINAL, Name.EXPRESSION);
        List<ParseTree> children = e.getChildren();
        ParseTree t1 = children.get(0);
        ParseTree s1 = children.get(1);
        ParseTree t2 = children.get(2);
        ParseTree s2 = children.get(3);
        ParseTree t3 = children.get(4);
        assertTree(t1, NON_TERMINAL, Name.TERM);
        assertTree(t1.getChildren().get(0), TERMINAL, Name.INT_CONST, "2");
        assertTree(s1, TERMINAL, Name.SYMBOL, "+");
        assertTree(t2, NON_TERMINAL, Name.TERM);
        assertTree(t2.getChildren().get(0), TERMINAL, Name.INT_CONST, "3");
        assertTree(s2, TERMINAL, Name.SYMBOL, "+");
        assertTree(t3, NON_TERMINAL, Name.TERM);
        assertTree(t3.getChildren().get(0), TERMINAL, Name.INT_CONST, "4");
    }

    @Test
    public void testAddTwoExpressionsExpression() throws ParseException {
        List<Token> ts = tokens(types(SYMBOL, INT_CONSTANT, SYMBOL, INT_CONSTANT, SYMBOL,
                SYMBOL, SYMBOL, INT_CONSTANT, SYMBOL, INT_CONSTANT, SYMBOL),
                values("(", "2", "+", "3", ")", "-", "(", "1", "*", "3", ")"));
        ParseTree e = new Parser(ts).parseExpression();
        assertTree(e, NON_TERMINAL, Name.EXPRESSION);
        List<ParseTree> children = e.getChildren();
        ParseTree t1 = children.get(0);
        ParseTree s = children.get(1);
        ParseTree t2 = children.get(2);
        assertTree(t1, NON_TERMINAL, Name.TERM);
        assertTree(s, TERMINAL, Name.SYMBOL, "-");
        assertTree(t2, NON_TERMINAL, Name.TERM);
        // assert general structure
        ParseTree e1 = t1.getChildren().get(1);
        assertTree(t1.getChildren().get(0), TERMINAL, Name.SYMBOL, "(");
        assertTree(e1, NON_TERMINAL, Name.EXPRESSION);
        assertTree(t1.getChildren().get(2), TERMINAL, Name.SYMBOL, ")");
        ParseTree e2 = t2.getChildren().get(1);
        assertTree(t2.getChildren().get(0), TERMINAL, Name.SYMBOL, "(");
        assertTree(e2, NON_TERMINAL, Name.EXPRESSION);
        assertTree(t2.getChildren().get(2), TERMINAL, Name.SYMBOL, ")");
        // assert left expression in parenthesis
        List<ParseTree> e1Children = e1.getChildren();
        ParseTree t11 = e1Children.get(0);
        ParseTree t12 = e1Children.get(2);
        assertTree(t11.getChildren().get(0), TERMINAL, Name.INT_CONST, "2");
        assertTree(e1Children.get(1), TERMINAL, Name.SYMBOL, "+");
        assertTree(t12.getChildren().get(0), TERMINAL, Name.INT_CONST, "3");
        // assert right expression in parenthesis
        List<ParseTree> e2Children = e2.getChildren();
        ParseTree t21 = e2Children.get(0);
        ParseTree t22 = e2Children.get(2);
        assertTree(t21.getChildren().get(0), TERMINAL, Name.INT_CONST, "1");
        assertTree(e2Children.get(1), TERMINAL, Name.SYMBOL, "*");
        assertTree(t22.getChildren().get(0), TERMINAL, Name.INT_CONST, "3");
    }

    @Test
    public void testUnaryOperatorExpression() throws ParseException {
        List<Token> ts = tokens(types(SYMBOL, INT_CONSTANT), values("-", "3"));
        ParseTree e = new Parser(ts).parseExpression();
        assertTree(e, NON_TERMINAL, Name.EXPRESSION);
        ParseTree t = e.getChildren().get(0);
        assertTree(t, NON_TERMINAL, Name.TERM);
        List<ParseTree> children = t.getChildren();
        assertTree(children.get(0), TERMINAL, Name.SYMBOL, "-");
        ParseTree innerTerm = t.getChildren().get(1);
        assertTree(innerTerm, NON_TERMINAL, Name.TERM);
        assertTree(innerTerm.getChildren().get(0), TERMINAL, Name.INT_CONST, "3");
    }

    @Test
    public void testVariableExpression() throws ParseException {
        List<Token> ts = tokens(types(IDENTIFIER), values("x"));
        ParseTree e = new Parser(ts).parseExpression();
        assertTree(e, NON_TERMINAL, Name.EXPRESSION);
        ParseTree t = e.getChildren().get(0);
        assertTree(t, NON_TERMINAL, Name.TERM);
        assertTree(t.getChildren().get(0), TERMINAL, Name.IDENTIFIER, "x");
    }

    @Test
    public void testArrayAccessExpression() throws ParseException {
        List<Token> ts = tokens(types(IDENTIFIER, SYMBOL, INT_CONSTANT, SYMBOL),
                values("xs", "[", "0", "]"));
        ParseTree e = new Parser(ts).parseExpression();
        assertTree(e, NON_TERMINAL, Name.EXPRESSION);
        ParseTree t = e.getChildren().get(0);
        assertTree(t, NON_TERMINAL, Name.TERM);
        List<ParseTree> children = t.getChildren();
        assertTree(children.get(0), TERMINAL, Name.IDENTIFIER, "xs");
        assertTree(children.get(1), TERMINAL, Name.SYMBOL, "[");
        ParseTree idxExp = children.get(2);
        assertTree(idxExp, NON_TERMINAL, Name.EXPRESSION);
        ParseTree idxTerm = idxExp.getChildren().get(0);
        assertTree(idxTerm, NON_TERMINAL, Name.TERM);
        assertTree(idxTerm.getChildren().get(0), TERMINAL, Name.INT_CONST, "0");
        assertTree(children.get(3), TERMINAL, Name.SYMBOL, "]");
    }

}
