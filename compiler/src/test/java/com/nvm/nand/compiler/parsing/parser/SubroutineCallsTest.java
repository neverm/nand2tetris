package com.nvm.nand.compiler.parsing.parser;

import com.nvm.nand.compiler.parsing.ParseException;
import com.nvm.nand.compiler.parsing.ParseTree;
import com.nvm.nand.compiler.parsing.Parser;
import com.nvm.nand.compiler.parsing.Token;
import org.junit.Test;

import java.util.List;

import static com.nvm.nand.compiler.parsing.ParseTree.*;
import static com.nvm.nand.compiler.parsing.ParseTree.NodeType.EMPTY;
import static com.nvm.nand.compiler.parsing.ParseTree.NodeType.NON_TERMINAL;
import static com.nvm.nand.compiler.parsing.ParseTree.NodeType.TERMINAL;
import static com.nvm.nand.compiler.parsing.Token.Type.*;
import static com.nvm.nand.compiler.parsing.parser.TestUtils.*;
import static com.nvm.nand.compiler.parsing.parser.TestUtils.values;
import static org.junit.Assert.assertEquals;

public class SubroutineCallsTest {

    @Test
    public void testNonEmptyExpressionList() throws ParseException {
        List<Token> ts = tokens(types(IDENTIFIER, SYMBOL, IDENTIFIER),
                values("x", ",", "y"));
        ParseTree t = new Parser(ts).parseExpressionList();
        assertTree(t, NON_TERMINAL, Name.EXPRESSION_LIST);
        List<ParseTree> children = t.getChildren();
        ParseTree e1 = children.get(0);
        ParseTree e2 = children.get(2);
        ParseTree term1 = e1.getChildren().get(0);
        ParseTree term2 = e2.getChildren().get(0);
        assertTree(e1, NON_TERMINAL, Name.EXPRESSION);
        assertTree(term1, NON_TERMINAL, Name.TERM);
        assertTree(term1.getChildren().get(0),
                TERMINAL, Name.IDENTIFIER, "x");
        assertTree(children.get(1), TERMINAL, Name.SYMBOL, ",");
        assertTree(e2, NON_TERMINAL, Name.EXPRESSION);
        assertTree(term2, NON_TERMINAL, Name.TERM);
        assertTree(term2.getChildren().get(0),
                TERMINAL, Name.IDENTIFIER, "y");
    }

    @Test
    public void testFunctionInvokationNoArguments() throws ParseException {
        List<Token> ts = tokens(types(IDENTIFIER, SYMBOL, SYMBOL),
                values("f", "(", ")"));
        ParseTree t = new Parser(ts).parseTerm();
        assertTree(t, NON_TERMINAL, Name.TERM);
        List<ParseTree> children = t.getChildren();
        assertTree(children.get(0), TERMINAL, Name.IDENTIFIER, "f");
        assertTree(children.get(1), TERMINAL, Name.SYMBOL, "(");
        assertTree(children.get(2), EMPTY, Name.EXPRESSION_LIST);
        assertTree(children.get(3), TERMINAL, Name.SYMBOL, ")");
    }

    @Test
    public void testFunctionInvokationTwoArguments() throws ParseException {
        List<Token> ts = tokens(types(IDENTIFIER, SYMBOL, IDENTIFIER, SYMBOL, IDENTIFIER, SYMBOL),
                values("f", "(", "x", ",", "y", ")"));
        ParseTree t = new Parser(ts).parseTerm();
        assertTree(t, NON_TERMINAL, Name.TERM);
        List<ParseTree> children = t.getChildren();
        ParseTree args = children.get(2);
        assertTree(children.get(0), TERMINAL, Name.IDENTIFIER, "f");
        assertTree(children.get(1), TERMINAL, Name.SYMBOL, "(");
        assertTree(children.get(3), TERMINAL, Name.SYMBOL, ")");
        assertTree(args, NON_TERMINAL, Name.EXPRESSION_LIST);
        assertEquals(3, args.getChildren().size());
    }

    @Test
    public void testMethodInvokationNoArguments() throws ParseException {
        List<Token> ts = tokens(types(IDENTIFIER, SYMBOL, IDENTIFIER, SYMBOL, SYMBOL),
                values("something", ".", "f", "(", ")"));
        ParseTree t = new Parser(ts).parseTerm();
        assertTree(t, NON_TERMINAL, Name.TERM);
        List<ParseTree> children = t.getChildren();
        assertEquals(6, children.size());
        assertTree(children.get(0), TERMINAL, Name.IDENTIFIER, "something");
        assertTree(children.get(1), TERMINAL, Name.SYMBOL, ".");
        assertTree(children.get(2), TERMINAL, Name.IDENTIFIER, "f");
        assertTree(children.get(3), TERMINAL, Name.SYMBOL, "(");
        assertTree(children.get(4), EMPTY, Name.EXPRESSION_LIST);
        assertTree(children.get(5), TERMINAL, Name.SYMBOL, ")");
    }
}
