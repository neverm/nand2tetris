package com.nvm.nand.compiler.parsing.lexer;

import com.nvm.nand.compiler.parsing.Lexer;
import com.nvm.nand.compiler.parsing.Token;
import com.nvm.nand.compiler.parsing.Token.Type;

import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

public class TestUtils {

    public static void assertTokenTypeEqual(String input, Type expectedType) {
        assertEquals(expectedType, Lexer.tokenize(input).get(0).getType());
    }

    public static void assertTokenValueEqual(String input, String expectedValue) {
        assertEquals(expectedValue, Lexer.tokenize(input).get(0).getValue());
    }

    public static void assertTokenSequenceTypesEqual(String input, Type... assertedTokenTypes) {
        assertSequenceEqual(input, Token::getType, Arrays.asList(assertedTokenTypes));
    }

    public static void assertTokenSequenceValuesEqual(String input, String... assertedResults) {
        assertSequenceEqual(input, Token::getValue, Arrays.asList(assertedResults));
    }

    /**
     * Tokenize given input and verify that the resulting tokens conform to expected results:
     * - the number of tokens in the input is equal to the number of expected results
     * - result data of some type R of every i-th token is equal to the i-th expected result
     * @param input sting to be tokenized
     * @param resultGetter function to extract information to be tested from token
     * @param expected correct results to which test results should be compared
     * @param <R> type of the that is being checked
     */
    private static <R> void assertSequenceEqual(String input, Function<Token, R> resultGetter, List<R> expected) {
        List<R> actual = Lexer.tokenize(input).stream()
                .map(resultGetter)
                .collect(Collectors.toList());
        assertThat(actual, is(expected));
    }
}
