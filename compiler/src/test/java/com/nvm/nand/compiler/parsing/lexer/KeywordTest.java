package com.nvm.nand.compiler.parsing.lexer;

import com.nvm.nand.compiler.parsing.Token.Type;
import org.junit.Test;

import static com.nvm.nand.compiler.parsing.lexer.TestUtils.assertTokenTypeEqual;
import static com.nvm.nand.compiler.parsing.lexer.TestUtils.assertTokenValueEqual;

public class KeywordTest {

    @Test
    public void testTokenizeSingleKeywordCorrectType() {
        assertTokenTypeEqual("class", Type.KEYWORD);
        assertTokenTypeEqual("constructor", Type.KEYWORD);
        assertTokenTypeEqual("function", Type.KEYWORD);
        assertTokenTypeEqual("method", Type.KEYWORD);
        assertTokenTypeEqual("field", Type.KEYWORD);
        assertTokenTypeEqual("static", Type.KEYWORD);
        assertTokenTypeEqual("var", Type.KEYWORD);
        assertTokenTypeEqual("int", Type.KEYWORD);
        assertTokenTypeEqual("char", Type.KEYWORD);
        assertTokenTypeEqual("boolean", Type.KEYWORD);
        assertTokenTypeEqual("void", Type.KEYWORD);
        assertTokenTypeEqual("true", Type.KEYWORD);
        assertTokenTypeEqual("false", Type.KEYWORD);
        assertTokenTypeEqual("null", Type.KEYWORD);
        assertTokenTypeEqual("this", Type.KEYWORD);
        assertTokenTypeEqual("let", Type.KEYWORD);
        assertTokenTypeEqual("do", Type.KEYWORD);
        assertTokenTypeEqual("if", Type.KEYWORD);
        assertTokenTypeEqual("else", Type.KEYWORD);
        assertTokenTypeEqual("while", Type.KEYWORD);
        assertTokenTypeEqual("return", Type.KEYWORD);
    }

    @Test
    public void testTokenizeSingleKeywordCorrectValue() {
        assertTokenValueEqual("class", "class");
        assertTokenValueEqual("constructor", "constructor");
        assertTokenValueEqual("function", "function");
        assertTokenValueEqual("method", "method");
        assertTokenValueEqual("field", "field");
        assertTokenValueEqual("static", "static");
        assertTokenValueEqual("var", "var");
        assertTokenValueEqual("int", "int");
        assertTokenValueEqual("char", "char");
        assertTokenValueEqual("boolean", "boolean");
        assertTokenValueEqual("void", "void");
        assertTokenValueEqual("true", "true");
        assertTokenValueEqual("false", "false");
        assertTokenValueEqual("null", "null");
        assertTokenValueEqual("this", "this");
        assertTokenValueEqual("let", "let");
        assertTokenValueEqual("do", "do");
        assertTokenValueEqual("if", "if");
        assertTokenValueEqual("else", "else");
        assertTokenValueEqual("while", "while");
        assertTokenValueEqual("return", "return");
    }

    @Test
    public void testTokenizeSingleKeywordWithWhitespace() {
        assertTokenValueEqual("/**   */class", "class");
        assertTokenValueEqual(" class", "class");
    }

    @Test
    public void testKeywordIsSubstringOfIdentifier() {
        assertTokenValueEqual("classique", "classique");
        assertTokenTypeEqual("classsique", Type.IDENTIFIER);
    }
}