package com.nvm.nand.compiler.codegen;

import com.nvm.nand.compiler.parsing.ParseTree;
import org.junit.Test;

import java.util.Arrays;

import static com.nvm.nand.compiler.codegen.ParseTreeMaker.*;
import static com.nvm.nand.compiler.codegen.Utils.commandSeq;
import static junit.framework.TestCase.assertEquals;

/**
 * Test functions, methods and constructors.
 * Void functions are tested in do statement tests
 */
public class ClassDeclarationTest {

    private static final String CLASS_NAME = "ClassName";

    @Test
    public void testEmptyDeclaration() {
        ParseTree dec = makeClassDec(
                CLASS_NAME,
                Arrays.asList(),
                Arrays.asList()
        );
        assertEquals("", new Generator().generateClass(dec));
    }

    @Test
    public void testSingleFunction() {
        ParseTree functionDec = makeSubroutineDec(
                "f",
                "function",
                makeKeyword("void"),
                Arrays.asList(),
                Arrays.asList(),
                Arrays.asList(makeReturn())
        );
        ParseTree classDec = makeClassDec(
                CLASS_NAME,
                Arrays.asList(),
                Arrays.asList(functionDec)
        );
        String expected = commandSeq(
                "function ClassName.f 0",
                "push constant 0",
                "return"
        );
        assertEquals(expected, new Generator().generateClass(classDec));
    }

    @Test
    public void testConstructorNoFields() {
        ParseTree functionDec = makeSubroutineDec(
                "f",
                "constructor",
                makeIdentifier(CLASS_NAME),
                Arrays.asList(),
                Arrays.asList(),
                Arrays.asList(makeReturn(makeExp(makeTermKeyword("this"))))
        );
        ParseTree classDec = makeClassDec(
                CLASS_NAME,
                Arrays.asList(),
                Arrays.asList(functionDec)
        );
        String expected = commandSeq(
                "function ClassName.f 1",
                "push constant 1",
                "call Memory.alloc 1",
                "pop local 0",
                "push local 0",
                "pop pointer 0",
                "push pointer 0",
                "return"
        );
        assertEquals(expected, new Generator().generateClass(classDec));
    }

    @Test
    public void testConstructorOneField() {
        ParseTree functionDec = makeSubroutineDec(
                "f",
                "constructor",
                makeIdentifier(CLASS_NAME),
                Arrays.asList(),
                Arrays.asList(),
                Arrays.asList(makeReturn(makeExp(makeTermKeyword("this"))))
        );
        ParseTree classDec = makeClassDec(
                CLASS_NAME,
                Arrays.asList(makeClassVariablesDec("field", "int", "a")),
                Arrays.asList(functionDec)
        );
        String expected = commandSeq(
                "function ClassName.f 1",
                "push constant 1",
                "call Memory.alloc 1",
                "pop local 0",
                "push local 0",
                "pop pointer 0",
                "push pointer 0",
                "return"
        );
        assertEquals(expected, new Generator().generateClass(classDec));
    }

    @Test
    public void testConstructorAssignToField() {
        String localField = "a";
        ParseTree functionDec = makeSubroutineDec(
                "f",
                "constructor",
                makeIdentifier(CLASS_NAME),
                Arrays.asList(),
                Arrays.asList(),
                Arrays.asList(
                        makeLetVariable(localField, makeExp(makeInt("1"))),
                        makeReturn(makeExp(makeTermKeyword("this")))
                )
        );
        ParseTree classDec = makeClassDec(
                CLASS_NAME,
                Arrays.asList(makeClassVariablesDec("field", "int", localField)),
                Arrays.asList(functionDec)
        );
        String expected = commandSeq(
                "function ClassName.f 1",
                "push constant 1",
                "call Memory.alloc 1",
                "pop local 0",
                "push local 0",
                "pop pointer 0",
                "push constant 1",
                "pop this 0",
                "push pointer 0",
                "return"
        );
        assertEquals(expected, new Generator().generateClass(classDec));
    }

    @Test
    public void testConstructorTwoFields() {
        ParseTree functionDec = makeSubroutineDec(
                "f",
                "constructor",
                makeIdentifier(CLASS_NAME),
                Arrays.asList(),
                Arrays.asList(),
                Arrays.asList(makeReturn(makeExp(makeTermKeyword("this"))))
        );
        ParseTree classDec = makeClassDec(
                CLASS_NAME,
                Arrays.asList(makeClassVariablesDec("field", "int", "a", "b")),
                Arrays.asList(functionDec)
        );
        String expected = commandSeq(
                "function ClassName.f 1",
                "push constant 2",
                "call Memory.alloc 1",
                "pop local 0",
                "push local 0",
                "pop pointer 0",
                "push pointer 0",
                "return"
        );
        assertEquals(expected, new Generator().generateClass(classDec));
    }

    @Test
    public void testCallToMethodInConstructor() {
        ParseTree functionDec = makeSubroutineDec(
                "f",
                "constructor",
                makeIdentifier(CLASS_NAME),
                Arrays.asList(),
                Arrays.asList(),
                Arrays.asList(
                        makeDo(makeSubroutineCall("g", Arrays.asList())),
                        makeReturn(makeExp(makeTermKeyword("this")))
                )
        );
        ParseTree classDec = makeClassDec(
                CLASS_NAME,
                Arrays.asList(makeClassVariablesDec("field", "int", "a", "b")),
                Arrays.asList(functionDec)
        );
        String expected = commandSeq(
                "function ClassName.f 1",
                "push constant 2",
                "call Memory.alloc 1",
                "pop local 0",
                "push local 0",
                "pop pointer 0",
                // push THIS (local 0 in constructors) to the stack and pass it implicitly to g
                "push local 0",
                "call " + CLASS_NAME + ".g 1",
                "pop temp 0",
                "push pointer 0",
                "return"
        );
        assertEquals(expected, new Generator().generateClass(classDec));
    }

    @Test
    public void testCallToMethodInMethod() {
        ParseTree functionDec = makeSubroutineDec(
                "f",
                "method",
                makeIdentifier(CLASS_NAME),
                Arrays.asList(),
                Arrays.asList(),
                Arrays.asList(
                        makeDo(makeSubroutineCall("g", Arrays.asList())),
                        makeReturn(makeExp(makeTermKeyword("this")))
                )
        );
        ParseTree classDec = makeClassDec(
                CLASS_NAME,
                Arrays.asList(makeClassVariablesDec("field", "int", "a", "b")),
                Arrays.asList(functionDec)
        );
        String expected = commandSeq(
                "function ClassName.f 0",
                "push argument 0",
                "pop pointer 0",
                // push THIS (argument 0 in constructors) to the stack and pass it implicitly to g
                "push argument 0",
                "call " + CLASS_NAME + ".g 1",
                "pop temp 0",
                "push pointer 0",
                "return"
        );
        assertEquals(expected, new Generator().generateClass(classDec));
    }
}
