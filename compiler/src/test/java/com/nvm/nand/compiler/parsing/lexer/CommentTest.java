package com.nvm.nand.compiler.parsing.lexer;

import com.nvm.nand.compiler.parsing.Token.Type;
import org.junit.Test;

import static com.nvm.nand.compiler.parsing.lexer.TestUtils.assertTokenSequenceTypesEqual;
import static com.nvm.nand.compiler.parsing.lexer.TestUtils.assertTokenSequenceValuesEqual;

public class CommentTest {

    @Test
    public void testMultilineComment() {
        String input = "/** \n * class description \n */ \n class";
        assertTokenSequenceTypesEqual(input, Type.KEYWORD);
        assertTokenSequenceValuesEqual(input, "class");
    }

    @Test
    public void testTokenBetweenMultilineComments() {
        String input = "/** \n * class description \n */ \n class /** additional comment */";
        assertTokenSequenceTypesEqual(input, Type.KEYWORD);
        assertTokenSequenceValuesEqual(input, "class");
    }

    @Test
    public void testTokenMixedWithComments() {
        String input = "class /** \n * class description \n */ \n class /** additional comment */ class";
        assertTokenSequenceTypesEqual(input, Type.KEYWORD, Type.KEYWORD, Type.KEYWORD);
        assertTokenSequenceValuesEqual(input, "class", "class", "class");
    }
}
