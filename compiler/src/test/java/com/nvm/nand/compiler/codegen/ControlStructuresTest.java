package com.nvm.nand.compiler.codegen;

import com.nvm.nand.compiler.codegen.SymbolTable.Entry.Kind;
import com.nvm.nand.compiler.parsing.ParseTree;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static com.nvm.nand.compiler.codegen.Generator.*;
import static com.nvm.nand.compiler.codegen.ParseTreeMaker.*;
import static com.nvm.nand.compiler.codegen.Utils.commandSeq;
import static org.junit.Assert.assertEquals;

public class ControlStructuresTest {

    private SymbolTable table;
    private Generator generator;
    private ParseTree simpleCondition;
    private List<ParseTree> callFStatements, callGStatements ;

    @Before
    public void init() {
        table = new SymbolTable();
        table.put("a", "int", Kind.LOCAL);
        generator = new Generator();
        generator.setClassTable(new SymbolTable());
        generator.setClassName("ClassName");
        simpleCondition = makeExp(makeTermIdentifier("a"), "=", makeInt("1"));
        callFStatements = Arrays.asList(makeDo(makeSubroutineCall("ClassName", "f", Arrays.asList())));
        callGStatements = Arrays.asList(makeDo(makeSubroutineCall("ClassName", "g", Arrays.asList())));
    }

    @Test
    public void testOneHandIfNoStatemens() {
        ParseTree statement = makeIf(simpleCondition, Arrays.asList());
        String generated = generator.generateIf(statement, table);
        String expected = commandSeq(
                "push local 0",
                "push constant 1",
                "eq",
                "not",
                "if-goto " + LABEL_END + "1",
                "label " + LABEL_END + "1"
        );
        assertEquals(expected, generated);
    }

    @Test
    public void testTwoHandIfNoStatemens() {
        ParseTree statement = makeIf(simpleCondition, Arrays.asList(), Arrays.asList());
        String generated = generator.generateIf(statement, table);
        String expected = commandSeq(
                "push local 0",
                "push constant 1",
                "eq",
                "not",
                "if-goto " + LABEL_ALTERNATIVE + "1",
                "goto " + LABEL_END + "1",
                "label " + LABEL_ALTERNATIVE + "1",
                "label " + LABEL_END + "1"
        );
        assertEquals(expected, generated);
    }

    @Test
    public void testEmptyWhile() {
        ParseTree statement = makeWhile(simpleCondition, Arrays.asList());
        String generated = generator.generateWhile(statement, table);
        String expected = commandSeq(
                "label " + LABEL_LOOP + "1",
                "push local 0",
                "push constant 1",
                "eq",
                "not",
                "if-goto " + LABEL_LOOP_END + "1",
                "goto " + LABEL_LOOP + "1",
                "label " + LABEL_LOOP_END + "1"
        );
        assertEquals(expected, generated);
    }

    @Test
    public void testOneHandIfOneStatement() {
        ParseTree statement = makeIf(
                simpleCondition,
                callFStatements
        );
        String generated = generator.generateIf(statement, table);
        String expected = commandSeq(
                "push local 0",
                "push constant 1",
                "eq",
                "not",
                "if-goto " + LABEL_END + "1",
                "call ClassName.f 0",
                "pop temp 0",
                "label " + LABEL_END + "1"
        );
        assertEquals(expected, generated);
    }

    @Test
    public void testTwoHandsIf() {
        ParseTree statement = makeIf(
                simpleCondition,
                callFStatements,
                callGStatements

        );
        String generated = generator.generateIf(statement, table);
        String expected = commandSeq(
                "push local 0",
                "push constant 1",
                "eq",
                "not",
                "if-goto " + LABEL_ALTERNATIVE + "1",
                "call ClassName.f 0",
                "pop temp 0",
                "goto " + LABEL_END + "1",
                "label " + LABEL_ALTERNATIVE + "1",
                "call ClassName.g 0",
                "pop temp 0",
                "label " + LABEL_END + "1"
        );
        assertEquals(expected, generated);
    }

    @Test
    public void testWhile() {
        ParseTree statement = makeWhile(simpleCondition, callFStatements);
        String generated = generator.generateWhile(statement, table);
        String expected = commandSeq(
                "label " + LABEL_LOOP + "1",
                "push local 0",
                "push constant 1",
                "eq",
                "not",
                "if-goto " + LABEL_LOOP_END + "1",
                "call ClassName.f 0",
                "pop temp 0",
                "goto " + LABEL_LOOP + "1",
                "label " + LABEL_LOOP_END + "1"
        );
        assertEquals(expected, generated);
    }

    @Test
    public void testNestedIf() {
        ParseTree statement = makeIf(
                simpleCondition,
                Arrays.asList(makeIf(simpleCondition, callFStatements)),
                callGStatements

        );
        String generated = generator.generateIf(statement, table);
        String expected = commandSeq(
                "push local 0",
                "push constant 1",
                "eq",
                "not",
                "if-goto " + LABEL_ALTERNATIVE + "1",
                "push local 0",
                "push constant 1",
                "eq",
                "not",
                "if-goto " + LABEL_END + "2",
                "call ClassName.f 0",
                "pop temp 0",
                "label " + LABEL_END + "2",
                "goto " + LABEL_END + "1",
                "label " + LABEL_ALTERNATIVE + "1",
                "call ClassName.g 0",
                "pop temp 0",
                "label " + LABEL_END + "1"
        );
        assertEquals(expected, generated);
    }

    @Test
    public void testNestedWhile() {
        ParseTree statement = makeWhile(
                simpleCondition,
                Arrays.asList(makeWhile(simpleCondition, callFStatements))
        );
        String generated = generator.generateWhile(statement, table);
        String expected = commandSeq(
                "label " + LABEL_LOOP + "1",
                "push local 0",
                "push constant 1",
                "eq",
                "not",
                "if-goto " + LABEL_LOOP_END + "1",
                "label " + LABEL_LOOP + "2",
                "push local 0",
                "push constant 1",
                "eq",
                "not",
                "if-goto " + LABEL_LOOP_END + "2",
                "call ClassName.f 0",
                "pop temp 0",
                "goto " + LABEL_LOOP + "2",
                "label " + LABEL_LOOP_END + "2",
                "goto " + LABEL_LOOP + "1",
                "label " + LABEL_LOOP_END + "1"
        );
        assertEquals(expected, generated);
    }
}