package com.nvm.nand.compiler.codegen;

import com.nvm.nand.compiler.codegen.SymbolTable.Entry.Kind;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class SymbolTableTest {

    private SymbolTable table;

    @Before
    public void init() {
        table = new SymbolTable();
    }

    @Test
    public void testHasVariableEmptyTable() {
        assertFalse(table.contains("x", Kind.LOCAL));
    }

    @Test
    public void testHasVariableAfterAdding() {
        table.put("x", "int", Kind.LOCAL);
        assertTrue(table.contains("x", Kind.LOCAL));
    }

    @Test
    public void testHasVariableCorrectKind() {
        table.put("x", "int", Kind.LOCAL);
        assertFalse(table.contains("x", Kind.ARGUMENT));
        assertTrue(table.contains("x", Kind.LOCAL));
    }

    @Test
    public void testGetVariableNumberCorrectNumber() {
        table.put("x", "int", Kind.LOCAL);
        assertEquals(0, table.getNumber("x"));
    }

    @Test
    public void testGetVariableNumberMultipleVariablesDifferentKind() {
        table.put("y", "int", Kind.ARGUMENT);
        table.put("x", "int", Kind.LOCAL);
        assertEquals(0, table.getNumber("x"));
    }

    @Test
    public void testGetVariableNumberMultipleVariablesSameKind() {
        table.put("x", "int", Kind.LOCAL);
        table.put("y", "int", Kind.LOCAL);
        assertEquals(1, table.getNumber("y"));
    }

    @Test
    public void testVariableNumersOfDifferentKindsIndependent() {
        table.put("x", "int", Kind.ARGUMENT);
        table.put("y", "int", Kind.LOCAL);
        assertEquals(0, table.getNumber("y"));
        assertEquals(0, table.getNumber("x"));
    }

    @Test
    public void testGetVariableShadowsPreviousDefinition() {
        table.put("x", "int", Kind.ARGUMENT);
        table.put("x", "int", Kind.LOCAL);
        assertEquals(Kind.LOCAL, table.get("x").getKind());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testTwoVariablesOfTheSameKindNotAllowed() {
        table.put("x", "int", Kind.LOCAL);
        table.put("x", "int", Kind.LOCAL);
    }
}