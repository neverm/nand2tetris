package com.nvm.nand.compiler.parsing.lexer;

import com.nvm.nand.compiler.parsing.Token.Type;
import org.junit.Test;

import static com.nvm.nand.compiler.parsing.lexer.TestUtils.assertTokenTypeEqual;
import static com.nvm.nand.compiler.parsing.lexer.TestUtils.assertTokenValueEqual;

public class IntegerTest {

    @Test
    public void testTokenizeSingleIntegerConstantCorrectType() {
        assertTokenTypeEqual("42", Type.INT_CONSTANT);
    }

    @Test
    public void testTokenizeSingleIntegerConstantCorrectValue() {
        assertTokenValueEqual("42", "42");
        assertTokenValueEqual("0", "0");
        assertTokenValueEqual("32768", "32768");
    }

    @Test
    public void testTokenizeSingleIntegerConstantWithWhitespace() {
        assertTokenValueEqual("/** comment */42", "42");
        assertTokenValueEqual(" 0", "0");
    }
}