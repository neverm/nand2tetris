package com.nvm.nand.compiler.parsing.parser;

import com.nvm.nand.compiler.parsing.ParseTree;
import com.nvm.nand.compiler.parsing.Parser;
import com.nvm.nand.compiler.parsing.Token;
import com.nvm.nand.compiler.parsing.ParseException;
import org.junit.Test;

import java.util.List;

import static com.nvm.nand.compiler.parsing.ParseTree.*;
import static com.nvm.nand.compiler.parsing.ParseTree.NodeType.NON_TERMINAL;
import static com.nvm.nand.compiler.parsing.ParseTree.NodeType.TERMINAL;
import static com.nvm.nand.compiler.parsing.Token.Type.INT_CONSTANT;
import static com.nvm.nand.compiler.parsing.Token.Type.KEYWORD;
import static com.nvm.nand.compiler.parsing.Token.Type.STR_CONSTANT;
import static com.nvm.nand.compiler.parsing.parser.TestUtils.*;

public class ConstantTermTest {

    @Test
    public void testIntegerConstant() throws ParseException {
        List<Token> ts = tokens(types(INT_CONSTANT), values("15"));
        ParseTree t = new Parser(ts).parseTerm();
        assertTree(t, NON_TERMINAL, Name.TERM);
        ParseTree child = t.getChildren().get(0);
        assertTree(child, TERMINAL, Name.INT_CONST, "15");
    }

    @Test(expected = ParseException.class)
    public void testIntegerConstantBiggerThanAllowed() throws ParseException {
        List<Token> ts = tokens(types(INT_CONSTANT), values("32768"));
        new Parser(ts).parseTerm();
    }

    @Test
    public void testEmptyString() throws ParseException {
        List<Token> ts = tokens(types(STR_CONSTANT), values(""));
        ParseTree t = new Parser(ts).parseTerm();
        assertTree(t, NON_TERMINAL, Name.TERM);
        ParseTree child = t.getChildren().get(0);
        assertTree(child, TERMINAL, Name.STR_CONST, "");
    }

    @Test
    public void testNonEmptyString() throws ParseException {
        List<Token> ts = tokens(types(STR_CONSTANT), values("some text; here"));
        ParseTree t = new Parser(ts).parseTerm();
        assertTree(t, NON_TERMINAL, Name.TERM);
        ParseTree child = t.getChildren().get(0);
        assertTree(child, TERMINAL, Name.STR_CONST, "some text; here");
    }

    @Test
    public void testNullConstant() throws ParseException {
        List<Token> ts = tokens(types(KEYWORD), values("null"));
        ParseTree t = new Parser(ts).parseTerm();
        assertTree(t, NON_TERMINAL, Name.TERM);
        ParseTree child = t.getChildren().get(0);
        assertTree(child, TERMINAL, Name.KEYWORD, "null");
    }
}
