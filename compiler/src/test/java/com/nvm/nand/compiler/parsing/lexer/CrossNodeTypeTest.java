package com.nvm.nand.compiler.parsing.lexer;

import com.nvm.nand.compiler.parsing.Token.Type;
import org.junit.Test;

import static com.nvm.nand.compiler.parsing.lexer.TestUtils.assertTokenSequenceTypesEqual;
import static com.nvm.nand.compiler.parsing.lexer.TestUtils.assertTokenSequenceValuesEqual;

public class CrossNodeTypeTest {

    @Test
    public void testTokenizeTokenTypesFunctionDeclaration() {
        String input = "method void f() { //method";
        assertTokenSequenceTypesEqual(input, Type.KEYWORD, Type.KEYWORD, Type.IDENTIFIER,
                Type.SYMBOL, Type.SYMBOL, Type.SYMBOL);
    }

    @Test
    public void testTokenizeTokenValuesFunctionDeclaration() {
        String input = "method void f() { //method";
        assertTokenSequenceValuesEqual(input, "method", "void", "f", "(", ")", "{");
    }

    @Test
    public void testTokenizeTokenTypesArrayAccess() {
        String input = "let a[100] = 77;";
        assertTokenSequenceTypesEqual(input, Type.KEYWORD, Type.IDENTIFIER, Type.SYMBOL,
                Type.INT_CONSTANT, Type.SYMBOL, Type.SYMBOL, Type.INT_CONSTANT, Type.SYMBOL);
    }

    @Test
    public void testTokenizeTokenValuesArrayAccess() {
        String input = "let a[100] = 77;";
        assertTokenSequenceValuesEqual(input, "let", "a", "[", "100", "]", "=", "77", ";");
    }

    @Test
    public void testTokenizeTokenTypesMethodInvocation() {
        String input = "do Memory.deAlloc(this);";
        assertTokenSequenceTypesEqual(input, Type.KEYWORD, Type.IDENTIFIER, Type.SYMBOL,
                Type.IDENTIFIER, Type.SYMBOL, Type.KEYWORD, Type.SYMBOL, Type.SYMBOL);
    }

    @Test
    public void testTokenizeTokenValuesMethodInvocation() {
        String input = "do Memory.deAlloc(this);";
        assertTokenSequenceValuesEqual(input, "do", "Memory", ".", "deAlloc", "(", "this", ")", ";");
    }

    @Test
    public void testTokenizeTokenTypesVariableAssignment() {
        String input = "let x = 25;";
        assertTokenSequenceTypesEqual(input, Type.KEYWORD, Type.IDENTIFIER, Type.SYMBOL,
                Type.INT_CONSTANT, Type.SYMBOL);
    }

    @Test
    public void testTokenizeTokenValuesVariableAssignment() {
        String input = "let x = 25;";
        assertTokenSequenceValuesEqual(input, "let", "x", "=", "25", ";");
    }

    @Test
    public void testTokenizeTokenTypesVariableDeclaration() {
        String input = "var boolean t;";
        assertTokenSequenceTypesEqual(input, Type.KEYWORD, Type.KEYWORD, Type.IDENTIFIER, Type.SYMBOL);
    }

    @Test
    public void testTokenizeTokenValuesVariableDeclaration() {
        String input = "var boolean t;";
        assertTokenSequenceValuesEqual(input, "var", "boolean", "t", ";");
    }

    @Test
    public void testTokenizeTokenTypesNestedExpression() {
        String input = "let x = ((2 + 3) * (4 / 2));";
        assertTokenSequenceTypesEqual(input, Type.KEYWORD, Type.IDENTIFIER, Type.SYMBOL, Type.SYMBOL,
                Type.SYMBOL, Type.INT_CONSTANT, Type.SYMBOL, Type.INT_CONSTANT, Type.SYMBOL, Type.SYMBOL, Type.SYMBOL,
                Type.INT_CONSTANT, Type.SYMBOL, Type.INT_CONSTANT, Type.SYMBOL, Type.SYMBOL, Type.SYMBOL);
    }

    @Test
    public void testTokenizeTokenValuesNestedExpression() {
        String input = "let x = ((2 + 3) * (4 / 2));";
        assertTokenSequenceValuesEqual(input, "let", "x", "=", "(", "(", "2", "+", "3", ")", "*", "(", "4",
                "/", "2", ")", ")", ";");
    }

    @Test
    public void testTokenizeTokenTypesIfStatement() {
        String input = "if (~(next = null)) {";
        assertTokenSequenceTypesEqual(input, Type.KEYWORD, Type.SYMBOL, Type.SYMBOL, Type.SYMBOL, Type.IDENTIFIER,
                Type.SYMBOL, Type.KEYWORD, Type.SYMBOL, Type.SYMBOL, Type.SYMBOL);
    }

    @Test
    public void testTokenizeTokenValuesIfdStatement() {
        String input = "if (~(next = null)) {";
        assertTokenSequenceValuesEqual(input, "if", "(", "~", "(", "next", "=", "null", ")", ")", "{");
    }

    @Test
    public void testTokenizeTokenTypesWhileStatement() {
        String input = "while (~(b = 0)) {";
        assertTokenSequenceTypesEqual(input, Type.KEYWORD, Type.SYMBOL, Type.SYMBOL, Type.SYMBOL, Type.IDENTIFIER,
                Type.SYMBOL, Type.INT_CONSTANT, Type.SYMBOL, Type.SYMBOL, Type.SYMBOL);
    }

    @Test
    public void testTokenizeTokenValuesWhileStatement() {
        String input = "while (~(b = 0)) {";
        assertTokenSequenceValuesEqual(input, "while", "(", "~", "(", "b", "=", "0", ")", ")", "{");
    }
}
