package com.nvm.nand.compiler.parsing.parser;

import com.nvm.nand.compiler.parsing.ParseException;
import com.nvm.nand.compiler.parsing.ParseTree;
import com.nvm.nand.compiler.parsing.Parser;
import com.nvm.nand.compiler.parsing.Token;
import org.junit.Test;

import java.util.List;

import static com.nvm.nand.compiler.parsing.ParseTree.*;
import static com.nvm.nand.compiler.parsing.ParseTree.NodeType.EMPTY;
import static com.nvm.nand.compiler.parsing.ParseTree.NodeType.NON_TERMINAL;
import static com.nvm.nand.compiler.parsing.ParseTree.NodeType.TERMINAL;
import static com.nvm.nand.compiler.parsing.Token.Type.*;
import static com.nvm.nand.compiler.parsing.parser.TestUtils.*;
import static com.nvm.nand.compiler.parsing.parser.TestUtils.values;
import static org.junit.Assert.assertEquals;

public class StatementTest {

    @Test
    public void testEmptyStatements() throws ParseException {
        List<Token> ts = tokens(types(), values());
        ParseTree t = new Parser(ts).parseStatements();
        assertTree(t, EMPTY, Name.STATEMENTS);
    }

    @Test
    public void testNonEmptyStatements() throws ParseException {
        List<Token> ts = tokens(types(KEYWORD, IDENTIFIER, SYMBOL, INT_CONSTANT, SYMBOL,
                KEYWORD, IDENTIFIER, SYMBOL, INT_CONSTANT, SYMBOL),
                values("let", "x", "=", "1", ";", "let", "x", "=", "1", ";"));
        ParseTree t = new Parser(ts).parseStatements();
        assertTree(t, NON_TERMINAL, Name.STATEMENTS);
        List<ParseTree> children = t.getChildren();
        assertEquals(2, children.size());
        assertTree(children.get(0), NON_TERMINAL, Name.LET_STATEMENT);
        assertTree(children.get(1), NON_TERMINAL, Name.LET_STATEMENT);
    }

    @Test
    public void testLetStatementSimple() throws ParseException {
        List<Token> ts = tokens(types(KEYWORD, IDENTIFIER, SYMBOL, INT_CONSTANT, SYMBOL),
                values("let", "x", "=", "1", ";"));
        ParseTree t = new Parser(ts).parseLet();
        assertTree(t, NON_TERMINAL, Name.LET_STATEMENT);
        List<ParseTree> children = t.getChildren();
        assertEquals(5, children.size());
        ParseTree exp = children.get(3);
        ParseTree expTerm = exp.getChildren().get(0);
        ParseTree termValue = expTerm.getChildren().get(0);
        assertTree(children.get(0), TERMINAL, Name.KEYWORD, "let");
        assertTree(children.get(1), TERMINAL, Name.IDENTIFIER, "x");
        assertTree(children.get(2), TERMINAL, Name.SYMBOL, "=");
        assertTree(exp, NON_TERMINAL, Name.EXPRESSION);
        assertTree(expTerm, NON_TERMINAL, Name.TERM);
        assertTree(termValue, TERMINAL, Name.INT_CONST, "1");
        assertTree(children.get(4), TERMINAL, Name.SYMBOL, ";");
    }

    @Test
    public void testLetStatementArray() throws ParseException {
        List<Token> ts = tokens(types(KEYWORD, IDENTIFIER, SYMBOL, INT_CONSTANT,
                SYMBOL, SYMBOL, INT_CONSTANT, SYMBOL),
                values("let", "x", "[", "0", "]", "=", "1", ";"));
        ParseTree t = new Parser(ts).parseLet();
        assertTree(t, NON_TERMINAL, Name.LET_STATEMENT);
        List<ParseTree> children = t.getChildren();
        assertEquals(8, children.size());
        ParseTree arrayExp = children.get(3);
        ParseTree arrayExpTerm = arrayExp.getChildren().get(0);
        ParseTree arrayTermValue = arrayExpTerm.getChildren().get(0);
        ParseTree exp = children.get(6);
        ParseTree expTerm = exp.getChildren().get(0);
        ParseTree termValue = expTerm.getChildren().get(0);
        assertTree(children.get(0), TERMINAL, Name.KEYWORD, "let");
        assertTree(children.get(1), TERMINAL, Name.IDENTIFIER, "x");
        assertTree(children.get(2), TERMINAL, Name.SYMBOL, "[");
        assertTree(arrayExp, NON_TERMINAL, Name.EXPRESSION);
        assertTree(arrayExpTerm, NON_TERMINAL, Name.TERM);
        assertTree(arrayTermValue, TERMINAL, Name.INT_CONST, "0");
        assertTree(children.get(4), TERMINAL, Name.SYMBOL, "]");
        assertTree(children.get(5), TERMINAL, Name.SYMBOL, "=");
        assertTree(exp, NON_TERMINAL, Name.EXPRESSION);
        assertTree(expTerm, NON_TERMINAL, Name.TERM);
        assertTree(termValue, TERMINAL, Name.INT_CONST, "1");
        assertTree(children.get(7), TERMINAL, Name.SYMBOL, ";");
    }

    @Test
    public void testIfOneBranch() throws ParseException {
        List<Token> ts = tokens(types(KEYWORD, SYMBOL, KEYWORD,
                SYMBOL, SYMBOL, KEYWORD, INT_CONSTANT, SYMBOL, SYMBOL),
                values("if", "(", "true", ")", "{", "return", "1", ";", "}"));
        ParseTree t = new Parser(ts).parseIf();
        assertTree(t, NON_TERMINAL, Name.IF_STATEMENT);
        List<ParseTree> children = t.getChildren();
        assertEquals(7, children.size());
        assertTree(children.get(0), TERMINAL, Name.KEYWORD, "if");
        assertTree(children.get(1), TERMINAL, Name.SYMBOL, "(");
        assertTree(children.get(2), NON_TERMINAL, Name.EXPRESSION);
        assertTree(children.get(3), TERMINAL, Name.SYMBOL, ")");
        assertTree(children.get(4), TERMINAL, Name.SYMBOL, "{");
        assertTree(children.get(5), NON_TERMINAL, Name.STATEMENTS);
        assertTree(children.get(6), TERMINAL, Name.SYMBOL, "}");
    }

    @Test
    public void testIfStatement() throws ParseException {
        List<Token> ts = tokens(types(KEYWORD, SYMBOL, KEYWORD,
                SYMBOL, SYMBOL, KEYWORD, INT_CONSTANT, SYMBOL, SYMBOL,
                KEYWORD, SYMBOL, KEYWORD, INT_CONSTANT, SYMBOL, SYMBOL),
                values("if", "(", "true", ")", "{", "return", "1", ";", "}",
                        "else", "{", "return", "1", ";", "}"));
        ParseTree t = new Parser(ts).parseIf();
        assertTree(t, NON_TERMINAL, Name.IF_STATEMENT);
        List<ParseTree> children = t.getChildren();
        assertEquals(11, children.size());
        assertTree(children.get(0), TERMINAL, Name.KEYWORD, "if");
        assertTree(children.get(1), TERMINAL, Name.SYMBOL, "(");
        assertTree(children.get(2), NON_TERMINAL, Name.EXPRESSION);
        assertTree(children.get(3), TERMINAL, Name.SYMBOL, ")");
        assertTree(children.get(4), TERMINAL, Name.SYMBOL, "{");
        assertTree(children.get(5), NON_TERMINAL, Name.STATEMENTS);
        assertTree(children.get(6), TERMINAL, Name.SYMBOL, "}");
        assertTree(children.get(7), TERMINAL, Name.KEYWORD, "else");
        assertTree(children.get(8), TERMINAL, Name.SYMBOL, "{");
        assertTree(children.get(9), NON_TERMINAL, Name.STATEMENTS);
        assertTree(children.get(10), TERMINAL, Name.SYMBOL, "}");
    }

    @Test
    public void testWhileStatement() throws ParseException {
        List<Token> ts = tokens(types(KEYWORD, SYMBOL, KEYWORD,
                SYMBOL, SYMBOL, KEYWORD, INT_CONSTANT, SYMBOL, SYMBOL),
                values("while", "(", "true", ")", "{", "return", "1", ";", "}"));
        ParseTree t = new Parser(ts).parseWhile();
        assertTree(t, NON_TERMINAL, Name.WHILE_STATEMENT);
        List<ParseTree> children = t.getChildren();
        assertEquals(7, children.size());
        assertTree(children.get(0), TERMINAL, Name.KEYWORD, "while");
        assertTree(children.get(1), TERMINAL, Name.SYMBOL, "(");
        assertTree(children.get(2), NON_TERMINAL, Name.EXPRESSION);
        assertTree(children.get(3), TERMINAL, Name.SYMBOL, ")");
        assertTree(children.get(4), TERMINAL, Name.SYMBOL, "{");
        assertTree(children.get(5), NON_TERMINAL, Name.STATEMENTS);
        assertTree(children.get(6), TERMINAL, Name.SYMBOL, "}");
    }

    @Test
    public void testDoStatement() throws ParseException {
        List<Token> ts = tokens(types(KEYWORD, IDENTIFIER, SYMBOL, SYMBOL, SYMBOL),
                values("do", "fun", "(", ")", ";"));
        ParseTree t = new Parser(ts).parseDo();
        assertTree(t, NON_TERMINAL, Name.DO_STATEMENT);
        List<ParseTree> children = t.getChildren();
        assertEquals(6, children.size());
        assertTree(children.get(0), TERMINAL, Name.KEYWORD, "do");
        assertTree(children.get(1), TERMINAL, Name.IDENTIFIER, "fun");
        assertTree(children.get(2), TERMINAL, Name.SYMBOL, "(");
        assertTree(children.get(3), EMPTY, Name.EXPRESSION_LIST);
        assertTree(children.get(4), TERMINAL, Name.SYMBOL, ")");
        assertTree(children.get(5), TERMINAL, Name.SYMBOL, ";");
    }

    @Test
    public void testEmptyReturnStatement() throws ParseException {
        List<Token> ts = tokens(types(KEYWORD, SYMBOL),
                values("return", ";"));
        ParseTree t = new Parser(ts).parseReturn();
        assertTree(t, NON_TERMINAL, Name.RETURN_STATEMENT);
        List<ParseTree> children = t.getChildren();
        assertEquals(2, children.size());
        assertTree(children.get(0), TERMINAL, Name.KEYWORD, "return");
        assertTree(children.get(1), TERMINAL, Name.SYMBOL, ";");
    }

    @Test
    public void testNonEmptyReturnStatement() throws ParseException {
        List<Token> ts = tokens(types(KEYWORD, IDENTIFIER, SYMBOL),
                values("return", "x", ";"));
        ParseTree t = new Parser(ts).parseReturn();
        assertTree(t, NON_TERMINAL, Name.RETURN_STATEMENT);
        List<ParseTree> children = t.getChildren();
        ParseTree exp = children.get(1);
        ParseTree innerTerm = exp.getChildren().get(0);
        assertEquals(3, children.size());
        assertTree(children.get(0), TERMINAL, Name.KEYWORD, "return");
        assertTree(exp, NON_TERMINAL, Name.EXPRESSION);
        assertTree(innerTerm, NON_TERMINAL, Name.TERM);
        assertTree(innerTerm.getChildren().get(0), TERMINAL, Name.IDENTIFIER, "x");
        assertTree(children.get(2), TERMINAL, Name.SYMBOL, ";");
    }
}
