package com.nvm.nand.compiler.codegen;

import com.nvm.nand.compiler.parsing.ParseTree;
import org.junit.Before;
import org.junit.Test;

import static com.nvm.nand.compiler.codegen.ParseTreeMaker.*;
import static com.nvm.nand.compiler.codegen.Utils.commandSeq;
import static org.junit.Assert.assertEquals;

public class OsTest {

    private SymbolTable table;

    @Before
    public void init() {
        table = new SymbolTable();
    }

    @Test
    public void testMultiply() {
        ParseTree exp = makeExp(makeInt("1"), "*", makeInt("1"));
        String generated = new Generator().generateExpression(exp, table);
        assertEquals(commandSeq("push constant 1", "push constant 1", "call Math.multiply 2"), generated);
    }

    @Test
    public void testDivide() {
        ParseTree exp = makeExp(makeInt("1"), "/", makeInt("1"));
        String generated = new Generator().generateExpression(exp, table);
        assertEquals(commandSeq("push constant 1", "push constant 1", "call Math.divide 2"), generated);
    }

    @Test
    public void testEmptyString() {
        ParseTree exp = makeExp(makeString(""));
        String generated = new Generator().generateExpression(exp, table);
        String expected = commandSeq(
                "push constant 1",
                "call String.new 1"
        );
        assertEquals(expected, generated);
    }

    @Test
    public void testNonEmptyString() {
        ParseTree exp = makeExp(makeString("set"));
        String generated = new Generator().generateExpression(exp, table);
        String expected = commandSeq(
                "push constant 3",
                "call String.new 1",
                "push constant 115",
                "call String.appendChar 2",
                "push constant 101",
                "call String.appendChar 2",
                "push constant 116",
                "call String.appendChar 2"
        );
        assertEquals(expected, generated);
    }
}
