package com.nvm.nand.compiler.codegen;

import com.nvm.nand.compiler.parsing.ParseTree;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;

import static com.nvm.nand.compiler.codegen.ParseTreeMaker.*;
import static com.nvm.nand.compiler.codegen.Utils.commandSeq;
import static junit.framework.TestCase.assertEquals;

/**
 * Test functions, methods and constructors.
 * Void functions are tested in do statement tests
 */
public class FunctionCallTest {

    private SymbolTable table;

    private Generator generator;

    @Before
    public void init() {
        table = new SymbolTable();
        table.put("o", "Object", SymbolTable.Entry.Kind.LOCAL);
        SymbolTable classTable = new SymbolTable();
        generator = new Generator();
        generator.setClassTable(classTable);
        generator.setClassName("ClassName");

    }

    @Test
    public void testFunctionCallNoArg() {
        ParseTree call = makeSubroutineCall("ClassName", "f", Arrays.asList());
        String generated = generator.generateSubroutineCall(call, table);
        assertEquals(commandSeq("call ClassName.f 0"), generated);
    }

    @Test
    public void testFunctionCallOneArg() {
        ParseTree call = makeSubroutineCall("ClassName", "f", Arrays.asList(makeExp(makeInt("1"))));
        String generated = generator.generateSubroutineCall(call, table);
        assertEquals(commandSeq("push constant 1", "call ClassName.f 1"), generated);
    }

    @Test
    public void testFunctionCallTwoArgs() {
        ParseTree call = makeSubroutineCall("ClassName", "f", Arrays.asList(makeExp(makeInt("1")), makeExp(makeInt("1"))));
        String generated = generator.generateSubroutineCall(call, table);
        assertEquals(commandSeq("push constant 1", "push constant 1", "call ClassName.f 2"), generated);
    }

    @Test
    public void testFunctionCallArgExp() {
        ParseTree addExp = makeExp(makeInt("1"), "+", makeInt("1"));
        ParseTree call = makeSubroutineCall("ClassName", "f", Arrays.asList(addExp));
        String generated = generator.generateSubroutineCall(call, table);
        assertEquals(commandSeq("push constant 1", "push constant 1", "add", "call ClassName.f 1"), generated);
    }

    @Test
    public void testMethodCallNoArgs() {
        ParseTree methodCall = makeSubroutineCall("o", "f", Arrays.asList());
        String generated = generator.generateSubroutineCall(methodCall, table);
        assertEquals(commandSeq("push local 0", "call Object.f 1"), generated);
    }

    @Test
    public void testMethodCallWithArgs() {
        ParseTree methodCall = makeSubroutineCall("o", "f", Arrays.asList(makeExp(makeInt("1"))));
        String generated = generator.generateSubroutineCall(methodCall, table);
        assertEquals(commandSeq("push local 0", "push constant 1", "call Object.f 2"), generated);
    }

    @Test
    public void testStaticFunctionCall() {
        ParseTree call = makeSubroutineCall("OtherClass", "f", Arrays.asList());
        String generated = generator.generateSubroutineCall(call, table);
        assertEquals(commandSeq("call OtherClass.f 0"), generated);
    }
}