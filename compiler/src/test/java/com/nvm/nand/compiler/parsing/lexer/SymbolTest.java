package com.nvm.nand.compiler.parsing.lexer;

import com.nvm.nand.compiler.parsing.Token.Type;
import org.junit.Test;

import static com.nvm.nand.compiler.parsing.lexer.TestUtils.assertTokenTypeEqual;
import static com.nvm.nand.compiler.parsing.lexer.TestUtils.assertTokenValueEqual;

public class SymbolTest {

    @Test
    public void testTokenizeSingleSymbolCorrectType() {
        assertTokenTypeEqual("{", Type.SYMBOL);
        assertTokenTypeEqual("}", Type.SYMBOL);
        assertTokenTypeEqual("(", Type.SYMBOL);
        assertTokenTypeEqual(")", Type.SYMBOL);
        assertTokenTypeEqual("[", Type.SYMBOL);
        assertTokenTypeEqual("]", Type.SYMBOL);
        assertTokenTypeEqual("{", Type.SYMBOL);
        assertTokenTypeEqual("<", Type.SYMBOL);
        assertTokenTypeEqual(">", Type.SYMBOL);
        assertTokenTypeEqual(".", Type.SYMBOL);
        assertTokenTypeEqual(",", Type.SYMBOL);
        assertTokenTypeEqual(";", Type.SYMBOL);
        assertTokenTypeEqual("+", Type.SYMBOL);
        assertTokenTypeEqual("-", Type.SYMBOL);
        assertTokenTypeEqual("*", Type.SYMBOL);
        assertTokenTypeEqual("/", Type.SYMBOL);
        assertTokenTypeEqual("&", Type.SYMBOL);
        assertTokenTypeEqual("=", Type.SYMBOL);
        assertTokenTypeEqual("~", Type.SYMBOL);
    }

    @Test
    public void testTokenizeSingleSymbolCorrectValue() {
        assertTokenValueEqual("{", "{");
        assertTokenValueEqual("}", "}");
        assertTokenValueEqual("(", "(");
        assertTokenValueEqual(")", ")");
        assertTokenValueEqual("[", "[");
        assertTokenValueEqual("]", "]");
        assertTokenValueEqual("{", "{");
        assertTokenValueEqual("<", "<");
        assertTokenValueEqual(">", ">");
        assertTokenValueEqual(".", ".");
        assertTokenValueEqual(",", ",");
        assertTokenValueEqual(";", ";");
        assertTokenValueEqual("+", "+");
        assertTokenValueEqual("-", "-");
        assertTokenValueEqual("*", "*");
        assertTokenValueEqual("/", "/");
        assertTokenValueEqual("&", "&");
        assertTokenValueEqual("=", "=");
        assertTokenValueEqual("~", "~");
    }

    @Test
    public void testTokenizeSingleSymbolWithWhitespace() {
        assertTokenValueEqual("/** hmmm */{", "{");
        assertTokenValueEqual("   }", "}");
    }

    @Test
    public void testTokenizeNegativeInteger() {
        assertTokenTypeEqual("-5", Type.SYMBOL);
        assertTokenValueEqual("-5", "-");
    }
}