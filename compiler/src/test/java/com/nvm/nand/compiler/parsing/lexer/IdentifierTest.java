package com.nvm.nand.compiler.parsing.lexer;

import com.nvm.nand.compiler.parsing.Token.Type;
import org.junit.Test;

import static com.nvm.nand.compiler.parsing.lexer.TestUtils.assertTokenTypeEqual;
import static com.nvm.nand.compiler.parsing.lexer.TestUtils.assertTokenValueEqual;

public class IdentifierTest {

    @Test
    public void testTokenizeSingleIdentifierCorrectType() {
        assertTokenTypeEqual("some_var", Type.IDENTIFIER);
        assertTokenTypeEqual("some5", Type.IDENTIFIER);
        assertTokenTypeEqual("some5_5", Type.IDENTIFIER);
        assertTokenTypeEqual("some_5", Type.IDENTIFIER);
        assertTokenTypeEqual("___", Type.IDENTIFIER);
        assertTokenTypeEqual("___5", Type.IDENTIFIER);
        assertTokenTypeEqual("___some", Type.IDENTIFIER);
    }

    @Test
    public void testTokenizeSingleIdentifierCorrectValue() {
        assertTokenValueEqual("some_var", "some_var");
        assertTokenValueEqual("some5", "some5");
        assertTokenValueEqual("some5_5", "some5_5");
        assertTokenValueEqual("some_5", "some_5");
        assertTokenValueEqual("___", "___");
        assertTokenValueEqual("___5", "___5");
        assertTokenValueEqual("___some", "___some");
    }

    @Test
    public void testTokenizeSingleIdentifierWithWhitespace() {
        assertTokenValueEqual("/** hmmm */some_var", "some_var");
        assertTokenValueEqual(" some", "some");
    }
}