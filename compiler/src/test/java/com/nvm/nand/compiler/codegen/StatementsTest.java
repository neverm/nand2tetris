package com.nvm.nand.compiler.codegen;

import com.nvm.nand.compiler.codegen.SymbolTable.Entry.Kind;
import com.nvm.nand.compiler.parsing.ParseTree;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;

import static com.nvm.nand.compiler.codegen.ParseTreeMaker.*;
import static com.nvm.nand.compiler.codegen.Utils.commandSeq;
import static org.junit.Assert.assertEquals;

public class StatementsTest {

    private SymbolTable table;

    private Generator generator;

    private ParseTree zeroIndex;

    @Before
    public void init() {
        table = new SymbolTable();
        table.put("a", "int", Kind.LOCAL);
        table.put("field1", "int", Kind.FIELD);
        table.put("b", "object", Kind.LOCAL);
        table.put("c", "object", Kind.LOCAL);
        generator = new Generator();
        generator.setClassName("ClassName");
        generator.setClassTable(new SymbolTable());
        zeroIndex = makeExp(makeInt("0"));
    }

    @Test
    public void testLetConst() {
        ParseTree statement = makeLetVariable("a", makeExp(makeInt("1")));
        String generated = generator.generateLet(statement, table);
        String expected = commandSeq(
                "push constant 1",
                "pop local 0"
        );
        assertEquals(expected, generated);
    }

    @Test
    public void testLetExp() {
        ParseTree statement = makeLetVariable("a", makeExp(makeInt("1"), "+",
                makeInt("1")));
        String generated = generator.generateLet(statement, table);
        String expected = commandSeq(
                "push constant 1",
                "push constant 1",
                "add",
                "pop local 0"
        );
        assertEquals(expected, generated);
    }

    @Test
    public void testLetField() {
        table.put("this", "object", Kind.ARGUMENT);
        ParseTree statement = makeLetVariable("field1", makeExp(makeInt("1")));
        String generated = generator.generateLet(statement, table);
        String expected = commandSeq(
                "push constant 1",
                "pop this 0"
        );
        assertEquals(expected, generated);
    }

    @Test
    public void testLetSecondField() {
        table.put("this", "object", Kind.ARGUMENT);
        table.put("field2", "int", Kind.FIELD);
        ParseTree statement = makeLetVariable("field2", makeExp(makeInt("1")));
        String generated = generator.generateLet(statement, table);
        String expected = commandSeq(
                "push constant 1",
                "pop this 1"
        );
        assertEquals(expected, generated);
    }

    @Test
    public void testLetFieldToField() {
        table.put("this", "object", Kind.ARGUMENT);
        table.put("field2", "int", Kind.FIELD);
        ParseTree statement = makeLetVariable("field2", makeExp(makeTermIdentifier("field1")));
        String generated = generator.generateLet(statement, table);
        String expected = commandSeq(
                "push this 0", // field1 on top of the stack
                "pop this 1" // write into field2
        );
        assertEquals(expected, generated);
    }

    @Test
    public void testLetArray() {
        ParseTree statement = makeLetVariable("a", makeExp(makeArrayAccess("b", zeroIndex)));
        String generated = generator.generateLet(statement, table);
        String expected = commandSeq(
                "push constant 0",
                "push local 1",
                "add",
                "pop pointer 1",
                "push that 0",
                "pop local 0"
        );
        assertEquals(expected, generated);
    }

    @Test
    public void testLetArrayToArray() {
        ParseTree statement = makeLetArray(
                "b",
                zeroIndex,
                makeExp(makeArrayAccess("c", zeroIndex))
        );
        String generated = generator.generateLet(statement, table);
        String expected = commandSeq(
                "push constant 0",
                "push local 2",
                "add",
                "pop pointer 1",
                "push that 0",
                "pop temp 0", // save c[0] at temp
                "push constant 0",
                "push local 1",
                "add",
                "pop pointer 1",
                "push temp 0",
                "pop that 0" // write c[0] to b[0]
        );
        assertEquals(expected, generated);
    }

    @Test
    public void testDoStatement() {
        ParseTree statement = makeDo(makeSubroutineCall("ClassName", "f", Arrays.asList()));
        String generated = generator.generateDo(statement, table);
        String expected = commandSeq(
                "call ClassName.f 0",
                "pop temp 0" // dump result of calling void function
        );
        assertEquals(expected, generated);
    }

    @Test
    public void testDoStatementWithOtherClass() {
        ParseTree statement = makeDo(makeSubroutineCall("OtherClass", "f", Arrays.asList()));
        String generated = generator.generateDo(statement, table);
        String expected = commandSeq(
                "call OtherClass.f 0",
                "pop temp 0" // dump result of calling void function
        );
        assertEquals(expected, generated);
    }

    @Test
    public void testDoStatementWithMethodCall() {
        ParseTree statement = makeDo(makeSubroutineCall("o", "f", Arrays.asList()));
        table.put("o", "ClassType", Kind.LOCAL);
        String generated = generator.generateDo(statement, table);
        String expected = commandSeq(
                "push local 3",
                "call ClassType.f 1",
                "pop temp 0" // dump result of calling void function
        );
        assertEquals(expected, generated);
    }

    @Test
    public void testEmptyReturn() {
        ParseTree statement = makeReturn();
        String generated = generator.generateReturn(statement, table);
        String expected = commandSeq(
                "push constant 0",
                "return"
        );
        assertEquals(expected, generated);
    }

    @Test
    public void testReturnConstant() {
        ParseTree statement = makeReturn(makeExp(makeInt("1")));
        String generated = generator.generateReturn(statement, table);
        String expected = commandSeq(
                "push constant 1",
                "return"
        );
        assertEquals(expected, generated);
    }

    @Test
    public void testReturnExp() {
        ParseTree statement = makeReturn(makeExp(makeInt("1"), "+", makeInt("2")));
        String generated = generator.generateReturn(statement, table);
        String expected = commandSeq(
                "push constant 1",
                "push constant 2",
                "add",
                "return"
        );
        assertEquals(expected, generated);
    }
}