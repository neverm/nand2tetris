package com.nvm.nand.compiler.codegen;

import com.nvm.nand.compiler.codegen.SymbolTable.Entry.Kind;
import com.nvm.nand.compiler.parsing.ParseTree;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;

import static com.nvm.nand.compiler.codegen.ParseTreeMaker.*;
import static com.nvm.nand.compiler.codegen.Utils.commandSeq;
import static org.junit.Assert.assertEquals;

public class ExpressionsTest {

    private SymbolTable table;

    @Before
    public void init() {
        table = new SymbolTable();
    }

    @Test
    public void testIntConstant() {
        ParseTree constOne = makeExp(makeInt("1"));
        String generated = new Generator().generateExpression(constOne, table);
        assertEquals("push constant 1\n", generated);
    }

    @Test
    public void testOnePlusOne() {
        ParseTree exp = makeExp(makeInt("1"), "+", makeInt("1"));
        String generated = new Generator().generateExpression(exp, table);
        assertEquals(commandSeq("push constant 1", "push constant 1", "add"), generated);
    }

    @Test
    public void testMixVarConst() {
        table.put("x", "int", Kind.LOCAL);
        ParseTree exp = makeExp(makeInt("1"), "+", makeTermIdentifier("x"));
        String generated = new Generator().generateExpression(exp, table);
        assertEquals(commandSeq("push constant 1", "push local 0", "add"), generated);
    }

    @Test
    public void testMinusOne() {
        ParseTree exp = makeExp(makeUnary("-", makeInt("1")));
        String generated = new Generator().generateExpression(exp, table);
        assertEquals(commandSeq("push constant 1", "neg"), generated);
    }

    @Test
    public void testFalse() {
        ParseTree exp = makeExp(makeTermKeyword("false"));
        String generated = new Generator().generateExpression(exp, table);
        assertEquals(commandSeq("push constant 0"), generated);
    }

    @Test
    public void testTrue() {
        ParseTree exp = makeExp(makeTermKeyword("true"));
        String generated = new Generator().generateExpression(exp, table);
        assertEquals(commandSeq("push constant 1", "neg"), generated);
    }

    @Test
    public void testNull() {
        ParseTree exp = makeExp(makeTermKeyword("null"));
        String generated = new Generator().generateExpression(exp, table);
        assertEquals(commandSeq("push constant 0"), generated);
    }

    @Test
    public void testFlatLongExpression() {
        ParseTree exp = new ParseTree(
                ParseTree.Name.EXPRESSION,
                Arrays.asList(
                        makeInt("1"),
                        makeSymbol("+"),
                        makeInt("2"),
                        makeSymbol("+"),
                        makeInt("3"),
                        makeSymbol("+"),
                        makeInt("4")
                )
        );
        String generated = new Generator().generateExpression(exp, table);
        String expected = commandSeq(
                "push constant 1",
                "push constant 2",
                "add",
                "push constant 3",
                "add",
                "push constant 4",
                "add"
        );
        assertEquals(expected, generated);
    }

    @Test
    public void testParenthesizedExpression() {
        ParseTree exp = new ParseTree(
                ParseTree.Name.EXPRESSION,
                Arrays.asList(
                        makeInt("1"),
                        makeSymbol("+"),
                        makeTerm(Arrays.asList(
                                makeSymbol("("),
                                makeExp(makeInt("2"), "+", makeInt("3")),
                                makeSymbol(")")
                        )),
                        makeSymbol("+"),
                        makeInt("4")
                )
        );
        String generated = new Generator().generateExpression(exp, table);
        String expected = commandSeq(
                "push constant 1",
                "push constant 2",
                "push constant 3",
                "add",
                "add",
                "push constant 4",
                "add"
        );
        assertEquals(expected, generated);
    }
}