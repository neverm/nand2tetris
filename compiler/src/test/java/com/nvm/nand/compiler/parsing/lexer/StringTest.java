package com.nvm.nand.compiler.parsing.lexer;

import com.nvm.nand.compiler.parsing.Token.Type;
import org.junit.Test;

import static com.nvm.nand.compiler.parsing.lexer.TestUtils.assertTokenTypeEqual;
import static com.nvm.nand.compiler.parsing.lexer.TestUtils.assertTokenValueEqual;

public class StringTest {

    @Test
    public void testTokenizeSingleStringConstantCorrectType() {
        assertTokenTypeEqual("\"hello\"", Type.STR_CONSTANT);
        assertTokenTypeEqual("\"\"", Type.STR_CONSTANT);
        assertTokenTypeEqual("\"/* comment in double quotes */\"", Type.STR_CONSTANT);
        assertTokenTypeEqual("\"// comment in double quotes\"", Type.STR_CONSTANT);
        assertTokenTypeEqual("\"return something\"", Type.STR_CONSTANT);
    }

    @Test
    public void testTokenizeSingleStringConstantCorrectValue() {
        assertTokenValueEqual("\"hello\"", "hello");
        assertTokenValueEqual("\"\"", "");
        assertTokenValueEqual("\"/* comment in double quotes */\"", "/* comment in double quotes */");
        assertTokenValueEqual("\"// comment in double quotes\"", "// comment in double quotes");
        assertTokenValueEqual("\"return something\"", "return something");
    }

    @Test
    public void testTokenizeSingleStringConstantWithWhitespace() {
        assertTokenValueEqual("/** some comment */\"hello\"", "hello");
        assertTokenValueEqual(" \"hello\"", "hello");
    }
}