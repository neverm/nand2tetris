package com.nvm.nand.compiler.parsing.parser;

import com.nvm.nand.compiler.parsing.ParseException;
import com.nvm.nand.compiler.parsing.ParseTree;
import com.nvm.nand.compiler.parsing.Parser;
import com.nvm.nand.compiler.parsing.Token;
import org.junit.Test;

import java.util.List;

import static com.nvm.nand.compiler.parsing.ParseTree.*;
import static com.nvm.nand.compiler.parsing.ParseTree.NodeType.EMPTY;
import static com.nvm.nand.compiler.parsing.ParseTree.NodeType.NON_TERMINAL;
import static com.nvm.nand.compiler.parsing.ParseTree.NodeType.TERMINAL;
import static com.nvm.nand.compiler.parsing.Token.Type.*;
import static com.nvm.nand.compiler.parsing.parser.TestUtils.*;
import static com.nvm.nand.compiler.parsing.parser.TestUtils.values;
import static org.junit.Assert.assertEquals;

public class ClassTest {

    @Test
    public void testEmptyClass() throws ParseException {
        List<Token> ts = tokens(types(KEYWORD, IDENTIFIER, SYMBOL, SYMBOL),
                values("class", "A", "{", "}"));
        ParseTree t = new Parser(ts).parseClass();
        assertTree(t, NON_TERMINAL, Name.CLASS);
        List<ParseTree> children = t.getChildren();
        assertEquals(4, children.size());
        assertTree(children.get(0), TERMINAL, Name.KEYWORD, "class");
        assertTree(children.get(1), TERMINAL, Name.IDENTIFIER, "A");
        assertTree(children.get(2), TERMINAL, Name.SYMBOL, "{");
        assertTree(children.get(3), TERMINAL, Name.SYMBOL, "}");
    }

    @Test
    public void testClassNoVariables() throws ParseException {
        List<Token> ts = tokens(types(KEYWORD, IDENTIFIER, SYMBOL, KEYWORD, KEYWORD, IDENTIFIER,
                SYMBOL, SYMBOL, SYMBOL, KEYWORD, SYMBOL, SYMBOL, SYMBOL),
                values("class", "A", "{", "function", "void", "f", "(", ")", "{", "return", ";", "}", "}"));
        ParseTree t = new Parser(ts).parseClass();
        assertTree(t, NON_TERMINAL, Name.CLASS);
        List<ParseTree> children = t.getChildren();
        assertEquals(5, children.size());
        assertTree(children.get(0), TERMINAL, Name.KEYWORD, "class");
        assertTree(children.get(1), TERMINAL, Name.IDENTIFIER, "A");
        assertTree(children.get(2), TERMINAL, Name.SYMBOL, "{");
        assertTree(children.get(3), NON_TERMINAL, Name.SUBROUTINE_DECLARATION);
        assertTree(children.get(4), TERMINAL, Name.SYMBOL, "}");
    }

    @Test
    public void testClassNoSubroutine() throws ParseException {
        List<Token> ts = tokens(types(KEYWORD, IDENTIFIER,  SYMBOL, KEYWORD, KEYWORD, IDENTIFIER, SYMBOL, SYMBOL),
                values("class", "A", "{", "field", "int", "x", ";", "}"));
        ParseTree t = new Parser(ts).parseClass();
        assertTree(t, NON_TERMINAL, Name.CLASS);
        List<ParseTree> children = t.getChildren();
        assertEquals(5, children.size());
        assertTree(children.get(0), TERMINAL, Name.KEYWORD, "class");
        assertTree(children.get(1), TERMINAL, Name.IDENTIFIER, "A");
        assertTree(children.get(2), TERMINAL, Name.SYMBOL, "{");
        assertTree(children.get(3), NON_TERMINAL, Name.CLASS_VAR);
        assertTree(children.get(4), TERMINAL, Name.SYMBOL, "}");
    }

    @Test
    public void testClassFieldVariableDeclaration() throws ParseException {
        List<Token> ts = tokens(types(KEYWORD, KEYWORD, IDENTIFIER, SYMBOL),
                values("field", "int", "x", ";"));
        ParseTree t = new Parser(ts).parseClassVariableDeclaration();
        assertTree(t, NON_TERMINAL, Name.CLASS_VAR);
        List<ParseTree> children = t.getChildren();
        assertEquals(4, children.size());
        assertTree(children.get(0), TERMINAL, Name.KEYWORD, "field");
        assertTree(children.get(1), TERMINAL, Name.KEYWORD, "int");
        assertTree(children.get(2), TERMINAL, Name.IDENTIFIER, "x");
        assertTree(children.get(3), TERMINAL, Name.SYMBOL, ";");
    }

    @Test
    public void testClassStaticVariableDeclaration() throws ParseException {
        List<Token> ts = tokens(types(KEYWORD, KEYWORD, IDENTIFIER, SYMBOL),
                values("static", "int", "x", ";"));
        ParseTree t = new Parser(ts).parseClassVariableDeclaration();
        assertTree(t, NON_TERMINAL, Name.CLASS_VAR);
        List<ParseTree> children = t.getChildren();
        assertEquals(4, children.size());
        assertTree(children.get(0), TERMINAL, Name.KEYWORD, "static");
        assertTree(children.get(1), TERMINAL, Name.KEYWORD, "int");
        assertTree(children.get(2), TERMINAL, Name.IDENTIFIER, "x");
        assertTree(children.get(3), TERMINAL, Name.SYMBOL, ";");
    }

    @Test
    public void testClassVariableDeclarationMultipleVariables() throws ParseException {
        List<Token> ts = tokens(types(KEYWORD, KEYWORD, IDENTIFIER, SYMBOL, IDENTIFIER, SYMBOL),
                values("field", "int", "x", ",", "y", ";"));
        ParseTree t = new Parser(ts).parseClassVariableDeclaration();
        assertTree(t, NON_TERMINAL, Name.CLASS_VAR);
        List<ParseTree> children = t.getChildren();
        assertEquals(6, children.size());
        assertTree(children.get(0), TERMINAL, Name.KEYWORD, "field");
        assertTree(children.get(1), TERMINAL, Name.KEYWORD, "int");
        assertTree(children.get(2), TERMINAL, Name.IDENTIFIER, "x");
        assertTree(children.get(3), TERMINAL, Name.SYMBOL, ",");
        assertTree(children.get(4), TERMINAL, Name.IDENTIFIER, "y");
        assertTree(children.get(5), TERMINAL, Name.SYMBOL, ";");
    }

    @Test
    public void testClassVariableDeclarationClassType() throws ParseException {
        List<Token> ts = tokens(types(KEYWORD, IDENTIFIER, IDENTIFIER, SYMBOL),
                values("field", "Point", "x", ";"));
        ParseTree t = new Parser(ts).parseClassVariableDeclaration();
        assertTree(t, NON_TERMINAL, Name.CLASS_VAR);
        List<ParseTree> children = t.getChildren();
        assertEquals(4, children.size());
        assertTree(children.get(0), TERMINAL, Name.KEYWORD, "field");
        assertTree(children.get(1), TERMINAL, Name.IDENTIFIER, "Point");
        assertTree(children.get(2), TERMINAL, Name.IDENTIFIER, "x");
        assertTree(children.get(3), TERMINAL, Name.SYMBOL, ";");
    }

    @Test
    public void testConstructorSubroutine() throws ParseException {
        List<Token> ts = tokens(types(KEYWORD, IDENTIFIER, IDENTIFIER, SYMBOL, SYMBOL, SYMBOL, SYMBOL),
                values("constructor", "Point", "new", "(", ")", "{", "}"));
        ParseTree t = new Parser(ts).parseSubroutineDeclaration();
        assertTree(t, NON_TERMINAL, Name.SUBROUTINE_DECLARATION);
        List<ParseTree> children = t.getChildren();
        assertEquals(7, children.size());
        assertTree(children.get(0), TERMINAL, Name.KEYWORD, "constructor");
        assertTree(children.get(1), TERMINAL, Name.IDENTIFIER, "Point");
        assertTree(children.get(2), TERMINAL, Name.IDENTIFIER, "new");
        assertTree(children.get(3), TERMINAL, Name.SYMBOL, "(");
        assertTree(children.get(4), EMPTY, Name.PARAMETERS_LIST);
        assertTree(children.get(5), TERMINAL, Name.SYMBOL, ")");
        assertTree(children.get(6), NON_TERMINAL, Name.SUBROUTINE_BODY);
    }

    @Test
    public void testFunctionSubroutine() throws ParseException {
        List<Token> ts = tokens(types(KEYWORD, IDENTIFIER, IDENTIFIER, SYMBOL, SYMBOL, SYMBOL, SYMBOL),
                values("function", "Point", "new", "(", ")", "{", "}"));
        ParseTree t = new Parser(ts).parseSubroutineDeclaration();
        assertTree(t, NON_TERMINAL, Name.SUBROUTINE_DECLARATION);
        List<ParseTree> children = t.getChildren();
        assertEquals(7, children.size());
        assertTree(children.get(0), TERMINAL, Name.KEYWORD, "function");
        assertTree(children.get(1), TERMINAL, Name.IDENTIFIER, "Point");
        assertTree(children.get(2), TERMINAL, Name.IDENTIFIER, "new");
        assertTree(children.get(3), TERMINAL, Name.SYMBOL, "(");
        assertTree(children.get(4), EMPTY, Name.PARAMETERS_LIST);
        assertTree(children.get(5), TERMINAL, Name.SYMBOL, ")");
        assertTree(children.get(6), NON_TERMINAL, Name.SUBROUTINE_BODY);
    }

    @Test
    public void testMethodSubroutine() throws ParseException {
        List<Token> ts = tokens(types(KEYWORD, IDENTIFIER, IDENTIFIER, SYMBOL, SYMBOL, SYMBOL, SYMBOL),
                values("method", "Point", "new", "(", ")", "{", "}"));
        ParseTree t = new Parser(ts).parseSubroutineDeclaration();
        assertTree(t, NON_TERMINAL, Name.SUBROUTINE_DECLARATION);
        List<ParseTree> children = t.getChildren();
        assertEquals(7, children.size());
        assertTree(children.get(0), TERMINAL, Name.KEYWORD, "method");
        assertTree(children.get(1), TERMINAL, Name.IDENTIFIER, "Point");
        assertTree(children.get(2), TERMINAL, Name.IDENTIFIER, "new");
        assertTree(children.get(3), TERMINAL, Name.SYMBOL, "(");
        assertTree(children.get(4), EMPTY, Name.PARAMETERS_LIST);
        assertTree(children.get(5), TERMINAL, Name.SYMBOL, ")");
        assertTree(children.get(6), NON_TERMINAL, Name.SUBROUTINE_BODY);
    }

    @Test
    public void testParametersList() throws ParseException {
        List<Token> ts = tokens(types(KEYWORD, IDENTIFIER, SYMBOL, KEYWORD, IDENTIFIER),
                values("int", "x", ",", "int", "y"));
        ParseTree t = new Parser(ts).parseParametersList();
        assertTree(t, NON_TERMINAL, Name.PARAMETERS_LIST);
        List<ParseTree> children = t.getChildren();
        assertEquals(5, children.size());
        assertTree(children.get(0), TERMINAL, Name.KEYWORD, "int");
        assertTree(children.get(1), TERMINAL, Name.IDENTIFIER, "x");
        assertTree(children.get(2), TERMINAL, Name.SYMBOL, ",");
        assertTree(children.get(3), TERMINAL, Name.KEYWORD, "int");
        assertTree(children.get(4), TERMINAL, Name.IDENTIFIER, "y");
    }

    @Test
    public void testSubroutineBodyEmpty() throws ParseException {
        List<Token> ts = tokens(types(SYMBOL, SYMBOL), values("{", "}"));
        ParseTree t = new Parser(ts).parseSubroutineBody();
        assertTree(t, NON_TERMINAL, Name.SUBROUTINE_BODY);
        List<ParseTree> children = t.getChildren();
        assertEquals(3, children.size());
        assertTree(children.get(0), TERMINAL, Name.SYMBOL, "{");
        assertTree(children.get(1), EMPTY, Name.STATEMENTS);
        assertTree(children.get(2), TERMINAL, Name.SYMBOL, "}");
    }

    @Test
    public void testSubroutineOnlyVariableDeclaration() throws ParseException {
        List<Token> ts = tokens(types(SYMBOL, KEYWORD, KEYWORD, IDENTIFIER, SYMBOL, SYMBOL),
                values("{", "var", "int", "x", ";", "}"));
        ParseTree t = new Parser(ts).parseSubroutineBody();
        assertTree(t, NON_TERMINAL, Name.SUBROUTINE_BODY);
        List<ParseTree> children = t.getChildren();
        assertEquals(4, children.size());
        assertTree(children.get(0), TERMINAL, Name.SYMBOL, "{");
        assertTree(children.get(1), NON_TERMINAL, Name.VARIABLE_DECLARATION);
        assertTree(children.get(2), EMPTY, Name.STATEMENTS);
        assertTree(children.get(3), TERMINAL, Name.SYMBOL, "}");
    }

    @Test
    public void testSubroutineOnlyStatements() throws ParseException {
        List<Token> ts = tokens(types(SYMBOL, KEYWORD, SYMBOL, SYMBOL), values("{", "return", ";", "}"));
        ParseTree t = new Parser(ts).parseSubroutineBody();
        assertTree(t, NON_TERMINAL, Name.SUBROUTINE_BODY);
        List<ParseTree> children = t.getChildren();
        assertEquals(3, children.size());
        assertTree(children.get(0), TERMINAL, Name.SYMBOL, "{");
        assertTree(children.get(1), NON_TERMINAL, Name.STATEMENTS);
        assertTree(children.get(2), TERMINAL, Name.SYMBOL, "}");
    }

    @Test
    public void testVariableDecSingle() throws ParseException {
        List<Token> ts = tokens(types(KEYWORD, KEYWORD, IDENTIFIER, SYMBOL), values("var", "int", "x", ";"));
        ParseTree t = new Parser(ts).parseVariableDeclaration();
        assertTree(t, NON_TERMINAL, Name.VARIABLE_DECLARATION);
        List<ParseTree> children = t.getChildren();
        assertEquals(4, children.size());
        assertTree(children.get(0), TERMINAL, Name.KEYWORD, "var");
        assertTree(children.get(1), TERMINAL, Name.KEYWORD, "int");
        assertTree(children.get(2), TERMINAL, Name.IDENTIFIER, "x");
        assertTree(children.get(3), TERMINAL, Name.SYMBOL, ";");
    }

    @Test
    public void testVariableDecMultiple() throws ParseException {
        List<Token> ts = tokens(types(KEYWORD, KEYWORD, IDENTIFIER, SYMBOL, IDENTIFIER, SYMBOL),
                values("var", "int", "x", ",", "y", ";"));
        ParseTree t = new Parser(ts).parseVariableDeclaration();
        assertTree(t, NON_TERMINAL, Name.VARIABLE_DECLARATION);
        List<ParseTree> children = t.getChildren();
        assertEquals(6, children.size());
        assertTree(children.get(0), TERMINAL, Name.KEYWORD, "var");
        assertTree(children.get(1), TERMINAL, Name.KEYWORD, "int");
        assertTree(children.get(2), TERMINAL, Name.IDENTIFIER, "x");
        assertTree(children.get(3), TERMINAL, Name.SYMBOL, ",");
        assertTree(children.get(4), TERMINAL, Name.IDENTIFIER, "y");
        assertTree(children.get(5), TERMINAL, Name.SYMBOL, ";");
    }
}
