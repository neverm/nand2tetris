package com.nvm.nand.compiler.codegen;

import com.nvm.nand.compiler.codegen.SymbolTable.Entry.Kind;
import com.nvm.nand.compiler.parsing.ParseTree;
import org.junit.Before;
import org.junit.Test;

import static com.nvm.nand.compiler.codegen.ParseTreeMaker.makeExp;
import static com.nvm.nand.compiler.codegen.ParseTreeMaker.makeTermIdentifier;
import static org.junit.Assert.*;

public class VariablesTest {

    private SymbolTable table;
    private ParseTree accessX;

    @Before
    public void init() {
        table = new SymbolTable();
        accessX = makeExp(makeTermIdentifier("x"));
    }

    @Test
    public void testVarSingleArgument() {
        table.put("x", "int", Kind.ARGUMENT);
        String generated = new Generator().generateExpression(accessX, table);
        assertEquals("push argument 0\n", generated);
    }

    @Test
    public void testVarSecondArgument() {
        table.put("y", "int", Kind.ARGUMENT);
        table.put("x", "int", Kind.ARGUMENT);
        String generated = new Generator().generateExpression(accessX, table);
        assertEquals("push argument 1\n", generated);
    }

    @Test
    public void testVarLocal() {
        table.put("x", "int", Kind.LOCAL);
        String generated = new Generator().generateExpression(accessX, table);
        assertEquals("push local 0\n", generated);
    }

    @Test
    public void testVarStatic() {
        table.put("x", "int", Kind.STATIC);
        String generated = new Generator().generateExpression(accessX, table);
        assertEquals("push static 0\n", generated);
    }
}