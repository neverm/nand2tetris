package com.nvm.nand.asm;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import java.util.HashMap;
import java.util.Map;
import com.nvm.nand.asm.CodeGenerator.UndefinedSymbolException;
import com.nvm.nand.asm.Parser.ParseErrorException;

import static org.junit.Assert.*;


/**
 * Created by nvm on 3/29/17.
 */
public class CodeGeneratorTest {

    private static Assembler.SymbolTable mockTable;

    private Parser parser;

    @BeforeClass
    public static void setUpClass() {
        Map<String, Integer> vals = new HashMap<>();
        vals.put("mock-key", 1);
        mockTable = new Assembler.SymbolTable(vals);
    }

    @Before
    public void setUp() {
        parser = new Parser();
    }

    @Test
    public void generatesFromConstant() throws ParseErrorException, UndefinedSymbolException {
        parser.feedLine("@10");
        String res = CodeGenerator.generateACommand(parser.getACommandValue(), mockTable);
        assertEquals("0000000000001010", res);
    }

    @Test
    public void roundsConstant() throws ParseErrorException, UndefinedSymbolException {
        parser.feedLine("@65536");
        String res = CodeGenerator.generateACommand(parser.getACommandValue(), mockTable);
        assertEquals("0000000000000000", res);
    }

    @Test
    public void generatesFromSymbol() throws ParseErrorException, UndefinedSymbolException {
        parser.feedLine("@mock-key");
        String res = CodeGenerator.generateACommand(parser.getACommandValue(), mockTable);
        assertEquals("0000000000000001", res);
    }
}
