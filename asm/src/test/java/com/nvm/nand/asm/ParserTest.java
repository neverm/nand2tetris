package com.nvm.nand.asm;

import org.junit.Before;
import org.junit.Test;
import com.nvm.nand.asm.Parser.ParseErrorException;
import com.nvm.nand.asm.Parser.CommandType;

import static org.junit.Assert.*;

/**
 * Created by nvm on 3/24/17.
 */
public class ParserTest {

    private Parser parser;

    @Before
    public void setUp() {
        parser = new Parser();
    }

    @Test
    public void advancesLineNumber() throws ParseErrorException {
        String line = "D=M";
        assertEquals(0, parser.getNextCommandAddr());
        parser.feedLine(line);
        assertEquals(1, parser.getNextCommandAddr());
        parser.feedLine(line);
        assertEquals(2, parser.getNextCommandAddr());
    }

    @Test
    public void doesNotCountLabel() throws ParseErrorException {
        parser.feedLine("D=M");
        assertEquals(1, parser.getNextCommandAddr());
        parser.feedLine("(LABEL)");
        assertEquals(1, parser.getNextCommandAddr());
    }

    @Test
    public void recognizesTypeEmpty() throws ParseErrorException {
        assertEquals("Command type should be empty", CommandType.EMPTY, parser.feedLine(""));
    }

    @Test
    public void recognizesTypeSpaces() throws ParseErrorException {
        assertEquals("Command type should be empty", CommandType.EMPTY, parser.feedLine("     "));
    }

    @Test
    public void recognizesTypeComment() throws ParseErrorException {
        assertEquals("Command type should be empty", CommandType.EMPTY, parser.feedLine("// this is a comment"));
    }

    @Test
    public void recognizesTypeLabel() throws ParseErrorException {
        assertEquals("Command type should be L_COMMAND", CommandType.L_COMMAND, parser.feedLine("(loop)"));
    }

    @Test
    public void recognizesTypeAddress() throws ParseErrorException {
        assertEquals("Command type should be A_COMMAND", CommandType.A_COMMAND, parser.feedLine("@15"));
        assertEquals("Command type should be A_COMMAND", CommandType.A_COMMAND, parser.feedLine("@name"));
    }

    @Test
    public void recognizesTypeCompute() throws ParseErrorException {
        assertEquals("Command type should be C_COMMAND", CommandType.C_COMMAND, parser.feedLine("D=1"));
    }

    @Test(expected = ParseErrorException.class)
    public void throwsWhenIllegalCommand() throws ParseErrorException {
        parser.feedLine("some text");
    }

    @Test
    public void passesValidProgram() {
        String[] validText = new String[] {
                "//start if the program",
                "\n",
                "@i",
                "M=1",
                "\n",
                "    ",
                "(LOOP)",
                "@i",
                "D=M",
                "@1",
                "D=D+M",
                "@LOOP",
                "0;JMP"
        };
        try {
            for (String line: validText) {
                parser.feedLine(line);
            }
        }
        catch (ParseErrorException ex) {
            fail("Parse Error on a valid command: " + ex.getMessage());
        }
    }

    @Test
    public void rejectsInvalidSymbols() {

        String[] invalidSymbols = {
                "", "3name", " name", "@", "@name",
                "na!me", "na me", "name ", "na@me"
        };
        for (String symbol: invalidSymbols) {
            assertFalse("symbol " + symbol + " should be invalid", parser.validateSymbol(symbol));
        }
    }

    @Test
    public void passesValidSymbols() {
        String[] validSymbols = {
                "$name", "name", "na-me", "-name", "name-", "na1me",
                "_name", "na_me", "name_", "$name", "na.me", ".name",
                "name.", ":name", "na:me", "name:", "name:", "name5",
                ".", "$", "-", "_", ":", "_5"
        };
        for (String symbol: validSymbols) {
            assertTrue("symbol " + symbol + " should be valid", parser.validateSymbol(symbol));
        }
    }

    @Test(expected = ParseErrorException.class)
    public void getCCommandPartThrowsException() throws ParseErrorException {
        parser.feedLine("@a");
        parser.getCommandComp();
    }

    @Test
    public void getsCompPart() throws ParseErrorException {
        parser.feedLine("D=1;JMP");
        assertEquals("1", parser.getCommandComp());
        parser.feedLine("0;JMP");
        assertEquals("0", parser.getCommandComp());
        parser.feedLine("D=M");
        assertEquals("M", parser.getCommandComp());
        parser.feedLine("M=M-D");
        assertEquals("M-D", parser.getCommandComp());
    }

    @Test
    public void getsDestPart() throws ParseErrorException {
        parser.feedLine("D=0;JMP");
        assertEquals("D", parser.getCommandDest());
        parser.feedLine("D=0");
        assertEquals("D", parser.getCommandDest());
        parser.feedLine("0;JMP");
        assertEquals(null, parser.getCommandDest());
    }

    @Test
    public void getsJmpPart() throws ParseErrorException {
        parser.feedLine("D=0;JMP");
        assertEquals("JMP", parser.getCommandJmp());
        parser.feedLine("D=0");
        assertEquals(null, parser.getCommandJmp());
        parser.feedLine("0;JMP");
        assertEquals("JMP", parser.getCommandJmp());
    }

    @Test(expected = ParseErrorException.class)
    public void getLabelSymbolThrowsException() throws ParseErrorException {
        parser.feedLine("@CHECK");
        parser.getLabelSymbol();
    }

    @Test
    public void getLabelSymbol() throws ParseErrorException {
        parser.feedLine("(CHECK)");
        assertEquals("CHECK", parser.getLabelSymbol());
    }

    @Test
    public void getACommandVariable() throws ParseErrorException {
        parser.feedLine("@var");
        assertEquals("var", parser.getACommandValue());
    }

    @Test
    public void getACommandConstant() throws ParseErrorException {
        parser.feedLine("@100");
        assertEquals("100", parser.getACommandValue());
    }

    @Test
    public void parseCommentWithCode() throws ParseErrorException {
        parser.feedLine("   D=M              // D = first number");
        assertEquals("D", parser.getCommandDest());
        assertEquals("M", parser.getCommandComp());
    }

}