package com.nvm.nand.asm;

import org.junit.Before;
import org.junit.Test;

import java.io.*;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;

import static org.junit.Assert.*;

/**
 * Created by nvm on 3/30/17.
 */
public class AssemblerTest {

    private Assembler asm;

    @Before
    public void setUp() {
        asm = new Assembler();
    }

    @Test
    public void fillsSymbolTableTrivialLabel() throws Parser.ParseErrorException {
        String startSymb = "START";
        String loopSymb = "LOOP";
        assertFalse(asm.getSymbolTable().hasKey(startSymb));
        asm.passOne(listOf(
                "(" + startSymb + ")",
                "D=M",
                "// loop",
                "(" + loopSymb + ")",
                "@1",
                "D=D-A"
                ));
        assertEquals(0, asm.getSymbolTable().get(startSymb));
        assertEquals(1, asm.getSymbolTable().get(loopSymb));
    }

    @Test
    public void fillsSymbolTableTrivialVariables() throws Parser.ParseErrorException,
            CodeGenerator.UndefinedSymbolException {
        asm.passTwo(listOf(
                "@i",
                "D=M",
                "@j",
                "D=M"
        ));
        assertEquals(16, asm.getSymbolTable().get("i"));
        assertEquals(17, asm.getSymbolTable().get("j"));
    }

    @Test
    public void fillsSymbolTableReferenceAhead() throws Parser.ParseErrorException,
            CodeGenerator.UndefinedSymbolException {
        List<String> program = listOf(
                "@LOOP", // addr 0
                "0;JMP", // addr 1
                "(LOOP)",
                "D=M" // addr 2
        );
        asm.passOne(program);
        asm.passTwo(program);
        assertEquals(2, asm.getSymbolTable().get("LOOP"));
    }

    @Test
    public void generatesSimpleCode() throws Parser.ParseErrorException,
            CodeGenerator.UndefinedSymbolException {
        List<String> binary = asm.passTwo(listOf(
                "@5",
                "A=D&A"
        ));
        assertEquals("0000000000000101", binary.get(0));
        assertEquals("1110000000100000", binary.get(1));
    }

    @Test
    public void constantDoesNotIncrementVariableMemoryLoc() throws Parser.ParseErrorException,
            CodeGenerator.UndefinedSymbolException {
        List<String> binary = asm.passTwo(listOf(
                "@0",
                "@counter"
        ));
        assertEquals(16, asm.getSymbolTable().get("counter"));
    }

    private List<String> listOf(String... strs) {
        List<String> res = new ArrayList<>();
        for (String str: strs) {
            res.add(str);
        }
        return res;
    }
}
