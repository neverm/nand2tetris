package com.nvm.nand.asm;

/**
 * Created by nvm on 3/29/17.
 */
public class CodeGenerator {

    public static class UndefinedSymbolException extends Exception {}


    private static final int MAX_ALLOWED_VALUE = ~(-1<<Specification.MAX_ADDR_BITS);

    /**
     * Generate binary code for the C-command specified by destination, computation and jump
     * parts with respect to current specification
     * @param dest destination part, may be null
     * @param comp computation part
     * @param jmp jump part, may be null
     * @return binary code as string of 0's and 1's
     */
    public static String generateCCommand(String dest, String comp, String jmp) {
        return Specification.getCCodeBinary(dest, comp, jmp);
    }

    /**
     * Generate A-command specified by value of the command (command line without @ char)
     * in case value of command is a number, it's converted to binary, throwing out bits higher than MAX_ADDR_BITS
     * in case value of command is a constant, it's looked up in symbolTable
     * if symbol is not present in the table UndefinedSymbolException is thrown
     * @param value
     * @param symbolTable
     * @return binary representation of given A-command value
     * @throws UndefinedSymbolException
     */
    public static String generateACommand(String value, Assembler.SymbolTable symbolTable)
            throws UndefinedSymbolException {
        int addr;
        try {
            addr = Integer.parseInt(value);
        }
        catch (NumberFormatException ex) {
            addr = symbolTable.get(value);
        }
        if (addr == -1) {
            throw new UndefinedSymbolException();
        }
        // zero everything that doesn't fit in 15 bits
        addr &= MAX_ALLOWED_VALUE;
        // but set 16th bit to 1 temporarily to help conversion to string
        addr |= MAX_ALLOWED_VALUE+1;
        String addrBinary = Integer.toBinaryString(addr);
        // remove 16th bit
        addrBinary = addrBinary.substring(1);
        return Specification.A_COMMAND_PREFIX + addrBinary;
    }
}
