package com.nvm.nand.asm;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Stuff from assembly language specification like available computation functions and mappings from
 * them to binary
 */
public class Specification {

    public static final String C_COMMAND_PREFIX = "111";
    public static final String A_COMMAND_PREFIX = "0";
    public static final int MAX_ADDR_BITS = 15;

    private static final String KEY_NONE = "__key_none";

    private static Set<String> validCCommands;
    private static Map<String, String> destCodes, compCodes, jmpCodes;

    static {
        List<String> validDestinations = Arrays.asList(
                "M", "D", "MD", "A", "M", "AM", "AD", "AMD"
        );
        List<String> validJumps = Arrays.asList(
                "JGT", "JEQ", "JEQ", "JGE", "JLT", "JNE", "JLE", "JMP"
        );
        List<String> validComputations = Arrays.asList(
                "0", "1", "-1", "D", "A", "!D", "!A", "-D", "-A",
                "D+1", "A+1", "D-1", "A-1", "D+A", "D-A", "A-D",
                "D&A", "D|A", "M", "!M", "-M", "M+1", "M-1", "D+M",
                "D-M", "M-D", "D&M", "D|M"
        );
        // Add alternative order of symmetric functions: valid D+M also makes M+D valid
        validComputations =
                Stream.concat(
                        validComputations.stream(),
                        validComputations.stream()
                                .filter(comp -> comp.matches(".*[+|&].*"))
                                .map(comp -> new StringBuffer(comp).reverse().toString())
                ).collect(Collectors.toList());

        validCCommands =
                validComputations
                        .stream()
                        .flatMap(
                                comp -> Stream.concat(
                                        // commands in form comp;jmp
                                        validJumps.stream().map(jmp -> comp + ";" + jmp),
                                        validDestinations.stream().flatMap(
                                                dest -> Stream.concat(
                                                        // commands in form dest=comp
                                                        Stream.of(dest + "=" + comp),
                                                        // commands in form dest=comp;jmp
                                                        validJumps.stream().map(
                                                                jmp -> dest + "=" + comp + ";" + jmp
                                                        )
                                                )
                                        )
                                )
                        )
                        .collect(Collectors.toSet());

        String[][] destCodesArr = {
                {KEY_NONE, "000"}, {"M", "001"}, {"D", "010"},
                {"MD", "011"}, {"A", "100"}, {"AM", "101"},
                {"AD", "110"}, {"AMD", "111"}
        };
        String[][] jumpCodesArr = {
                {KEY_NONE, "000"}, {"JGT", "001"}, {"JEQ", "010"},
                {"JGE", "011"}, {"JLT", "100"}, {"JNE", "101"},
                {"JLE", "110"}, {"JMP", "111"}
        };
        String[][] compCodesArr = {
                {"0", "0101010"}, {"1", "0111111"}, {"-1", "0111010"}, {"D", "0001100"}, {"A", "0110000"},
                {"!D", "0001101"}, {"!A", "0110001"}, {"-D", "0001111"}, {"-A", "0110011"},
                {"D+1", "0011111"}, {"A+1", "0110111"}, {"D-1", "0001110"}, {"A-1", "0110010"},
                {"D+A", "0000010"}, {"D-A", "0010011"}, {"A-D", "0000111"},
                {"D&A", "0000000"}, {"D|A", "0010101"}, {"M", "1110000"}, {"!M", "1110001"},
                {"-M", "1110011"}, {"M+1", "1110111"}, {"M-1", "1110010"}, {"D+M", "1000010"},
                {"D-M", "1010011"}, {"M-D", "1000111"}, {"D&M", "1000000"}, {"D|M", "1010101"}
        };

        destCodes = arrToMap(destCodesArr);
        compCodes = arrToMap(compCodesArr);
        jmpCodes = arrToMap(jumpCodesArr);

        compCodes.keySet().stream()
                .filter(key -> key.matches(".*[+|&].*"))
                .collect(Collectors.toList())
                .forEach(
                        key -> compCodes.put(new StringBuilder(key).reverse().toString(), compCodes.get(key))
                );
    }

    /**
     * generate map from given two dimensional array
     * assume that given array has size nx2 and every string located at [k][0] is unique
     * return map where each [k][0] is key and [k][1] is value
     */
    private static Map<String, String> arrToMap(String[][] arr) {
        return Arrays.stream(arr)
                .collect(Collectors.toMap(row -> row[0], row -> row[1]));
    }

    public static boolean isCompCodeValid(String compCode) {
        return validCCommands.contains(compCode);
    }

    /**
     * Generate binary code for the C-command specified by destination, computation and jump
     * parts
     * @param dest destination part, may be null
     * @param comp computation part
     * @param jmp jump part, may be null
     * @return binary code as string of 0's and 1's
     */
    public static String getCCodeBinary(String dest, String comp, String jmp) {
        if (dest == null) {
            dest = KEY_NONE;
        }
        if (jmp == null) {
            jmp = KEY_NONE;
        }

        return C_COMMAND_PREFIX + compCodes.get(comp) + destCodes.get(dest) + jmpCodes.get(jmp);
    }
}
