package com.nvm.nand.asm;

import java.io.*;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by nvm on 3/17/17.
 */
public class Main {

    private static final String SRC_FILE_EXTENSION = ".asm";
    private static final String RES_FILE_EXTENSION = ".hack";

    private static List<String> readSource(String filePath) {
        try {
            BufferedReader reader = new BufferedReader(new FileReader(filePath));
            List<String> sourceText = reader.lines().collect(Collectors.toList());
            reader.close();
            return sourceText;
        }
        catch (FileNotFoundException ex) {
            ex.printStackTrace();
            return null;
        }
        catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    private static void writeSource(List<String> binary, String filePath) {
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(filePath));
            writer.write(String.join("\n", binary));
            writer.write("\n");
            writer.flush();
            writer.close();
        }
        catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    private static void printUsage() {
        System.out.println("Usage: java -jar program.asm");
    }

    public static void main(String[] args) throws Parser.ParseErrorException,
            CodeGenerator.UndefinedSymbolException {
        if (args.length != 1) {
            printUsage();
            return;
        }
        String srcFilePath = args[0];
        // check that source file has the right extension
        // TODO:
        // this actually works for now only when there is only one substring .asm
        // that is located at the end of sourcePath.
        // So, path dir/my.asm.files/source.asm will fail
        int extPos = srcFilePath.toLowerCase().indexOf(SRC_FILE_EXTENSION);
        if (extPos != srcFilePath.length()-SRC_FILE_EXTENSION.length()) {
            printUsage();
            return;
        }
        List<String> src = readSource(srcFilePath);
        Assembler asm = new Assembler();
        asm.passOne(src);
        String resultFilePath = srcFilePath.substring(0, extPos) + RES_FILE_EXTENSION;
        writeSource(asm.passTwo(src), resultFilePath);

    }
}
