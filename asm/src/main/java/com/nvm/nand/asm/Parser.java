package com.nvm.nand.asm;

import java.util.regex.Pattern;

import static com.nvm.nand.asm.Parser.CommandType.*;

/**
 * Parser is used for initial processing of assembly language text, consuming commands sequentially
 * and allowing to get access to various bits of the recently consumed command
 * Ignores comments and whitespaces and keeps track of total command number in program (excluding labels).
 */
public class Parser {

    public static class ParseErrorException extends Exception {
        public ParseErrorException(String line) {
            super(line);
        }
    }



    private int nextCommandAddr = 0;
    private String currentCommand;

    private Pattern symbolRegex = Pattern.compile("[a-zA-Z-_$:.][a-zA-Z-_$:.0-9]*");

    /**
     * Different command types
     * A_COMMAND - address command in form @symbol or @10
     * C_COMMAND - compute command like M=D+M
     * L_COMMAND - label, in form of (symbol)
     * EMPTY - empty line, whitespace or comment
     * Complete assembly language specification can be found in the book
     */
    public enum CommandType {A_COMMAND, C_COMMAND, L_COMMAND, EMPTY}

    /**
     * Feed one line of assembly code text, possibly advancing number of current command if the line being parsed
     * is one of the commands that will be translated to binary
     * @param line text line to be parsed
     * @return type of the parsed command
     */
    public CommandType feedLine(String line) throws ParseErrorException {
        line = line.trim();
        if (line.indexOf("//") != -1) {
            line = line.split("//")[0].trim();
        }
        if (line.equals("") || line.startsWith("//")) {
            return EMPTY;
        }
        else if (validateACommand(line)) {
            currentCommand = line;
            nextCommandAddr++;
            return A_COMMAND;
        }
        else if (validateCCommand(line)) {
            currentCommand = line;
            nextCommandAddr++;
            return C_COMMAND;
        }
        else if (validateLabel(line)) {
            currentCommand = line;
            return L_COMMAND;
        }
        else {
            throw new ParseErrorException(line);
        }
    }

    /**
     * Check if given string is a valid A command (one of the following):
     * - @name where name is a valid symbol
     * - @number where number is a decimal number
     * @param line string to check, assume that it starts with @ character
     * @return true if given string is a valid A command
     */
    public boolean validateACommand(String line) {
        if (!line.startsWith("@")) {
            return false;
        }
        String value = line.substring(1);
        if (!validateSymbol(value)) {
            try {
                int val = Integer.parseInt(value);
                return val >= 0;
            }
            catch (NumberFormatException ex) {
                return false;
            }
        }
        return true;
    }

    public boolean validateLabel(String line) {
        if (line.indexOf("(") == 0 && line.indexOf(")") == line.length()-1) {
            String labelValue = line.substring(1, line.length()-1);
            return validateSymbol(labelValue);
        }
        return false;
    }

    /**
     * Check if given symbol is valid (nonempty, should not start with a digit, and can contain
     * underscore (_), dot (.), dollar sign ($), and colon (:)
     * @param symbol
     * @return true if symbol is valid
     */
    public boolean validateSymbol(String symbol) {
        return symbolRegex.matcher(symbol).matches();
    }

    /**
     * @param line
     * @return true if given command is a valid C-command
     */
    public boolean validateCCommand(String line) {
        return Specification.isCompCodeValid(line);
    }

    /**
     * Get destination part of the current C-command
     * throw ParseErrorException in case current command is not a C-command
     * @return destination part, null if there is no destination part
     */
    public String getCommandDest() throws ParseErrorException {
        if (!validateCCommand(currentCommand)) {
            throw new ParseErrorException(currentCommand);
        }
        String dest = currentCommand;
        int eqPos = dest.indexOf("=");
        if (eqPos > -1) {
            return dest.substring(0, eqPos);
        }
        else {
            return null;
        }
    }

    /**
     * Get computation part of the current C-command
     * throw ParseErrorException in case current command is not a C-command
     * @return computation part
     */
    public String getCommandComp() throws ParseErrorException {
        if (!validateCCommand(currentCommand)) {
            throw new ParseErrorException(currentCommand);
        }
        String computation = currentCommand;
        int eqPos = computation.indexOf("=");
        if (eqPos > -1) {
            computation = computation.substring(eqPos+1);
        }
        int semicolonPos = computation.indexOf(";");
        if (semicolonPos > -1) {
            computation = computation.substring(0, semicolonPos);
        }
        return computation;
    }

    /**
     * Get jump part of the current C-command
     * throw ParseErrorException in case current command is not a C-command
     * @return jump part, null if it's not present
     */
    public String getCommandJmp() throws ParseErrorException {
        if (!validateCCommand(currentCommand)) {
            throw new ParseErrorException(currentCommand);
        }
        String jmp = currentCommand;
        int semicolonPos = jmp.indexOf(";");
        if (semicolonPos > -1) {
            return jmp.substring(semicolonPos+1, jmp.length());
        }
        else {
            return null;
        }
    }

    public String getACommandValue() throws ParseErrorException {
        if (!validateACommand(currentCommand)) {
            throw new ParseErrorException(currentCommand);
        }
        return currentCommand.substring(1);
    }

    /**
     * True if current A-command is a constant, like @15 and
     * not a variable like @var
     * throw ParseErrorException in case current command is not an A-command
     * @return
     */
    public boolean isACommandConstant() throws ParseErrorException {
        String value = getACommandValue();
        try {
            Integer.parseInt(value);
            return true;
        } catch (NumberFormatException ex) {
            return false;
        }
    }

    /**
     * Get symbol of current label command, LABEL for (LABEL) command
     * throw ParseErrorException in case current command is not a label
     * @return symbol of current label command
     */
    public String getLabelSymbol() throws ParseErrorException {
        if (!validateLabel(currentCommand)) {
            throw new ParseErrorException(currentCommand);
        }
        return currentCommand.substring(1, currentCommand.length()-1);
    }


    /**
     * Get address of the next command in resulting binary translation
     */
    public int getNextCommandAddr() {
        return nextCommandAddr;
    }
}
