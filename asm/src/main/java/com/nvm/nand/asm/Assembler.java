package com.nvm.nand.asm;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.nvm.nand.asm.CodeGenerator.UndefinedSymbolException;
import com.nvm.nand.asm.Parser.ParseErrorException;

/**
 * Created by nvm on 3/29/17.
 */
public class Assembler {

    private static final int VARIABLES_START_ADDR = 16;

    private SymbolTable table;
    private int varNextAddr = VARIABLES_START_ADDR;

    public Assembler() {
        Map<String, Integer> predefinedSymbols = new HashMap<>();
        predefinedSymbols.put("SP", 0);
        predefinedSymbols.put("LCL", 1);
        predefinedSymbols.put("ARG", 2);
        predefinedSymbols.put("THIS", 3);
        predefinedSymbols.put("THAT", 4);
        predefinedSymbols.put("SCREEN", 16384);
        predefinedSymbols.put("KBD", 24576);
        for (int i = 0; i <= 15; i++) {
            predefinedSymbols.put("R"+i, i);
        }
        table = new SymbolTable(predefinedSymbols);
    }

    /**
     * Make first pass over given program text, counting all command addresses
     * and filling symbol tables with label symbols addresses
     * @param program
     */
    public void passOne(List<String> program) throws Parser.ParseErrorException {
        Parser parser = new Parser();
        for (String line: program) {
            switch (parser.feedLine(line)) {
                case L_COMMAND:
                    String label = parser.getLabelSymbol();
                    table.set(label, parser.getNextCommandAddr());
                    break;
            }
        }
    }

    /**
     * Make second pass over given program text, producing a binary code string
     * for every command in the source program
     * @param program
     * @return
     */
    public List<String> passTwo(List<String> program) throws ParseErrorException, UndefinedSymbolException {
        List<String> binary = new ArrayList<>();
        Parser parser = new Parser();
        for (String line: program) {
            switch (parser.feedLine(line)) {
                case A_COMMAND:
                    String val = parser.getACommandValue();
                    if (!parser.isACommandConstant() && !getSymbolTable().hasKey(val)) {
                        // symbol in A-command is not present in symbol table, allocate new memory location for it
                        getSymbolTable().set(val, varNextAddr);
                        varNextAddr++;
                    }
                    binary.add(CodeGenerator.generateACommand(val, getSymbolTable()));
                    break;
                case C_COMMAND:
                    binary.add(CodeGenerator.generateCCommand(parser.getCommandDest(),
                            parser.getCommandComp(), parser.getCommandJmp()));
                    break;
            }
        }
        return binary;
    }

    public SymbolTable getSymbolTable() {
        return table;
    }

    public void setSymbolTable(SymbolTable table) {
        this.table = table;
    }

    public static class SymbolTable {

        private Map<String, Integer> symbolsMap;

        public SymbolTable() {
            symbolsMap = new HashMap<>();
        }

        public SymbolTable(Map<String, Integer> symbolsMap) {
            this.symbolsMap = symbolsMap;
        }

        public int get(String key) {
            return symbolsMap.getOrDefault(key, -1);
        }

        public boolean hasKey(String key) {
            return symbolsMap.containsKey(key);
        }

        private void set(String key, int value) {
            symbolsMap.put(key, value);
        }
    }
}
