package com.nvm.nand.vm.parser;

import org.junit.Test;


import java.util.List;

import static org.junit.Assert.*;

public class ParserTest {

    @Test
    public void parseCorrectArithmetic() throws Exception {

        assertEquals(Command.Op.ADD, Parser.parseLine("add").getOperation());
        assertEquals(Command.Op.SUB, Parser.parseLine("sub").getOperation());
        assertEquals(Command.Op.NEG, Parser.parseLine("neg").getOperation());
        assertEquals(Command.Op.EQ, Parser.parseLine("eq").getOperation());
        assertEquals(Command.Op.GT, Parser.parseLine("gt").getOperation());
        assertEquals(Command.Op.LT, Parser.parseLine("lt").getOperation());
        assertEquals(Command.Op.AND, Parser.parseLine("and").getOperation());
        assertEquals(Command.Op.OR, Parser.parseLine("or").getOperation());
        assertEquals(Command.Op.NOT, Parser.parseLine("not").getOperation());
    }

    @Test
    public void parseCorrectTypePush() throws Exception {
        assertEquals(Parser.parseLine("push constant 1").getOperation(), Command.Op.PUSH);
    }

    @Test
    public void parseCorrectTypePop() throws Exception {
        assertEquals(Parser.parseLine("pop local 0").getOperation(), Command.Op.POP);
    }

    @Test
    public void parseCorrectArgsPush() throws Exception {
        Command c = Parser.parseLine("push constant 1");
        assertEquals("arg1 incorrect", "constant", c.getArg1());
        assertEquals("arg2 incorrect", 1, c.getArg2());
    }

    @Test
    public void parseCorrectArgsPop() throws Exception {
        Command c = Parser.parseLine("push local 0");
        assertEquals("arg1 incorrect", "local", c.getArg1());
        assertEquals("arg2 incorrect", 0, c.getArg2());
    }

    @Test
    public void cleanTest() {
        assertEquals("Cleaned empty line should be empty", "", Parser.prepareLine(""));
        assertEquals("Cleaned spaces should be empty", "", Parser.prepareLine("      "));
        assertEquals("Cleaned comment should be empty", "", Parser.prepareLine("// some comment"));
        assertEquals("Cleaned comment with leading spaces should be empty", "", Parser.prepareLine("   // some comment"));
        assertEquals("Should strip leading spaces", "hello", Parser.prepareLine("     hello"));
        assertEquals("Should strip off comment", "hello", Parser.prepareLine("hello // this is hello"));
        assertEquals("Should strip off anything after //", "hello", Parser.prepareLine("hello // this is // hello /* hmmmm */"));
        assertEquals("Shouldn't affect the command body", "push local 0", Parser.prepareLine("push local 0 // save local var on the stack"));
        assertEquals("Line with no whitespace and no comment", "push local 0", Parser.prepareLine("push local 0"));
    }

    @Test
    public void parseProgramTest() throws Exception {
        List<Command> parsed = Parser.parse(
                "push constant 1\n" +
                "push constant 5\n"+
                "add");
        assertEquals(Command.Op.PUSH, parsed.get(0).getOperation());
        assertEquals("constant", parsed.get(0).getArg1());
        assertEquals(1, parsed.get(0).getArg2());
        assertEquals(Command.Op.PUSH, parsed.get(1).getOperation());
        assertEquals("constant", parsed.get(1).getArg1());
        assertEquals(5, parsed.get(1).getArg2());
        assertEquals(Command.Op.ADD, parsed.get(2).getOperation());
    }

    @Test
    public void labelTest() throws Exception {
        Command c = Parser.parseLine("label test");
        assertEquals("Incorrect operation", c.getOperation(), Command.Op.LABEL);
        assertEquals("arg1 incorrect", c.getArg1(), "test");
        assertEquals("arg2 incorrect", c.getArg2(), Command.NO_ARG2);
    }

    @Test
    public void gotoTest() throws Exception {
        Command c = Parser.parseLine("goto loop");
        assertEquals("Incorrect operation", c.getOperation(), Command.Op.GOTO);
        assertEquals("arg1 incorrect", c.getArg1(), "loop");
        assertEquals("arg2 incorrect", c.getArg2(), Command.NO_ARG2);
    }

    @Test
    public void ifTest() throws Exception {
        Command c = Parser.parseLine("if-goto end");
        assertEquals("Incorrect operation", c.getOperation(), Command.Op.IF);
        assertEquals("arg1 incorrect", c.getArg1(), "end");
        assertEquals("arg2 incorrect", c.getArg2(), Command.NO_ARG2);
    }

    @Test
    public void functionTest() throws Exception {
        Command c = Parser.parseLine("function fact 2");
        assertEquals("Incorrect operation", c.getOperation(), Command.Op.FUNCTION);
        assertEquals("arg1 incorrect", c.getArg1(), "fact");
        assertEquals("arg2 incorrect", c.getArg2(), 2);
    }

    @Test
    public void returnTest() throws Exception {
        Command c = Parser.parseLine("return");
        assertEquals("Incorrect operation", c.getOperation(), Command.Op.RETURN);
        assertEquals("arg1 incorrect", c.getArg1(), Command.NO_ARG1);
        assertEquals("arg2 incorrect", c.getArg2(), Command.NO_ARG2);
    }

    @Test
    public void callTest() throws Exception {
        Command c = Parser.parseLine("call mult 2");
        assertEquals("Incorrect operation", c.getOperation(), Command.Op.CALL);
        assertEquals("arg1 incorrect", c.getArg1(), "mult");
        assertEquals("arg2 incorrect", c.getArg2(), 2);
    }


}