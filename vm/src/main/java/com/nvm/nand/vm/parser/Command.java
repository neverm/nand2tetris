package com.nvm.nand.vm.parser;

/**
 * Represents a single VM command and provides access to the parts
 * By convention, first argument (if present) is always a string and second argument (if present)
 * is always an int
 */

public class Command {

    public enum Op { EMPTY, ADD, SUB, NEG, EQ, GT, LT, AND, OR, NOT, PUSH, POP, LABEL, GOTO, IF, FUNCTION, RETURN, CALL}


    public static final String NO_ARG1 = "";
    public static final int NO_ARG2 = -1;

    public static Command makeEmpty() {
        return new Command(Op.EMPTY, NO_ARG1, NO_ARG2);
    }

    public static Command makeArithmetic(Op op) {
        return new Command(op, NO_ARG1, NO_ARG2);
    }

    public static Command makeMemory(Op op, String arg1, int arg2) {
        return new Command(op, arg1, arg2);
    }

    public static Command makeReturn() {
        return new Command(Op.RETURN, NO_ARG1, NO_ARG2);
    }

    public static Command makeLabel(String labelIdentifier) {
        return new Command(Op.LABEL, labelIdentifier, NO_ARG2);
    }

    public static Command makeGoto(String labelIdentifier) {
        return new Command(Op.GOTO, labelIdentifier, NO_ARG2);
    }

    public static Command makeIf(String labelIdentifier) {
        return new Command(Op.IF, labelIdentifier, NO_ARG2);
    }

    public static Command makeFunction(String functionIdentifier, int argsNum) {
        return new Command(Op.FUNCTION, functionIdentifier, argsNum);
    }

    public static Command makeCall(String functionIdentifier, int argsNum) {
        return new Command(Op.CALL, functionIdentifier, argsNum);
    }

    private Op op;
    private String arg1;
    private int arg2;

    public Command(Op op, String arg1, int arg2) {
        this.op = op;
        this.arg1 = arg1;
        this.arg2 = arg2;
    }

    public Op getOperation() {
        return op;
    }

    public String getArg1() {
        return arg1;
    }

    public int getArg2() {
        return arg2;
    }

    public boolean isArithmeticBinary() {
        return (op == Op.ADD || op == Op.SUB || op == Op.AND || op == Op.OR);
    }

    public boolean isArithmeticUnary() {
        return (op == Op.NEG || op == Op.NOT);
    }

    public boolean isComparison() {
        return (op == Op.EQ || op == Op.GT || op == Op.LT);
    }

    public boolean isMemory() {
        return (op == Op.PUSH || op == Op.POP);
    }

    @Override
    public String toString() {
        return "Command{" +
                "op=" + op +
                ", arg1='" + arg1 + '\'' +
                ", arg2=" + arg2 +
                '}';
    }

    // todo: other factory methods for all types of operations
}
