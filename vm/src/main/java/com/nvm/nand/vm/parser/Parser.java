package com.nvm.nand.vm.parser;

import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Parser that takes input file in form of String with newline separated
 * VM commands and produces sequence of Command objects corresponding to
 * the given program.
 */

public class Parser {

    public static class ParseException extends Exception {
        public ParseException(String s) {
            super(s);
        }
    }

    private static final String OP_ADD = "add";
    private static final String OP_SUB = "sub";
    private static final String OP_NEG = "neg";
    private static final String OP_EQ = "eq";
    private static final String OP_GT = "gt";
    private static final String OP_LT = "lt";
    private static final String OP_AND = "and";
    private static final String OP_OR = "or";
    private static final String OP_NOT = "not";

    private static final String OP_PUSH = "push";
    private static final String OP_POP = "pop";

    private static final String OP_LABEL = "label";
    private static final String OP_GOTO = "goto";
    private static final String OP_IF_GOTO = "if-goto";

    private static final String OP_FUNC = "function";
    private static final String OP_CALL = "call";

    private static final Pattern PATTERN_ARITHMETIC = Pattern.compile("add|sub|neg|eq|gt|lt|and|or|not");
    private static final Pattern PATTERN_MEMORY =
            Pattern.compile("(push|pop)\\s(static|argument|local|this|that|pointer|temp|constant)\\s(\\d+)");

    private static final String SYMBOL = "[a-zA-Z_.:][a-zA-Z\\d_.:]*";
    private static final String FUNC_IDENTIFIER = SYMBOL, LABEL_IDENTIFIER = SYMBOL;
    private static final String RETURN = "return";

    // goto, if-goto and label commands have identical syntax
    private static final Pattern PATTERN_LABEL = Pattern.compile("(label|goto|if-goto)\\s+("+LABEL_IDENTIFIER+")");
    // function declaration and call have identical syntax
    private static final Pattern PATTERN_FUNC = Pattern.compile("(function|call)\\s+("+FUNC_IDENTIFIER+")\\s+(\\d+)");

    /**
     * Parse given program text and return list of commands in order in which they
     * appear in the given text
     * @param programText
     * @return
     */
    public static List<Command> parse(String programText) {
        List<String> lines = Arrays.asList(programText.split("\\r?\\n"));
        return lines.stream()
                .map(Parser::safeParseLine)
                .filter(c -> !(c.getOperation() == Command.Op.EMPTY))
                .collect(Collectors.toList());
    }

    /**
     * Wrapper around parseLine that throws runtime exception in case of parse error
     * @param line
     * @return
     */
    private static Command safeParseLine(String line) {
        try {
            return parseLine(line);
        }
        catch (ParseException ex) {
            throw new RuntimeException(ex);
        }
    }

    /**
     * Parse a single line of VM program
     * @param line
     * @return command that represents the given line
     */
    public static Command parseLine(String line) throws ParseException {
        String cleaned = prepareLine(line);
        if (cleaned.equals("")) {
            return Command.makeEmpty();
        }
        Matcher arithmeticMatcher = PATTERN_ARITHMETIC.matcher(cleaned);
        if (arithmeticMatcher.matches()) {
            return parseArithmetic(cleaned);
        }
        Matcher memoryMatcher = PATTERN_MEMORY.matcher(cleaned);
        if (memoryMatcher.matches()) {
            return parseMemory(memoryMatcher.group(1), memoryMatcher.group(2), memoryMatcher.group(3));
        }
        Matcher labelMatcher = PATTERN_LABEL.matcher(cleaned);
        if (labelMatcher.matches()) {
            return parseLabel(labelMatcher.group(1), labelMatcher.group(2));
        }
        Matcher funcMatcher = PATTERN_FUNC.matcher(cleaned);
        if (funcMatcher.matches()) {
            return parseFunction(funcMatcher.group(1), funcMatcher.group(2), funcMatcher.group(3));
        }
        if (cleaned.equals(RETURN)) {
            return Command.makeReturn();
        }
        throw new ParseException("Unknown command: " + line);
    }

    private static Command parseArithmetic(String commandText) throws ParseException {
        switch (commandText) {
            case OP_ADD:
                return Command.makeArithmetic(Command.Op.ADD);
            case OP_AND:
                return Command.makeArithmetic(Command.Op.AND);
            case OP_EQ:
                return Command.makeArithmetic(Command.Op.EQ);
            case OP_GT:
                return Command.makeArithmetic(Command.Op.GT);
            case OP_LT:
                return Command.makeArithmetic(Command.Op.LT);
            case OP_NEG:
                return Command.makeArithmetic(Command.Op.NEG);
            case OP_NOT:
                return Command.makeArithmetic(Command.Op.NOT);
            case OP_OR:
                return Command.makeArithmetic(Command.Op.OR);
            case OP_SUB:
                return Command.makeArithmetic(Command.Op.SUB);
            default:
                throw new ParseException("Unknown arithmetic operation: " + commandText);
        }
    }

    private static Command parseMemory(String operation, String arg1, String arg2) throws ParseException {
        int offset;
        try {
            offset = Integer.parseUnsignedInt(arg2);
            if (offset < 0) {
                throw new NumberFormatException();
            }
        }
        catch (NumberFormatException e) {
            throw new ParseException("Argument 2 must be a non-negative integer, was: " + arg2);
        }
        switch (operation) {
            case OP_PUSH:
                return Command.makeMemory(Command.Op.PUSH, arg1, offset);
            case OP_POP:
                return Command.makeMemory(Command.Op.POP, arg1, offset);
            default:
                throw new ParseException("Unknown memory operation: " + operation);
        }
    }

    /**
     * Parse label command provided as two strings
     * @param operation label, goto or if-goto
     * @param labelIdentifier name of the label
     * @return
     */
    private static Command parseLabel(String operation, String labelIdentifier) throws ParseException {
        switch (operation) {
            case OP_LABEL:
                return Command.makeLabel(labelIdentifier);
            case OP_IF_GOTO:
                return Command.makeIf(labelIdentifier);
            case OP_GOTO:
                return Command.makeGoto(labelIdentifier);
            default:
                throw new ParseException("Unknown label operation: " + operation);
        }
    }

    /**
     * Parse function command provided as three strings
     * @param operation label, goto or if-goto
     * @param functionIdentifier name of the function
     * @param argsNum number of arguments
     * @return
     */
    private static Command parseFunction(String operation, String functionIdentifier, String argsNum)
        throws ParseException {
        int args;
        try {
            args = Integer.parseUnsignedInt(argsNum);
            if (args < 0) {
                throw new NumberFormatException();
            }
        }
        catch (NumberFormatException e) {
            throw new ParseException("Number of arguments must be a non-negative integer, was: " + argsNum);
        }
        switch (operation) {
            case OP_FUNC:
                return Command.makeFunction(functionIdentifier, args);
            case OP_CALL:
                return Command.makeCall(functionIdentifier, args);
            default:
                throw new ParseException("Unknown function operation: " + operation);
        }
    }

    /**
     * Clean given line from surrounding whitespaces and comments and make it lowercase
     * @param line
     * @return
     */
    public static String prepareLine(String line) {
        int commentPos = line.indexOf("//");
        String cleaned = line;
        if (commentPos != -1) {
            cleaned = line.substring(0, commentPos);
        }
        return cleaned.trim();
    }
}
