package com.nvm.nand.vm;

import com.nvm.nand.vm.codegen.CodeGenerator;
import com.nvm.nand.vm.parser.Command;
import com.nvm.nand.vm.parser.Parser;

import java.io.*;

public class Main {

    private static String translateFile(File file, String generatorName) {
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file)));
            StringBuilder result = new StringBuilder();
            CodeGenerator gen = new CodeGenerator(generatorName);
            String line;
            while((line = reader.readLine()) != null) {
                Command c = Parser.parseLine(line);
                if (c.getOperation() != Command.Op.EMPTY) {
                    result.append(gen.generate(c));
                }
            }
            return result.toString();
        }
        catch (IOException|Parser.ParseException ex) {
            throw new RuntimeException(ex);
        }
    }

    /**
     * Write assembly program text to the file specified by filename
     * Prepend the code by vm bootstrap code
     */
    private static void writeFile(String text, String filename) {
        text = CodeGenerator.generateInit() + text;
        try {
            PrintWriter writer = new PrintWriter(filename, "UTF-8");
            writer.println(text);
            writer.close();
        }
        catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    private static boolean isVmFile(String filename) {
        return filename.toLowerCase().endsWith(".vm");
    }

    private static String stripVmExtension(String filename) {
        return filename.substring(0, filename.indexOf(".vm"));
    }

    public static void main(String[] args) {
        if (args.length < 1) {
            System.err.println("Provide vm file or a directory name as an argument");
        }

        String filename = args[0];
        File input = new File(filename);
        if (input.isDirectory()) {
            StringBuilder result = new StringBuilder();
            for (File file: input.listFiles()) {
                if (isVmFile(file.getName())) {
                    result.append("// " + file.getName() + "\n");
                    result.append(translateFile(file, stripVmExtension(file.getName())));
                }
            }
            writeFile(result.toString(), input.getName() + ".asm");
        }
        else if (isVmFile(filename)) {
            String baseFilename = stripVmExtension(filename);
            String outName = baseFilename + ".asm";
            writeFile(translateFile(input, baseFilename), outName);
        }
        else {
            System.err.println("Invalid argument: not a directory and not a *.vm file");
        }

    }
}
