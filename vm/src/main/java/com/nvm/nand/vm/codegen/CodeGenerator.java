package com.nvm.nand.vm.codegen;

import com.nvm.nand.vm.parser.Command;
import com.nvm.nand.vm.parser.Command.Op;

public class CodeGenerator {

    private static final String VM_SEG_THIS = "this";
    private static final String VM_SEG_THAT = "that";
    private static final String VM_SEG_LOCAL = "local";
    private static final String VM_SEG_ARGUMENT = "argument";
    private static final String VM_SEG_POINTER = "pointer";
    private static final String VM_SEG_CONSTANT = "constant";
    private static final String VM_SEG_STATIC = "static";
    private static final String VM_SEG_TEMP = "temp";

    private static final int TEMP_BASE_ADDR = 5;
    private static final int PRESERVE_D_VALUE_ADDR = 15;
    private static final int PRESERVE_RETURN_ADDR = 14;
    private static final int PRESERVE_FRAME_ADDR = 13;
    private static final int STACK_ADDR = 256;
    private static final String INIT_FUNCTION_NAME = "Sys.init";

    /**
     * Concatenate together given commands into a single command sequence, separated by newlines.
     * Assume commands do not end with newlines.
     */
    private static String makeSequence(String... commands) {
        StringBuilder result = new StringBuilder();
        for (String command: commands) {
            if (!command.endsWith("\n")) {
                command += "\n";
            }
            result.append(command);
        }
        return result.toString();
    }
    /**
     * Push contents of D register onto the stack and increment stack pointer
     * tested on the emulator
     */
    public static final String PUSH_D = makeSequence(
            "@SP",
            "A=M",
            "M=D",
            "@SP",
            "M=M+1");

    /**
     * Pop value from the stack and save it in the D register
     */
    public static final String POP_D = makeSequence(
            "@SP",
            "A=M-1",
            "D=M",
            "@SP",
            "M=M-1");

    /**
     * Load arguments from the stack: y at the top of the stack to D register and next to top value x
     * to A register
     */
    public static final String LOAD_ARGS = makeSequence(
            "@SP",
            "A=M-1",
            "D=M",
            "A=A-1",
            "A=M");

    /**
     * Remove two top items from the stack and put the content of register D to the top of the stack
     */
    public static final String SAVE_RESULT_BINARY_OP = makeSequence(
            "@SP",
            "M=M-1",
            "A=M-1",
            "M=D");

    /**
     * Take top of the stack to the register D and the next item to the top to the register A
     * Perform given binary operation on these registers and save the result on the top of the stack
     * So if the top of the stack is y and the item next to it is x, then to perform x-y the argument supplied
     * must be D=A-D
     * This operation reduces size of the stack by 1
     * @param operationCommandAsm assembly command that performs binary operation and saves its result
     *                            in the D register
     */
    private static String carryOutBinaryOp(String operationCommandAsm) {
        return makeSequence(LOAD_ARGS, operationCommandAsm, SAVE_RESULT_BINARY_OP);

    }

    /**
     * Load value located in memory, pointed by given symbolName (i.e. dereference this symbol)
     * and push this value to the stack
     * @param symbolName whose value should be pushed, eg ARG. Symbol name should not start with @
     */
    private static String pushValueAt(String symbolName) {
        return makeSequence(
                "@" + symbolName,
                "A=M",
                "D=A",
                PUSH_D);
    }

    /**
     * Restore something that is located in memory at location specified by base address and offset
     * and save it into target address
     * Target is provided either in form of direct address like "@15" or as an assembly language alias,
     * like "@LCL"
     */
    private static String restoreItem(int baseAddr, int offset, String target) {
        String loadValueToD;
        if (offset == 0) {
            loadValueToD = makeSequence(
                    "@" + baseAddr,
                    "D=M"
            );
        }
        else if (offset == 1) {
            loadValueToD = makeSequence(
                    "@" + baseAddr,
                    "A=M-1",
                    "D=M"
            );
        }
        else {
            loadValueToD = makeSequence(
                    "@" + baseAddr,
                    "D=M",
                    "@" + offset,
                    "A=D-A",
                    "D=M"
            );
        }
        return makeSequence(
                loadValueToD,
                "@" + target,
                "M=D");
    }

    private int cmpCounter;
    private int callCounter;
    private final String fileName;
    private String currentFunction = "";

    public CodeGenerator(String fileName) {
        this.fileName = fileName;
    }

    public String generate(Command cmd) {
        if (cmd.isArithmeticBinary()) {
            return generateArithmeticBinary(cmd);
        }
        else if (cmd.isArithmeticUnary()) {
            return generateArithmeticUnary(cmd);
        }
        else if (cmd.isComparison()) {
            return generateComparison(cmd);
        }
        else if (cmd.isMemory()) {
            return generateMemory(cmd);
        }
        else if (cmd.getOperation() == Op.FUNCTION) {
            currentFunction = cmd.getArg1();
            return generateFunction(cmd);
        }
        else if (cmd.getOperation() == Op.CALL) {
            String funcName = cmd.getArg1();
            String returnLabel = generateReturnLabel(funcName);
            return generateCall(cmd, funcName, returnLabel);
        }
        else if (cmd.getOperation() == Op.RETURN) {
            return generateReturn(cmd);
        }
        else if (cmd.getOperation() == Op.LABEL) {
            return generateLabel(cmd);
        }
        else if (cmd.getOperation() == Op.GOTO) {
            return generateGoto(cmd);
        }
        else if (cmd.getOperation() == Op.IF) {
            return generateGotoIf(cmd);
        }
        else {
            throw new IllegalArgumentException("Unsupported command: " + cmd.toString());
        }
    }

    /**
     * Generate vm initialization code that assigns stack address and calls
     * init function. This function should be provided by compiler frontend and determines
     * the entry point of the resulting program
     * Technically, we could jump directly to the init function label,
     * but the provided test scripts assume presence of a stack frame
     * so I guess that's what they wanted me to do
     */
    public static String generateInit() {
        String retLabel = INIT_FUNCTION_NAME + "-ret";
        return makeSequence(
                "@" + STACK_ADDR+" // init",
                "D=A",
                "@SP",
                "M=D",
                generateCall(Command.makeCall(INIT_FUNCTION_NAME, 0), INIT_FUNCTION_NAME, retLabel));
    }

    /**
     * Generate assembly code for a given function command
     * Resulting assembly code consists of label (function entry) and padding stack
     * for function's local variables
     */
    private String generateFunction(Command cmd) {
        int localCount = cmd.getArg2();
        String result = "(" + cmd.getArg1() + ") //function\n"; // write label with function name at the top
        if (localCount > 0) {
            result += "@0\n" + "D=A\n"; // load 0 into D register
            for (int i = 0; i < localCount; i++) {
                result += PUSH_D; // push 0s onto the stack to serve as local variables
            }
        }
        return result;
    }

    /**
     * Generate assembly code for a given call command.
     * Call command assumes that n arguments have been pushed onto the stack. It does the following:
     * 1. Save return address, local, argument, this and that pointers onto the stack
     * 2. Adjust ARG pointer to point to the arguments to this function
     * 3. Adjust local variables pointer to the top of the stack
     * 4. Jump to the function entry point
     * @param cmd command object that represents this call command
     * @param funcName name of the function to be called
     * @param retLabel generated return label name for this call
     */
    private static String generateCall(Command cmd, String funcName, String retLabel) {
        int argCount = cmd.getArg2();
        // offset from the top of the stack to first argument after caller's world has been saved on the stack
        int argOffset = 5 + argCount;
        return makeSequence(
                // save caller's world onto the stack:
                // save return address
                "@" + retLabel,
                "D=A",
                PUSH_D,
                // save values of the registers
                pushValueAt("LCL"),
                pushValueAt("ARG"),
                pushValueAt("THIS"),
                pushValueAt("THAT"),
                // adjust ARG to point to the first argument
                "@SP",
                "D=M",
                "@" + argOffset + "",
                "D=D-A",
                "@ARG",
                "M=D",
                // adjust LCL to point to the top of the stack (below caller's world)
                "@SP",
                "D=M",
                "@LCL",
                "M=D",
                // jump to the body of the function being called
                "@" + funcName + "",
                "0;JMP",
                // put return label to jump after the function returns
                "(" + retLabel + ")");

    }

    /**
     * Generate assembly code for a given return command
     * Return command has to remedy the actions that call command performed and put the function's return
     * value on the stack. It accomplishes this as follows:
     * 1. Save local pointer to a temp variable FRAME (it's the start address of current function frame)
     * 2. Take top value from the stack (result of current function) and put it into
     * the first argument.
     * 3. Set stack pointer to arg+1. Effectively, by steps 2-3 we throw away all
     * arguments (and whatever is below them on the stack) and put there return value.
     * 4. Restore previous values of this, that, arg, and local pointers using frame pointer with offsets
     * 5. Take return address using frame pointer and jump
     */
    private String generateReturn(Command cmd) {
        return makeSequence(
                // save LCL value as base of current frame (just below the caller world)
                "@LCL // return",
                "D=M",
                "@" + PRESERVE_FRAME_ADDR,
                "M=D",
                // retrieve return address from caller's world
                "@5",
                "A=D-A",
                "D=M",
                // save return address to a temp memory location
                "@" + PRESERVE_RETURN_ADDR,
                "M=D",
                // save return value to the place where 1st argument to current function was
                // (or return address if the function had 0 arguments)
                // but no worries, it's saved in a temp location
                POP_D,
                "@ARG",
                "A=M",
                "M=D",
                // adjust stack pointer to make return value the top of the stack
                "D=A+1",
                "@SP",
                "M=D",
                // restore pointers to the state before the call, use saved to a temp memory location frame address
                // restore THAT
                restoreItem(PRESERVE_FRAME_ADDR, 1, "THAT"),
                // restore THIS
                restoreItem(PRESERVE_FRAME_ADDR, 2, "THIS"),
                // restore ARG
                restoreItem(PRESERVE_FRAME_ADDR, 3, "ARG"),
                // restore LCL
                restoreItem(PRESERVE_FRAME_ADDR, 4, "LCL"),
                // restore return address
                "@" + PRESERVE_RETURN_ADDR,
                "A=M",
                "0;JMP");
    }

    private String generateLabel(Command cmd) {
        return "(" + generateJumpLabel(cmd.getArg1()) + ")\n";
    }

    /**
     * Generate assembly code for vm goto function
     * Take into account current function (the nearest function command to the top) and
     * generate label augmented with the current function name
     */
    private String generateGoto(Command cmd) {
        return makeSequence("@" +  generateJumpLabel(cmd.getArg1()) + " // goto",
                            "0;JMP");
    }

    /**
     * Generate assembly code for vm goto-if function, which pops item off the stack,
     * checks if it's nonzero and if it's the case then jumps to the given label
     */
    private String generateGotoIf(Command cmd) {
        return makeSequence(
                POP_D,
                "@" + generateJumpLabel(cmd.getArg1()),
                "D;JNE");
    }

    /**
     * Generate function return label. Since we can have multiple calls to the same function,
     * we need to distinguish between them. This is done by using call counter, which is
     * appended to each function call
     */
    private String generateReturnLabel(String funcName) {
        callCounter++;
        return "return-" + funcName + "-" + callCounter + "-to-" + currentFunction;
    }

    /**
     * Generate label with respect to current function, in form
     * functionName$labelName
     */
    private String generateJumpLabel(String labelName) {
        return currentFunction + "$" + labelName;
    }

    /**
     * Generate assembly code for the given binary operation command.
     * This operation takes two arguments x, y off the stack where y is on the top
     * and calculates given arithmetic function f(x, y). For subtraction the order matters,
     * so to calculate x-y y must be at the top of the stack
     * @param cmd
     * @return
     */
    private String generateArithmeticBinary(Command cmd) {
        switch (cmd.getOperation()) {
            case ADD:
                return carryOutBinaryOp("D=A+D");
            case SUB:
                return carryOutBinaryOp("D=A-D");
            case AND:
                return carryOutBinaryOp("D=A&D");
            case OR:
                return carryOutBinaryOp("D=A|D");
            default:
                throw new IllegalArgumentException("Illegal arithmetic command: " + cmd.toString());
        }
    }

    private String generateArithmeticUnary(Command cmd) {
        // load topmost stack item address to A
        String LOAD_ARG_ADDR =
                "@SP\n" +
                "A=M-1\n";
        switch (cmd.getOperation()) {
            case NOT:
                return LOAD_ARG_ADDR + "M=!M\n";
            case NEG:
                return LOAD_ARG_ADDR + "M=-M\n";
            default:
                throw new IllegalArgumentException("Illegal arithmetic command: " + cmd.toString());
        }
    }

    /**
     * Compute comparison function f(x, y) on the items at the top of the stack (y is the topmost, x is next to it)
     * Put the result back in form of True or False, where False=0(0x0000) and True=-1(0xFFFF)
     * @param cmd
     * @return
     */
    private String generateComparison(Command cmd) {
        String cmpTrueLabel = fileName + ".CMP_IS_TRUE_" + cmpCounter;
        String cmpEpilogueLabel = fileName + ".CMP_EPILOGUE_" + cmpCounter;
        cmpCounter++;
        final String COMPARISON_TEMPLATE = makeSequence(
                LOAD_ARGS,
                "D=A-D", // save x-y to D
                "@"+cmpTrueLabel,
                "D;%s", // jump depending on a function that will be inserted here
                "@0", // set answer to False
                "D=A",
                "@"+cmpEpilogueLabel,
                "0;JMP", // jump to saving result
                "("+cmpTrueLabel+")",
                "@0",
                "D=!A", // set answer to True
                "("+cmpEpilogueLabel+")",
                SAVE_RESULT_BINARY_OP);
        switch (cmd.getOperation()) {
            case LT:
                return String.format(COMPARISON_TEMPLATE, "JLT");
            case GT:
                return String.format(COMPARISON_TEMPLATE, "JGT");
            case EQ:
                return String.format(COMPARISON_TEMPLATE, "JEQ");
            default:
                throw new IllegalArgumentException("Illegal comparison command: " + cmd.toString());
        }
    }

    /**
     * Generate assembly code for the given memory command.
     * Memory commands are written in form push/pop segment idx
     */
    private String generateMemory(Command cmd) {
        String segment = cmd.getArg1();
        int idx = cmd.getArg2();
        String loadAddrToA = generateLoadSegmentAddr(segment, idx);

        if (cmd.getOperation() == Op.PUSH) {
            if (segment.equals(VM_SEG_CONSTANT)) {
                // the value we have in A is the value we need to push
                loadAddrToA += "D=A\n";
            }
            else {
                // the value we have in A is an address, we need to dereference it to get the value
                loadAddrToA += "D=M\n";
            }
            return loadAddrToA + PUSH_D;
        }
        else if (cmd.getOperation() == Op.POP) {
            if (segment.equals(VM_SEG_CONSTANT)) {
                // the value we have in A is the value we need to push
                throw new IllegalArgumentException("Pop to constant segment: " + cmd.toString());
            }
            return makeSequence(
                    loadAddrToA,
                    "D=A",
                    "@" + PRESERVE_D_VALUE_ADDR,
                    "M=D", // preserve calculated segment address value into a temp memory location
                     POP_D +
                    "@" + PRESERVE_D_VALUE_ADDR,
                    "A=M", // load target segment address to A, so that we can write there
                    "M=D");
        }
        else {
            throw new IllegalArgumentException("Illegal memory command: " + cmd.toString());
        }
    }


    /**
     * Generate assembly code to load address of the memory location of given segment with offset idx
     * into address register (A)
     * Implementation of different segments is the following:
     * - constant, this segment is emulated by simply inlining the constant in assembly code by
     * - argument, local, this, that are managed by VM implementation, assembly code just loads base address
     *  of corresponding segment and adds idx offset
     * - pointer loads the address of this or that by using assembly language aliases THIS and THAT
     * - temp uses addresses between RAM[5] and RAM[12], thus allowing indexing from 0 to 7 inclusive
     * - static emulates static variable space by creating for each variable idx an assembly language
     *  variable @CurrentFile.idx
     *  Warning: this operation does not preserve value in the D register
     * @param segment
     * @return
     */
    private String generateLoadSegmentAddr(String segment, int idx) {
        switch (segment) {
            case VM_SEG_CONSTANT:
                return "@" + idx + "\n";
            case VM_SEG_ARGUMENT:
            case VM_SEG_LOCAL:
            case VM_SEG_THIS:
            case VM_SEG_THAT:
                // todo: omit indexing for idx==0
                return  makeSequence(
                        "@" + idx,
                        "D=A",
                        "@" + getMemoryAlias(segment),
                        "A=M",
                        "A=A+D");
            case VM_SEG_POINTER:
                // get address of THIS segment
                if (idx == 0) {
                    return "@" + getMemoryAlias(VM_SEG_THIS) + "\n";
                }
                // get address of THAT segment
                else if (idx == 1) {
                    return "@" + getMemoryAlias(VM_SEG_THAT) + "\n";
                }
                throw new IllegalArgumentException("Illegal index for pointer segment: " + idx);
            case VM_SEG_TEMP:
                if (idx < 0 || idx > 7) {
                    throw new IllegalArgumentException("Illegal index for temp segment, must be 0 <= i <= 7: " + idx);
                }
                return "@" + (TEMP_BASE_ADDR + idx) + "\n";
            case VM_SEG_STATIC:
                return "@" + fileName + "." + idx + "\n";
            default:
                throw new IllegalArgumentException("Illegal segment: " + segment);
        }
    }

    /**
     * Get alias that is used by assembler to generate an address for pointer to the given segment
     * @param segmentName name of the segment to get alias for
     */
    private static String getMemoryAlias(String segmentName) {
        switch (segmentName) {
            case "this":
            case "that":
                return segmentName.toUpperCase();
            case "local":
                return "LCL";
            case "argument":
                return "ARG";
            default:
                throw new IllegalArgumentException("Illegal segment: " + segmentName);
        }
    }
}
